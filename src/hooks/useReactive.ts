/**
 * @description  声明ref变量的hooks
 * @param {initialState} 默认值
 * @return state 声明ref变量
 *          setState 设置变量方法
 * example const [ state, setState, stateRefs ] = useState(initialState)
 */

import { reactive, toRefs } from 'vue';

function useReactive (initialState: Record<any, any>) {

  const state = reactive(initialState)

  const stateRefs = toRefs(state)

  /**
   * @description  声明ref变量的hooks
   * @param {value} 默认值
   * example setState('key', value)
   *          setState({ key: value})
   */
  function setState (key: any, value: unknown) {
    if (Object.prototype.toString.call(key) !== '[object Object]' && !Object.prototype.hasOwnProperty.call(initialState, key)) {
      throw new ReferenceError(`Can't find the prototype ${ key } form the Object`);
    }

    if (Object.prototype.toString.call(key) === '[object Object]') {
      for (let k in key) {
        if (Object.prototype.hasOwnProperty.call(initialState, k)) {
          (state as any)[k] = key[k]
        }
      }
    } else {
      if (typeof value === 'function') {
        (state as any)[key] = value((state as any)[key])
      } else {
        (state as any)[key] = value
      }
    }
    
  }
  return ([
    state,
    setState,
    stateRefs
  ]) as any
}

export default useReactive;