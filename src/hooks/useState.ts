/**
 * @description  声明ref变量的hooks
 * @param {initialState} 默认值
 * @return state 声明ref变量
 *          setState 设置变量方法
 * example const [ state, setState ] = useState(initialState)
 */
import { ref } from 'vue';

const states: any[] = [];
const stateSetters: any[] = [];
let stateIndex: number = 0;

function createState (initialState: unknown, stateIndex: number) {
  const state = ref(initialState);

  return states[stateIndex] !== undefined ? states[stateIndex] : state
}
function createStateSetter (stateIndex: number) {
  return function (newState: unknown) {
    if (typeof newState === 'function') {
      states[stateIndex].value = newState(states[stateIndex])
    } else {
      states[stateIndex].value = newState
    }
  }
}
function useState (initialState: unknown) {
  states[stateIndex] = createState(initialState, stateIndex)

  if (!stateSetters[stateIndex]) {
    stateSetters.push(createStateSetter(stateIndex))
  }
  const _state = states[stateIndex]
  const _setState = stateSetters[stateIndex]

  stateIndex++

  return [
    _state,
    _setState
  ]
}

export default useState
