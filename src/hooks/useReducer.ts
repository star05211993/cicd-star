/**
 * @description  声明ref变量的hooks
 * @param {reducer} 派发器
 * @param {initialState} 默认值
 * @return state 声明ref变量
 *          setState 设置变量方法
 * example const [ state, dispatch ] = useState(initialState)
 */
import useState from './useState';


function useReducer (reducer: Function, initialState: unknown) {

  const [ state, setState ] = useState(initialState)

  const dispatch = (action: Object) => {
    if (Object.prototype.toString.call(action) !== '[object Object]') {
      throw TypeError("The parmetter 'action' must be the type 'Object'")
    }

    if (!Object.prototype.hasOwnProperty.call(action,'type')) {
      throw TypeError("The parmetter 'action' need the type 'Object'")
    }

    reducer(state, setState, action)
  }
  return [
    state,
    dispatch
  ]
}

export default useReducer
 