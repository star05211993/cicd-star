export const TOKEN = 'token';
// 用户信息
export const USER_INFO = 'userInfo';
// 租户信息
export const TENANT_INFO = 'tenantInfo';
// 默认登录路径
export const DEFAULT_LOGIN_PATH = 'dashboard';
// 默认接口返回错误码处理
export const DEFAULT_CODE = ['401', '212008','1112'];
// 默认logo
export const DEFAULT_LOGO = 'https://front.mayibida.com/zhilian/images/logo/mos-logo.png';
