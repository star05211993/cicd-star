
import AVATAR from "@/assets/images/menu/user2.png";
export const ENV:any = import.meta.env.MODE;

export const encryptKey = 'mayizhilian2020'; // 加密关键字
export const encryptStorage = true;  // 是否加密

export const SHA = ENV === 'prod' ? 'cd0d99cb11834cd395fb2c5eabd33f20' : '711f1e7fe9ee24212045662b5b578f0432ec3fb5';
export const AGENT = 3;
export const APPVERSION = '1.0.0';
export const APPID = ENV === 'prod' ? '83027d594db543b983d75575f26e69b4' : 'kkTk4bUB' ;
export const APPNAME = 1;


export const DEFAULT_AVATAR = AVATAR;
export const DEFAULT_TITLE = '蚂蚁必达'

const DOMAIN_MAP:any = {
    // localhost: 'http://devmos.mayizhilian.net', // 本地代理调试 NODE_ENV = "development"
    test: 'http://testmosnew.mayizhilian.net', // 本地代理调试 NODE_ENV = "development"
    // dev: 'http://devmos.mayizhilian.net'
    // qc: 'http://testmosnew.mayizhilian.net',
    // pre: 'https://premos.mayibida.com/', // 未定(需真实对应慎重添加 最好不加)
    // prod: 'http://prodmos.mayizhilian.net', // 未定(需真实对应慎重添加 最好不加)

}
export const DOMAIN = DOMAIN_MAP[ENV] // 本地调试修改IP

// 根据IP匹配
export const TENANT_DOMAIN = !(ENV in DOMAIN_MAP) ? window.location.origin : DOMAIN