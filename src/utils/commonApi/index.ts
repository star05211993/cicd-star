import { settlementApi } from '@/api/settlementApi/index';
import { settleTrackApi } from '@/api/settleTrackApi/index';
import { memberUser } from '@/api/member';
import lodash from 'lodash';
import { useUserStore, useRouterStore } from '@/store';
const userStore = useUserStore();
const useRouter = useRouterStore();
export default {
  // 所属公司、销货单位=所属公司=出票公司（发票管理页）
  memberQueryUserAuthorithPart: async (params) => {
    const body = {
      relationTypes: '4',
    };
    const paramsData = lodash.merge(body, params);
    const res = await settlementApi.memberQueryUserAuthorithPart(paramsData);
    const { data } = res;
    return data || {};
  },
  // 获取可用的支票信息
  getSettleListCheque: async (params) => {
    const res = await settlementApi.getSettleListCheque(params);
    const { data } = res;
    return data || [];
  },
  // 获取可用的汇票信息
  getSettleListDraft: async (params) => {
    const res = await settlementApi.getSettleListDraft(params);
    const { data } = res;
    return data || [];
  },
  //获取币别 - 新提供的
  async getCurrency() {
    const res = await settlementApi.settleBasicEnterpriseCurrencyAll();
    const { data } = res;
    return data || [];
  },
  // 按钮权限
  getRoleByPage: async (page) => {
    const pageIdList = useRouter.pageIdList || [];
    const pageId = pageIdList.find(
      (v) => v.pageCode == page || v.pageName == page
    )?.pageId;
    const params = { pageId };
    const res = await memberUser.getRoleByPage(params);
    const { data } = res;
    return data || [];
  },
  // 下拉框-未关账的结算单列表
  statementUnclosed: async (params) => {
    const res = await settlementApi.statementUnclosed(params);
    const { data } = res;
    return data || [];
  },
  // 获取费用默认发票
  async getDefaultInvoice(params) {
    const res = await settlementApi.getLogisticsFeeDefaultInvoice(params);
    const { code, data } = res;
    if (code !== '200') {
      return;
    }
    return data;
  },
  //获取支付方式
  async getPaymentOptions() {
    const res = await settlementApi.basicdataCodeSelect(['paymentMethod']);
    const { code, data } = res;
    if (code !== '200') {
      return;
    }
    return data || [];
  },
  //支付对象、收款对象
  async queryRelationByLabelCode(params) {
    const body = {
      relationType: 9, // 8=客户 9=供应商 二可选一
      labelCode: '',
      name: '', // 模糊查询关键词 非必传
      enterpriseIds: [], // 非必传，全选的情况传企业id集合，返回的数据会包含勾选过的
    };
    const paramsData = lodash.merge(body, params);
    const res = await settlementApi.queryRelationByLabelCode(paramsData);
    const { data } = res;
    return data || [];
  },
  // 发票类型
  async settleBasicEnterpriseInvoice() {
    const res = await settlementApi.settleBasicEnterpriseInvoice();
    const { data } = res;
    return data || [];
  },
  // 收款人||员工列表
  getMembersByEnterpriseid: async () => {
    const params = { enterpriseId: userStore.enterpriseId };
    const res = await settlementApi.getMembersByEnterpriseid(params);
    const { data } = res;
    return data || [];
  },
  // 查询企业币种汇率
  settleBasicCurrencyRate: async (params) => {
    const body = {
      convertCode: 'CNY', // 源币种code
      sourceCode: 'CNY', // 换算币种code
      ownerId: 489, // 所属公司id
    };
    const paramsData = lodash.merge(body, params);
    const res = await settleTrackApi.settleBasicCurrencyRate(paramsData);
    const { code, data } = res;
    if (code == 200) {
      return data || [];
    }
  },
  // 费用名称
  async settleBasicEnterpriseCostFee(params) {
    const res = await settlementApi.settleBasicEnterpriseCostFee(params);
    const { code, data } = res;
    if (code == '200') {
      return data || [];
    }
  },
};
