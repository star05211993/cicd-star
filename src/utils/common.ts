import CryptoJS from 'crypto-js';
import { SHA } from '@/constant';
import lodash from 'lodash';
import { format } from 'date-fns';
export const env = import.meta.env.VITE_API_NODE_ENV;
type TargetContext = '_self' | '_parent' | '_blank' | '_top';

export const openWindow = (
  url: string,
  opts?: { target?: TargetContext; [key: string]: any }
) => {
  const { target = '_blank', ...others } = opts || {};
  window.open(
    url,
    target,
    Object.entries(others)
      .reduce((preValue: string[], curValue) => {
        const [key, value] = curValue;
        return [...preValue, `${key}=${value}`];
      }, [])
      .join(',')
  );
};

export const regexUrl = new RegExp(
  '^(?!mailto:)(?:(?:http|https|ftp)://)(?:\\S+(?::\\S*)?@)?(?:(?:(?:[1-9]\\d?|1\\d\\d|2[01]\\d|22[0-3])(?:\\.(?:1?\\d{1,2}|2[0-4]\\d|25[0-5])){2}(?:\\.(?:[0-9]\\d?|1\\d\\d|2[0-4]\\d|25[0-4]))|(?:(?:[a-z\\u00a1-\\uffff0-9]+-?)*[a-z\\u00a1-\\uffff0-9]+)(?:\\.(?:[a-z\\u00a1-\\uffff0-9]+-?)*[a-z\\u00a1-\\uffff0-9]+)*(?:\\.(?:[a-z\\u00a1-\\uffff]{2,})))|localhost)(?::\\d{2,5})?(?:(/|\\?|#)[^\\s]*)?$',
  'i'
);

export const sha256Encrypt = (str: string) => {
  const hash = CryptoJS.HmacSHA256(str, SHA);
  const hashInHex = CryptoJS.enc.Hex.stringify(hash);
  return hashInHex.toUpperCase();
};
/**
 *  json转换成formData
 * @param {*} data
 */
export const jsonToForm = (obj: any) => {
  if (Object.prototype.toString.call(obj) === '[object FormData]') {
    return obj;
  }
  const formData = new FormData();
  for (let key in obj) {
    if ({}.hasOwnProperty.call(obj, key)) {
      formData.append(key, obj[key]);
    }
  }
  return formData;
};
export const formatOptions = (target: any, option: any = {}) => {
  const { value = 'value', label = 'label', string = false } = option;
  let arr: any = [];
  target &&
    target.forEach((i: any) => {
      const isObject = Object.prototype.toString.call(i) === '[object Object]';
      if (isObject) {
        arr.push({
          ...i,
          value: !string ? i[value] : String(i[value]),
          label: String(i[label]),
        });
      } else {
        arr.push({ ...i, value: !string ? i : String(i), label: String(i) });
      }
    });
  return arr;
};

// 设置数字文本格式显示
export const setNumericText = (value: any, config: any) => {
  let initConfig = {
    format: 2,
    round: true,
    commas: false,
    nullText: '-',
    zeroText: '-',
    addText: null,
  };
  config.format =
    config.format && !isNaN(config.format) ? parseInt(config.format) : 2;
  config = lodash.merge(initConfig, config);
  if (value || value === 0) {
    let newNumber;
    // 判断0的显示
    if (value === 0) {
      if (config.zeroText) {
        return config.addText
          ? config.zeroText + config.addText
          : config.zeroText;
      }
    }
    //判断遵循四舍五入还是截取，true:四舍五入
    if (config.round) {
      newNumber = (
        Math.round(value * Math.pow(10, config.format)) /
        Math.pow(10, config.format)
      ).toFixed(config.format);
    } else {
      let str = String(value);
      let index = str.indexOf('.');
      let str1 = index > -1 ? str.substring(0, index + config.format + 1) : str;
      newNumber = Number(str1);
    }
    // 千分逗号
    if (config.commas) {
      newNumber = newNumber
        .toString()
        .replace(/\d{1,3}(?=(\d{3})+(\.\d*)?$)/g, '$&,');
    }
    return config.addText ? newNumber + config.addText : newNumber;
  } else {
    return config.addText ? config.nullText + config.addText : config.nullText;
  }
};
// 金额合计
export const totalNum = (arr: any[], config: any = {}) => {
  const initConfig = {
    format: 2, //四舍五入2位
    commas: true,
    zeroText: 0,
  };
  config = lodash.merge(initConfig, config);

  // 金额做计算不要千分位
  // 金额展示千分位可选
  let num = arr.reduce((a, b) => {
    a = Number(a || 0);
    b = Number(b || 0);
    return (a += b);
  }, 0);

  let res = 0;
  if (config.commas) {
    res = setNumericText(num, {
      format: config.format,
      zeroText: config.zeroText,
      commas: config.commas,
    });
  } else {
    res = Number(
      setNumericText(num, {
        format: config.format,
        zeroText: config.zeroText,
      })
    );
  }
  return res;
};
/**
 * 时间戳转换成string
 */
export const getStringTime = (time, formatDef) => {
  let _format = formatDef || 'yyyy-MM-dd'; // yyyy-MM-dd hh:mm:ss a
  if (time && time !== 0) {
    if (typeof time === 'number') {
      return format(new Date(time), _format);
    } else {
      if (time.length === 10) {
        return format(
          new Date((time + ' 0:0:0').replace(/T/g, '/').replace(/-/g, '/')),
          _format
        );
      } else if (time.length > 10) {
        return format(
          new Date(time.replace(/T/g, '/').replace(/-/g, '/')),
          _format
        );
      }
    }
  } else {
    return time && `${time}`.toUpperCase() !== 'NULL' ? time : '';
  }
};
export const formatTimes = (date1: any) => {
  let date = new Date(date1);
  let YY = date.getFullYear() + '-';
  let MM =
    (date.getMonth() + 1 < 10
      ? '0' + (date.getMonth() + 1)
      : date.getMonth() + 1) + '-';
  let DD = date.getDate() < 10 ? '0' + date.getDate() : date.getDate();
  let hh =
    (date.getHours() < 10 ? '0' + date.getHours() : date.getHours()) + ':';
  let mm =
    (date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes()) +
    ':';
  let ss = date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds();
  return YY + MM + DD + ' ' + hh + mm + ss;
};
// 获取会员管理项目的地址

export const iframeDomin = () => {
  let iframeDomin = '';
  if (env == 'dev') {
    iframeDomin = 'http://devmember.mayizhilian.net';
  }
  if (env == 'qc') {
    iframeDomin = 'http://testmember.mayizhilian.net';
  }
  if (env == 'pre') {
    iframeDomin = 'http://premember.mayibida.com';
  }
  if (env == 'prod') {
    iframeDomin = 'https://member.mayibida.com';
  }
  return iframeDomin;
};

// 获取mos2.0项目的地址
export const iframeDominMos = () => {
  let iframeDomin = '';
  if (env == 'dev') {
    iframeDomin = 'http://devmos.mayizhilian.net';
  }
  if (env == 'qc') {
    iframeDomin = 'http://testmos.mayizhilian.net';
  }
  if (env == 'pre') {
    iframeDomin = 'https://premos.mayibida.com';
  }
  if (env == 'prod') {
    iframeDomin = 'http://mos20.mayibida.com';
  }
  return iframeDomin;
};
