export const text = [{required:true, message:'请输入', trigger: 'change'} ]
export const select = [{required:true, message:'请选择', trigger: 'change'} ]

export const RULESTYPE = {
  text,
  select
}