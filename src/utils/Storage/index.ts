import Stotage from './Storage';

export const localStorage = new Stotage({
  type: 'localStorage',
});
export const sessionStorage = new Stotage({
  type: 'sessionStorage',
});

export const localStrategie: any = {
  setItem (key: string, state: any) {
    return localStorage.setStorage(key, state)
  },
  getItem (key: string) {
    return localStorage.getStorage(key)
  },
}

export const sessionStrategie: any = {
  setItem (key: string, state: any) {
    return sessionStorage.setStorage(key, state)
  },
  getItem (key: string) {
    return sessionStorage.getStorage(key)
  },
}
