/** *
 * title: storage.js
 * Author:
 * Email: xxx@126.com
 * Time: 2022/6/15 16:30
 * last: 2022/6/15 17:30
 * Desc: 对存储的简单封装
 */

import CryptoJS from 'crypto-js';
import _ from 'lodash';
import { useStorage } from '@vueuse/core';
import packageConfig from '../../../package.json';

// 十六位十六进制数作为密钥
const SECRET_KEY = CryptoJS.enc.Utf8.parse('3333e6e143439161');
// 十六位十六进制数作为密钥偏移量
const SECRET_IV = CryptoJS.enc.Utf8.parse('e3bbe7e3ba84431a');

// 类型 window.localStorage,window.sessionStorage,
const config = {
  type: 'localStorage', // 本地存储类型 sessionStorage
  prefix: `MOS_${packageConfig.version}`, // 名称前缀 建议：项目名 + 项目版本
  expire: 0, // 过期时间 单位：秒
  isEncrypt: import.meta.env.MODE === 'prod', // 默认加密 为了调试方便, 开发过程中可以不加密
};

// 名称前自动添加前缀
const autoAddPrefix = (key: string) => {
  const prefix = config.prefix ? `${config.prefix}_` : '';
  return prefix + key;
};
// 移除已添加的前缀
const autoRemovePrefix = (key: string) => {
  const len = config.prefix ? config.prefix.length + 1 : 0;
  return key.substr(len);
  // const prefix = config.prefix ? config.prefix + '_' : '';
  // return  prefix + key;
};

/**
 * 加密方法
 * @param data
 * @returns {string}
 */
const encrypt = (data: string) => {
  if (typeof data === 'object') {
    try {
      data = JSON.stringify(data);
    } catch (error) {
      console.log('encrypt error:', error);
    }
  }
  const dataHex = CryptoJS.enc.Utf8.parse(data);
  const encrypted = CryptoJS.AES.encrypt(dataHex, SECRET_KEY, {
    iv: SECRET_IV,
    mode: CryptoJS.mode.CBC,
    padding: CryptoJS.pad.Pkcs7,
  });
  return encrypted.ciphertext.toString();
};

/**
 * 解密方法
 * @param data
 * @returns {string}
 */
const decrypt = (data: string) => {
  const encryptedHexStr = CryptoJS.enc.Hex.parse(data);
  const str = CryptoJS.enc.Base64.stringify(encryptedHexStr);
  const decryptAES = CryptoJS.AES.decrypt(str, SECRET_KEY, {
    iv: SECRET_IV,
    mode: CryptoJS.mode.CBC,
    padding: CryptoJS.pad.Pkcs7,
  });
  const decryptedStr = decryptAES.toString(CryptoJS.enc.Utf8);
  return decryptedStr.toString();
};

export default class Storages {
  config: any;

  constructor(c: any) {
    this.config = _.merge(_.cloneDeep(config), c);
  }

  // 判断是否支持 Storage
  isSupportStorage() {
    return typeof Storage !== 'undefined';
  }

  // 删除 removeStorage
  removeStorage(key: string) {
    (window as any)[this.config.type].removeItem(autoAddPrefix(key));
  }

  // 清空 clearStorage
  clearStorage() {
    (window as any)[this.config.type].clear();
  }

  // 设置 setStorage
  setStorage(key: string, value: any, expire = 0) {
    if (value === '' || value === null || value === undefined) {
      value = null;
    }

    if (Number.isNaN(expire) || expire < 0)
      throw new Error('Expire must be a number');

    expire = (expire || this.config.expire) * 1000;
    const data = {
      value, // 存储值
      time: Date.now(), // 存值时间戳
      expire, // 过期时间
    };
    const useStorageValue = useStorage(
      autoAddPrefix(key),
      data,
      (window as any)[this.config.type],
      {
        serializer: {
          read: (v: any) => {
            const decryptString = this.config.isEncrypt
              ? JSON.parse(decrypt(v))
              : JSON.parse(v);
            return v ? decryptString : null;
          },
          write: (v: any) => {
            return this.config.isEncrypt
              ? encrypt(JSON.stringify(v))
              : JSON.stringify(v);
          },
        },
      }
    );
    return useStorageValue.value;
  }

  // 获取 getStorage
  getStorage(key: string) {
    key = autoAddPrefix(key);
    // key 不存在判断
    if (
      !(window as any)[this.config.type].getItem(key) ||
      JSON.stringify((window as any)[this.config.type].getItem(key)) === 'null'
    ) {
      return null;
    }

    // 优化 持续使用中续期
    const storage = this.config.isEncrypt
      ? JSON.parse(decrypt((window as any)[this.config.type].getItem(key)))
      : JSON.parse((window as any)[this.config.type].getItem(key));
    const nowTime = Date.now();

    // 过期删除
    if (storage.expire && this.config.expire * 6000 < nowTime - storage.time) {
      this.removeStorage(key);
      return null;
    }
    // 未过期期间被调用 则自动续期 进行保活
    const { value: storageValue} = this.setStorage(autoRemovePrefix(key), storage.value);
    return storageValue;
  }

  // 获取全部 getAllStorage
  getStorageAll() {
    const len = (window as any)[this.config.type].length; // 获取长度
    const arr = []; // 定义数据集
    for (let i = 0; i < len; i++) {
      // 获取key 索引从0开始
      const getKey = (window as any)[this.config.type].key(i);
      // 获取key对应的值
      const getVal = (window as any)[this.config.type].getItem(getKey);
      // 放进数组
      arr[i] = { key: getKey, val: getVal };
    }
    return arr;
  }

  // 是否存在 hasStorage
  hasStorage(key: string) {
    key = autoAddPrefix(key);
    const arr = this.getStorageAll().filter((item) => {
      return item.key === key;
    });
    return !!arr.length;
  }

  // 获取所有key
  getStorageKeys() {
    const items = this.getStorageAll();
    const keys = [];
    for (let index = 0; index < items.length; index++) {
      keys.push(items[index].key);
    }
    return keys;
  }

  // 根据索引获取key
  getStorageForIndex(index: number) {
    return (window as any)[this.config.type].key(index);
  }

  // 获取localStorage长度
  getStorageLength() {
    return (window as any)[this.config.type].length;
  }
}
