export default {
  handleParams(params:any, data:any) {
    Object.keys(params).forEach((key) => {
      params[key] = key in data ? data[key] : params[key]
    })
    return params
  },

  handleLabelByValue(option:any, v:any, value = 'value', label = 'label') {
    const res = option.find((i:any) => i[value] == v)

    return res ? res[label] : ''
  },

  handleOptionByProp(prop:any, data:any) {
    return data.find((v:any) => v.prop === prop).props.options
  },
}
