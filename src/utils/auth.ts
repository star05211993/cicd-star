import { localStorage } from '@/utils/Storage';
import { TOKEN } from '@/constant';

const isLogin = () => {
  return localStorage.hasStorage(TOKEN);
};

const getToken = () => {
  return localStorage.getStorage(TOKEN);
};

const setToken = (token: string) => {
  localStorage.setStorage(TOKEN, token);
  
};

const clearToken = () => {
  localStorage.removeStorage(TOKEN);
};

export { isLogin, getToken, setToken, clearToken };
