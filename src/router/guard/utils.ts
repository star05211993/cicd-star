import NProgress from 'nprogress'; // progress bar
import { DEFAULT_TITLE } from "@/constant";
import usePermission from "@/hooks/permission";
import { useRouterStore, useTentantStore, useUserStore } from "@/store";
import { appRoutes } from "../routes";

export async function crossroads(to:any, from:any, next:any) {
  const userStore = useUserStore();
  const Permission = usePermission();
  if (Permission.accessRouter(to)) next();
  else {
    const destination = Permission.findFirstPermissionRoute(
      appRoutes,
      userStore.identityCode
    ) || {
      name: 'notFound',
    };
    next(destination);
  }
  NProgress.done();
}

// 请求租户信息 and 设置document.title
export async function setDocumentTitle (to: any) {
  const useTentant = useTentantStore()
  
  if (!useTentant.isTentant) { }
    // 租户企业信息
    await useTentant.getTenantInfo();
 
  
  if (to.meta.title === DEFAULT_TITLE) {
    document.title = useTentant.tenantAbbreviation || to.meta.title
  } else {
    document.title = (to.meta.title) as any
  }
}
// 请求路由
export async function getUserRouters () {
  const useTentant = useTentantStore()
  const params = {
    applicationType: useTentant.applicationType
  }
  
  const useRouter = useRouterStore();
  if (!useRouter.isRouter) {
    await useRouter.info(params)
  }
}