import type { Router, LocationQueryRaw } from 'vue-router';
import NProgress from 'nprogress'; // progress bar

import { useUserStore } from '@/store';
import { isLogin } from '@/utils/auth';
import { crossroads, getUserRouters, setDocumentTitle } from './utils';

export default function setupPermissionGuard(router: Router) {
  router.beforeEach(async (to, from, next) => {
    NProgress.start();
    const userStore = useUserStore();
    // 设置title
    await setDocumentTitle(to);
    if (isLogin()) {
      // 请求菜单数据
      await getUserRouters();
      // TODO: 权限处理
      // next();
      if (userStore.identityCode) {
        await userStore.info();
        crossroads(to, from, next);
      } else {
        try {
          crossroads(to, from, next);
        } catch (error) {
          next({
            name: 'login',
            query: {
              redirect: to.name,
              ...to.query,
            } as LocationQueryRaw,
          });
          NProgress.done();
        }
      }
    } else {
      //不需要重定向的路由路径
      if (
        to.name === 'login' ||
        to.name === 'forgetPassword' ||
        to.name === 'loginOutSide'
      ) {
        next();
        NProgress.done();
        return;
      }
      // next();
      next({
        name: 'login',
        query: {
          redirect: to.name,
          ...to.query,
        } as LocationQueryRaw,
      });
      NProgress.done();
    }
  });
}
