import { DEFAULT_LAYOUT } from '@/router/constans';

export default {
    path: '/mos',
    name: 'email',
    component: DEFAULT_LAYOUT,
    meta: {
        locale: 'menu.dashboard',
        requiresAuth: true,
        icon: 'icon-dashboard',
        order: 0,
    },
    children: [ 
        {
          path: 'examineEmailList',
          name: 'examineEmail',
          component: () => import('@/views/examineEmail/index.vue'),
          // component: () => import('@/views/merchants/index.vue'),
          meta: {
            locale: '审单邮件',
            requiresAuth: true,
            roles: ['*'],
            ignoreCache: false,
            title:'审单邮件'
          },
        }, 

    ],
};
