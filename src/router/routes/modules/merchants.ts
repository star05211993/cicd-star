import { DEFAULT_LAYOUT } from '@/router/constans';

export default {
    path: '/mos',
     name: 'merchants',
    component: DEFAULT_LAYOUT,
    meta: {
        locale: 'menu.dashboard',
        requiresAuth: true,
        icon: 'icon-dashboard',
        order: 0,
    },
    children: [ 
      {
        path: 'merchants',
        name: 'merchantsPage',
        component: () => import('@/views/merchants/index.vue'),
        meta: {
          locale: '客商列表',
          requiresAuth: true,
          roles: ['*'],
          ignoreCache: false,
          title: '客商列表'
        },
      },
      {
        path: 'merchants/add',
        name: 'merchantsAdd',
        component: () => import('@/views/merchants/add.vue'),
        meta: {
          locale: '新增企业',
          requiresAuth: true,
          roles: ['*'],
          ignoreCache: false,
          title: '新增企业'
        },
      },
      {
        path: 'merchants/detail',
        name: 'merchantsDetail',
        component: () => import('@/views/merchants/detail.vue'),
        meta: {
          locale: '企业详情',
          requiresAuth: true,
          roles: ['*'],
          ignoreCache: false,
          title: '企业详情'
        },
      },
    ],
};
