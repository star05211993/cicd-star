import { DEFAULT_LAYOUT } from '@/router/constans';

export default {
    path: '/mos',
    name: 'examine',
    component: DEFAULT_LAYOUT,
    meta: {
        locale: 'menu.dashboard',
        requiresAuth: true,
        icon: 'icon-dashboard',
        order: 0,
    },
    children: [ 
        {
          path: 'examineOrder',
          name: 'examineOrder',
          component: () => import('@/views/examineOrder/index.vue'),
          // component: () => import('@/views/merchants/index.vue'),
          meta: {
            locale: '审单跟踪',
            requiresAuth: true,
            roles: ['*'],
            ignoreCache: false,
            title:'审单跟踪'
          },
        }, 
        {
          path: 'examineOrderDetail',
          name: 'examineOrderDetail',
          component: () => import('@/views/examineOrder/detail.vue'),
          // component: () => import('@/views/merchants/index.vue'),
          meta: {
            locale: '审单详情',
            requiresAuth: true,
            roles: ['*'],
            ignoreCache: false,
            title:'审单详情'
          },
        }, 
    ],
};
