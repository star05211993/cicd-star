import { DEFAULT_LAYOUT } from '@/router/constans';

export default {
  path: '/mos',
  name: 'mosContract',
  component: DEFAULT_LAYOUT,
  meta: {
    locale: 'menu.dashboard',
    requiresAuth: true,
    icon: 'icon-dashboard',
    order: 0,
  },
  children: [
    {
      path: 'CRM/contract/customerContract/list',
      name: 'contractManage',
      component: () => import('@/views/contractManage/index.vue'),
      meta: {
        locale: '客户合同列表',
        requiresAuth: true,
        roles: ['*'],
        ignoreCache: false,
        title: '客户合同列表',
      }
    },
    {
      path: 'CRM/contract/list/mutualAgreement',
      name: 'CRM/contract/list/mutualAgreement',
      component: () => import('@/views/contractManage/contracts/mutualAgreement/index.vue'),
      meta: {
        locale: '双方协议',
        requiresAuth: true,
        roles: ['*'],
        ignoreCache: false,
        title: '双方协议',
      }
    },
    {
      path: 'CRM/contract/list/tripartiteAgreement',
      name: 'CRM/contract/list/tripartiteAgreement',
      component: () => import('@/views/contractManage/contracts/tripartiteAgreement/index.vue'),
      meta: {
        locale: '三方协议',
        requiresAuth: true,
        roles: ['*'],
        ignoreCache: false,
        title: '三方协议',
      }
    },
    {
      path: 'CRM/contract/list/agreementParites',
      name: 'CRM/contract/list/agreementParites',
      component: () => import('@/views/contractManage/contracts/agreementParites/index.vue'),
      meta: {
        locale: '控货双方协议',
        requiresAuth: true,
        roles: ['*'],
        ignoreCache: false,
        title: '控货双方协议',
      }
    },
    {
      path: 'CRM/contract/list/formalAgreement',
      name: 'CRM/contract/list/formalAgreement',
      component: () => import('@/views/contractManage/contracts/formalAgreement/index.vue'),
      meta: {
        locale: '形式协议',
        requiresAuth: true,
        roles: ['*'],
        ignoreCache: false,
        title: '形式协议',
      }
    },
    {
      path: 'CRM/contract/list/supplementaryAgreement',
      name: 'CRM/contract/list/supplementaryAgreement',
      component: () => import('@/views/contractManage/contracts/supplementaryAgreement/index.vue'),
      meta: {
        locale: '补充协议/新增报价',
        requiresAuth: true,
        roles: ['*'],
        ignoreCache: false,
        title: '补充协议/新增报价',
      }
    },
    {
      path: 'CRM/contract/list/tryAgreement',
      name: 'CRM/contract/list/tryAgreement',
      component: () => import('@/views/contractManage/contracts/tryAgreement/index.vue'),
      meta: {
        locale: '试做报价',
        requiresAuth: true,
        roles: ['*'],
        ignoreCache: false,
        title: '试做报价',
      }
    },
    
    {
      path: 'CRM/contract/list',
      name: 'CRM/contract/list',
      component: () => import('@/views/contractManage/contracts/createOffer/index.vue'),
      meta: {
        locale: '创建报价书',
        requiresAuth: true,
        roles: ['*'],
        ignoreCache: false,
        title: '创建报价书',
      }
    },
    // {
    //   path: 'CRM/contract/list/tripartiteAgreement',
    //   name: 'CRM/contract/list/tripartiteAgreement',
    //   component: () => import('@/views/contractManage/contracts/tripartiteAgreement/index.vue'),
    //   meta: {
    //     locale: '三方协议',
    //     requiresAuth: true,
    //     roles: ['*'],
    //     ignoreCache: false,
    //     title: '三方协议',
    //   }
    // },
    // {
    //   path: 'CRM/contract/list/agreementParites',
    //   name: 'CRM/contract/list/agreementParites',
    //   component: () => import('@/views/contractManage/contracts/agreementParites/index.vue'),
    //   meta: {
    //     locale: '控货双方协议',
    //     requiresAuth: true,
    //     roles: ['*'],
    //     ignoreCache: false,
    //     title: '控货双方协议',
    //   }
    // },
    // {
    //   path: 'CRM/contract/list/formalAgreement',
    //   name: 'CRM/contract/list/formalAgreement',
    //   component: () => import('@/views/contractManage/contracts/formalAgreement/index.vue'),
    //   meta: {
    //     locale: '形式协议',
    //     requiresAuth: true,
    //     roles: ['*'],
    //     ignoreCache: false,
    //     title: '形式协议',
    //   }
    // },
    // {
    //   path: 'CRM/contract/list/supplementaryAgreement',
    //   name: 'CRM/contract/list/supplementaryAgreement',
    //   component: () => import('@/views/contractManage/contracts/supplementaryAgreement/index.vue'),
    //   meta: {
    //     locale: '补充协议/新增报价',
    //     requiresAuth: true,
    //     roles: ['*'],
    //     ignoreCache: false,
    //     title: '补充协议/新增报价',
    //   }
    // },
    // {
    //   path: 'CRM/contract/list/tryAgreement',
    //   name: 'CRM/contract/list/tryAgreement',
    //   component: () => import('@/views/contractManage/contracts/tryAgreement/index.vue'),
    //   meta: {
    //     locale: '试做报价',
    //     requiresAuth: true,
    //     roles: ['*'],
    //     ignoreCache: false,
    //     title: '试做报价',
    //   }
    // },
    {
      path: 'CRM/contract/list/mutualAgreement/mutualDetail',
      name: 'CRM/contract/list/mutualAgreement/mutualDetail',
      component: () => import('@/views/contractManage/contracts/mutualAgreement/mutualDetail/index.vue'),
      meta: {
        locale: '合同详情',
        requiresAuth: true,
        roles: ['*'],
        ignoreCache: false,
        title: '合同详情',
      }
    },
    {
      path: 'CRM/contract/list/details',
      name: 'CRM/contract/list/details',
      component: () => import('@/views/contractManage/contracts/mutualAgreement/offerDetail/index.vue'),
      meta: {
        locale: '报价书详情',
        requiresAuth: true,
        roles: ['*'],
        ignoreCache: false,
        title: '报价书详情',
      }
    },
    {
      path: 'CRM/contract/template',
      name: 'CRM/contract/template',
      component: () => import('@/views/contractManage/components/template/index.vue'),
      meta: {
        locale: '合同模板',
        requiresAuth: true,
        roles: ['*'],
        ignoreCache: false,
        title: '合同模板',
      }
    },
  ],
};
