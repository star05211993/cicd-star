import { DEFAULT_LAYOUT } from '@/router/constans';

export default {
  path: '/mos',
  name: 'mosOrder',
  component: DEFAULT_LAYOUT,
  meta: {
    locale: 'menu.dashboard',
    requiresAuth: true,
    icon: 'icon-dashboard',
    order: 0,
  },
  children: [
    {
      path: 'runOrder',
      name: 'runOrder',
      component: () => import('@/views/runOrder/list/index.vue'),
      meta: {
        locale: '运单列表',
        requiresAuth: true,
        roles: ['*'],
        ignoreCache: false,
        title: '运单列表',
      },
    },
    {
      path: 'newOrderBill',
      name: 'newOrderBill',
      component: () => import('@/views/runOrder/newOrderBill/index.vue'),
      meta: {
        locale: '新建运单',
        requiresAuth: true,
        roles: ['*'],
        ignoreCache: false,
        title: '新建运单',
      },
    },
    {
      path: 'changeOrder',
      name: 'changeOrder',
      component: () => import('@/views/changeOrder/index.vue'),
      meta: {
        locale: '换单列表',
        requiresAuth: true,
        roles: ['*'],
        ignoreCache: false,
        title: '换单列表',
      },
    },
    {
      path: 'changeOrder/detail',
      name: 'changeOrderDetail',
      component: () => import('@/views/changeOrder/detail.vue'),
      meta: {
        locale: '换单详情',
        requiresAuth: true,
        roles: ['*'],
        ignoreCache: false,
        title: '换单详情',
      },
    },
    {
      path: 'receiptDocuments',
      name: 'receiptDocuments',
      component: () =>
        import('@/views/runOrder/receiptDocuments/list/index.vue'),
      meta: {
        locale: '单证签收',
        requiresAuth: true,
        roles: ['*'],
        ignoreCache: false,
        title: '单证签收',
      },
    },
    {
      path: 'unclaimedRunOrder',
      name: 'unclaimedRunOrder',
      component: () =>
        import('@/views/runOrder/unReceiveOrder/list/index.vue'),
      meta: {
        locale: '待领取运单',
        requiresAuth: true,
        roles: ['*'],
        ignoreCache: false,
        title: '待领取运单',
      },
    },
    {
      path: 'containerTrack',
      name: 'containerTrack',
      component: () => import('@/views/runOrder/containerTrack/index.vue'),
      meta: {
        locale: '集装箱跟踪',
        requiresAuth: true,
        roles: ['*'],
        ignoreCache: false,
        title: '集装箱跟踪',
      },
    },
    {
      path: 'standingBook',
      name: 'standingBook',
      component: () => import('@/views/runOrder/standingBook/index.vue'),
      meta: {
        locale: '客户台账',
        requiresAuth: true,
        roles: ['*'],
        ignoreCache: false,
        title: '客户台账',
      },
    },
    {
      path: 'standingBookTemplate',
      name: 'standingBookTemplate',
      component: () =>
        import('@/views/runOrder/setUps/standingBookTemplate/index.vue'),
      meta: {
        locale: '客户台账模板',
        requiresAuth: true,
        roles: ['*'],
        ignoreCache: false,
        title: '客户台账模板',
      },
    },

    {
      path: 'setTask',
      name: 'setTask',
      component: () =>
        import('@/views/runOrder/setUps/setTask/index.vue'),
      meta: {
        locale: '任务配置',
        requiresAuth: true,
        roles: ['*'],
        ignoreCache: false,
        title: '任务配置',
      },
    },
    {
      path: 'setAbnormal',
      name: 'setAbnormal',
      component: () =>
        import('@/views/runOrder/setUps/setAbnormal/index.vue'),
      meta: {
        locale: '异常配置',
        requiresAuth: true,
        roles: ['*'],
        ignoreCache: false,
        title: '异常配置',
      },
    },
    {
      path: 'signQuarantine',
      name: 'signQuarantine',
      component: () =>
        import('@/views/runOrder/receiptDocuments/list/index.vue'),
      meta: {
        locale: '单证签收',
        requiresAuth: true,
        roles: ['*'],
        ignoreCache: false,
        title: '单证签收',
      },
    },
  ],
};
