import { DEFAULT_LAYOUT } from '@/router/constans';
export default {
  path: '/mos',
  name: 'mosTask',
  component: DEFAULT_LAYOUT,
  meta: {
    locale: 'menu.dashboard',
    requiresAuth: true,
    icon: 'icon-dashboard',
    order: 0,
  },
  children: [
    {
      path: 'task',
      name: 'task',
      component: () => import('@/views/task/taskPending/index.vue'),
      meta: {
        locale: '待办任务',
        requiresAuth: true,
        roles: ['*'],
        ignoreCache: false,
        title: '待办任务',
      },
    },
    {
      path: 'historytask',
      name: 'historytask',
      component: () => import('@/views/task/taskHistory/index.vue'),
      meta: {
        locale: '历史任务',
        requiresAuth: true,
        roles: ['*'],
        ignoreCache: false,
        title: '历史任务',
      },
    },
  ],
};
