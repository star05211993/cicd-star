import { DEFAULT_LAYOUT } from '@/router/constans';

export default {
  path: '/mos',
  name: 'mosDashboard',
  component: DEFAULT_LAYOUT,
  meta: {
    locale: 'menu.dashboard',
    requiresAuth: true,
    icon: 'icon-dashboard',
    order: 0,
  },
  children: [
    {
      path: 'dashboard',
      name: 'dashboard',
      component: () => import('@/views/dashboard/workplace/index.vue'),
      meta: {
        locale: '概览',
        requiresAuth: true,
        roles: ['*'],
        ignoreCache: false,
        title:'概览'
      },
    },
    
  ],
};
