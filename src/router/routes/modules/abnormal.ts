import { DEFAULT_LAYOUT } from '@/router/constans';

export default {
  path: '/mos',
  name: 'mosAbnormal',
  component: DEFAULT_LAYOUT,
  meta: {
    locale: 'menu.dashboard',
    requiresAuth: true,
    icon: 'icon-dashboard',
    order: 0,
  },
  children: [
    {
      path: 'abnormal',
      name: 'abnormal',
      component: () => import('@/views/MOS/abnormal/index.vue'),
      meta: {
        locale: '异常看板',
        requiresAuth: true,
        roles: ['*'],
        ignoreCache: false,
        title: '异常看板',
      },
    },
  ],
};
