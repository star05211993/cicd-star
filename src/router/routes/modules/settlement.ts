import { DEFAULT_LAYOUT } from '@/router/constans';
export default {
  path: '/mos',
  name: 'mosSettlement',
  component: DEFAULT_LAYOUT,
  meta: {
    locale: 'menu.dashboard',
    requiresAuth: true,
    icon: 'icon-dashboard',
    order: 0,
  },
  children: [
    {
      path: 'refund',
      name: 'refund',
      component: () => import('@/views/settlement/refund/index.vue'),
      meta: {
        locale: '退款管理',
        requiresAuth: true,
        roles: ['*'],
        ignoreCache: false,
        title: '退款管理',
      },
    },
    {
      path: 'refund/detail',
      name: 'refund/detail',
      component: () => import('@/views/settlement/refund/detail.vue'),
      meta: {
        locale: '退款详情',
        requiresAuth: true,
        roles: ['*'],
        ignoreCache: false,
        title: '退款详情',
      },
    },
    {
      path: 'loan',
      name: 'loan',
      component: () => import('@/views/settlement/loan/index.vue'),
      meta: {
        locale: '借款管理',
        requiresAuth: true,
        roles: ['*'],
        ignoreCache: false,
        title: '借款管理',
      },
    },
    {
      path: 'loan/detail',
      name: 'loan/detail',
      component: () => import('@/views/settlement/loan/detail.vue'),
      meta: {
        locale: '借款详情',
        requiresAuth: true,
        roles: ['*'],
        ignoreCache: false,
        title: '借款详情',
      },
    },
    {
      path: 'reimburse',
      name: 'reimburse',
      component: () => import('@/views/settlement/reimburse/index.vue'),
      meta: {
        locale: '报销管理',
        requiresAuth: true,
        roles: ['*'],
        ignoreCache: false,
        title: '报销管理',
      },
    },
    {
      path: 'reimburse/detail',
      name: 'reimburse/detail',
      component: () => import('@/views/settlement/reimburse/detail.vue'),
      meta: {
        locale: '报销详情',
        requiresAuth: true,
        roles: ['*'],
        ignoreCache: false,
        title: '报销详情',
      },
    },
    {
      path: 'reimburse/apply',
      name: 'reimburse/apply',
      component: () => import('@/views/settlement/reimburse/apply.vue'),
      meta: {
        locale: '报销申请',
        requiresAuth: true,
        roles: ['*'],
        ignoreCache: false,
        title: '报销申请',
      },
    },
    {
      path: 'reimburse/costEnty',
      name: 'reimburse/costEnty',
      component: () => import('@/views/settlement/reimburse/costEnty.vue'),
      meta: {
        locale: '成本录入',
        requiresAuth: true,
        roles: ['*'],
        ignoreCache: false,
        title: '成本录入',
      },
    },
    {
      path: 'reimburse/costFeeQuery',
      name: 'reimburse/costFeeQuery',
      component: () => import('@/views/settlement/reimburse/costFeeQuery.vue'),
      meta: {
        locale: '成本费用查询',
        requiresAuth: true,
        roles: ['*'],
        ignoreCache: false,
        title: '成本费用查询',
      },
    },
    {
      path: 'reimburse/rpa',
      name: 'reimburse/rpa',
      component: () => import('@/views/settlement/reimburse/rpa.vue'),
      meta: {
        locale: 'RPA费用管理',
        requiresAuth: true,
        roles: ['*'],
        ignoreCache: false,
        title: 'RPA费用管理',
      },
    },
    {
      path: 'reimburse/approve',
      name: 'reimburse/approve',
      component: () => import('@/views/settlement/reimburse/approve/index.vue'),
      meta: {
        locale: '供应商成本审批',
        requiresAuth: true,
        roles: ['*'],
        ignoreCache: false,
        title: '供应商成本审批',
      },
    },
    {
      path: 'settleBasicData/exchangeRateManage',
      name: 'exchangeRateManage',
      component: () =>
        import('@/views/settlement/exchangeRateManage/index.vue'),
      meta: {
        locale: '汇率管理',
        requiresAuth: true,
        roles: ['*'],
        ignoreCache: false,
        title: '汇率管理',
      },
    },
    {
      path: 'settleBasicData/invoiceItemConfig',
      name: 'invoiceItemConfig',
      component: () => import('@/views/settlement/invoiceItemConfig/index.vue'),
      meta: {
        locale: '发票项目配置表',
        requiresAuth: true,
        roles: ['*'],
        ignoreCache: false,
        title: '发票项目配置表',
      },
    },
    {
      path: 'settleBasicData/invoiceProductConfig',
      name: 'invoiceProductConfig',
      component: () =>
        import('@/views/settlement/invoiceProductConfig/index.vue'),
      meta: {
        locale: '发票产品',
        requiresAuth: true,
        roles: ['*'],
        ignoreCache: false,
        title: '发票产品',
      },
    },
    {
      path: 'settleSignatureFlow',
      name: 'settleSignatureFlow',
      component: () =>
        import('@/views/settlement/settleSignatureFlow/index.vue'),
      meta: {
        locale: '配置结算签字流',
        requiresAuth: true,
        roles: ['*'],
        ignoreCache: false,
        title: '配置结算签字流',
      },
    },
    // settleBasicData/invoiceItemConfig
    // {
    //   path: 'fundRequest',
    //   name: 'fundRequest',
    //   component: () => import('@/views/settlement/fundRequest/'),
    //   meta: {
    //     locale: '请款管理',
    //     requiresAuth: true,
    //     roles: ['*'],
    //     ignoreCache: false,
    //     title: '请款管理',
    //   },
    // },
    //fundRequest

    {
      path: 'invoiceManage',
      name: 'invoiceManage',
      component: () => import('@/views/settlement/invoiceManage/index.vue'),
      meta: {
        locale: '发票管理',
        requiresAuth: true,
        roles: ['*'],
        ignoreCache: false,
        title: '发票管理',
      },
    },

    {
      path: 'invoiceManage/requestInvoice',
      name: 'invoiceManage/requestInvoice',
      component: () =>
        import('@/views/settlement/invoiceManage/requestInvoice/index.vue'),
      meta: {
        locale: '申请开票',
        requiresAuth: true,
        roles: ['*'],
        ignoreCache: false,
        title: '申请开票',
      },
    },
    {
      path: 'invoiceManage/detail',
      name: 'invoiceManage/detail',
      component: () => import('@/views/settlement/invoiceManage/invoiceDetail/index.vue'),
      meta: {
        locale: '发票详情',
        requiresAuth: true,
        roles: ['*'],
        ignoreCache: false,
        title: '发票详情',
      },
    },
    {
      path: 'invoiceManage/pool',
      name: 'invoiceManage/pool',
      component: () => import('@/views/settlement/invoiceManage/pool.vue'),
      meta: {
        locale: '发票池',
        requiresAuth: true,
        roles: ['*'],
        ignoreCache: false,
        title: '发票池',
      },
    },
  ],
};
