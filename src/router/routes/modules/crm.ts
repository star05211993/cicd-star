import { DEFAULT_LAYOUT } from '@/router/constans';

export default {
    path: '/mos',
    name: 'crm1',
    component: DEFAULT_LAYOUT,
    meta: {
        locale: '概览',
        requiresAuth: true,
        icon: 'icon-dashboard',
        order: 0,
    },
    children: [ 
      {
        path: 'CRM/costCenter',
        name: 'costCenter',
        component: () => import('@/views/costCenter/index.vue'),
        meta: {
          locale: '成本维护',
          requiresAuth: true,
          roles: ['*'],
          ignoreCache: false,
          title:'成本维护'
        },
      },
      {
        path: 'CRM/standardProduct/index',
        name: 'standardProduct',
        component: () => import('@/views/CRMIndex/standardProduct/index.vue'),
        meta: {
          locale: '标准产品',
          requiresAuth: true,
          roles: ['*'],
          ignoreCache: false,
          title:'标准产品'
        },
      },
      {
        path: 'CRM/standardProduct/add',
        name: 'standardProductAdd',
        component: () => import('@/views/CRMIndex/standardProduct/add.vue'),
        meta: {
          locale: '新增/编辑标准产品',
          requiresAuth: true,
          roles: ['*'],
          ignoreCache: false,
          title:'新增/编辑标准产品'
        },
      },
      {
        path: 'CRM/contract/supplier/list',
        name: 'supplierList',
        component: () => import('@/views/contractManage/supplier/index.vue'),
        meta: {
          locale: '供应商合同列表',
          requiresAuth: true,
          roles: ['*'],
          ignoreCache: false,
          title:'供应商合同列表'
        },
      },
      // 供应商双方协议
      {
        path: 'CRM/contract/supplier/signing',
        name: 'supplierSign',
        component: () => import('@/views/contractManage/supplier/signing.vue'),
        meta: {
          locale: '供应商合同签约',
          requiresAuth: true,
          roles: ['*'],
          ignoreCache: false,
          title:'供应商合同签约'
        },
      },
      {
        path: 'CRM/contract/supplier/supplementalDetails',
        name: 'supplierReplenishPactionDetails',
        component: () => import('@/views/contractManage/supplier/supplementalDetails.vue'),
        meta: {
          locale: '供应商双方协议合同详情',
          requiresAuth: true,
          roles: ['*'],
          ignoreCache: false,
          title:'供应商双方协议合同详情'
        },
      },
      // 供应商补充协议
      {
        path: 'CRM/contract/supplier/replenishPaction',
        name: 'supplierReplenishPaction',
        component: () => import('@/views/contractManage/supplier/replenishPaction.vue'),
        meta: {
          locale: '供应商合同签约',
          requiresAuth: true,
          roles: ['*'],
          ignoreCache: false,
          title:'供应商合同签约'
        },
      },
      {
        path: 'CRM/contract/supplier/supplierPactDetails',
        name: 'supplierSignDetails',
        component: () => import('@/views/contractManage/supplier/supplierPactDetails.vue'),
        meta: {
          locale: '供应商合同详情',
          requiresAuth: true,
          roles: ['*'],
          ignoreCache: false,
          title:'供应商合同详情'
        },
      },
      
      {
        path: 'CRM/cargoManage/manageUnit',
        name: 'cargoManageUnit',
        component: () => import('@/views/cargoManages/cargoManageUnit/index.vue'),
        meta: {
          locale: '经营单位控货管理',
          requiresAuth: true,
          roles: ['*'],
          ignoreCache: false,
          title: '经营单位控货管理',
        },
      },  
      {
        path: 'CRM/cargoManage',
        name: 'cargoManage',
        component: () => import('@/views/cargoManages/cargoManage/index.vue'),
        meta: {
          locale: '控货管理',
          requiresAuth: true,
          roles: ['*'],
          ignoreCache: false,
          title: '控货管理'
        },
      }, 
      {
        path: 'CRM/cargoManage/receivable',
        name: 'cargoManageReceivable',
        component: () => import('@/views/cargoManages/cargoManageReceivable/index.vue'),
        meta: {
          locale: '应收控货管理',
          requiresAuth: true,
          roles: ['*'],
          ignoreCache: false,
          title: '应收控货管理'
        },
      }, 
      {
        path: 'CRM/cargoManage/seaTransportation',
        name: 'cargoManageSea',
        component: () => import('@/views/cargoManages/cargoManageSea/index.vue'),
        meta: {
          locale: '海运控货管理',
          requiresAuth: true,
          roles: ['*'],
          ignoreCache: false,
          title: '海运控货管理'
        },
      }, 
      {
        path: 'CRM/approve',
        name: 'approve',
        component: () => import('@/views/CRM/components/approve.vue'),
        meta: {
          locale: '审批',
          requiresAuth: true,
          roles: ['*'],
          ignoreCache: false,
          title: '审批'
        },
      }, 

    ],
};
