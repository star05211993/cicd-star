import { DEFAULT_LAYOUT } from '@/router/constans';
export default {
  path: '/mos',
  name: 'mos',
  component: DEFAULT_LAYOUT,
  meta: {
    locale: 'menu.dashboard',
    requiresAuth: true,
    icon: 'icon-dashboard',
    order: 0,
  },
  children: [
    {
      path: 'declaration-track',
      name: 'declaration-track',
      component: () => import('@/views/customs/declarationTrack/index.vue'),
      meta: {
        locale: '申报追踪',
        requiresAuth: true,
        roles: ['*'],
        ignoreCache: false,
        title: '申报追踪',
      },
    },
    {
      path: 'order-preparation-track',
      name: 'order-preparation-track',
      component: () => import('@/views/customs/orderPreparationTrack/index.vue'),
      meta: {
        locale: '制单跟踪',
        requiresAuth: true,
        roles: ['*'],
        ignoreCache: false,
        title: '制单跟踪',
      },
    },
  ],
};
