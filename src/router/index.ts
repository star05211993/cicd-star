import { createRouter, createWebHistory } from 'vue-router';

import NProgress from 'nprogress'; // progress bar
import 'nprogress/nprogress.css';

import { appRoutes } from './routes';
import createRouteGuard from './guard';

NProgress.configure({ showSpinner: false }); // NProgress Configuration

const router = createRouter({
  history: createWebHistory(),
  routes: [
    {
      path: '/',
      redirect: 'login',
    },
    {
      path: '/login',
      name: 'login',
      component: () => import('@/views/login/index.vue'),
      meta: {
        requiresAuth: false,
        title: '蚂蚁必达',
      },
    },
    {
      path: '/loginOutSide',
      name: 'loginOutSide',
      component: () => import('@/views/login/components/loginOutSide.vue'),
      meta: {
        requiresAuth: false,
        title: '蚂蚁必达',
      },
    },
    {
      path: '/register',
      name: 'register',
      component: () => import('@/views/login/register.vue'),
      meta: {
        requiresAuth: false,
        title: '蚂蚁必达',
      }
    },
    {
      path: '/gocertification',
      name: 'gocertification',
      component: () => import('@/views/members/enterprisesSetting/index.vue'),
      meta: {
        requiresAuth: false,
        title: '蚂蚁必达',
      }
    },
      {
      path: '/forgetPassword',
      name: 'forgetPassword',
      component: () => import('@/views/login/forgetPassword/index.vue'),
      meta: {
        requiresAuth: false,
        title: '忘记密码',
      },
    },
    ...appRoutes,
    {
      path: '/:pathMatch(.*)*',
      name: 'notFound',
      component: () => import('@/views/not-found/index.vue'),
    },
    

  ],

  scrollBehavior() {
    return { top: 0 };
  },
});

createRouteGuard(router);
export default router;
