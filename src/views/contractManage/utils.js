
import { computed } from 'vue'
import { useTabBarStore } from '@/store';
import router from '@/router';
const fullPath = computed(()=>router.currentRoute.value.fullPath);
const tabBarStore = useTabBarStore();
const tagList = computed(() => {
  return tabBarStore.getTabList;
});
// 关闭当前页面
export const closePage = () => {
  let idx;
  tagList.value.find((item, index) => {
    if (item.fullPath === fullPath.value) {
      idx = index;
      return true;
    }
  });
  tabBarStore.deleteTag(idx, fullPath.value);
  if (idx === tagList.value.length) {
    const latest = tagList.value[tagList.value.length - 1];
    router.push({ ...latest });
  }
}
