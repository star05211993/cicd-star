//! 根据标签名 currentLabel 去匹配下拉框
//! list 可接受 list 有三种类型 []固定的下拉， function需要处理的下拉，
// !!!特别注意，目前不可单独使用 必须在dwForm配合下
import {
  mappingUrl,
  handleAjaxOption,
  handleList,
} from '@/api/base/allSelectorTypes';
import { ref, watch } from 'vue';
import { allSelector } from '@/api/base/allSelector';
import { cloneDeep, isArray, isFunction } from 'lodash';
export default {
  name: 'DwSelect',
  inheritAttrs: false,
  props: {
    item: {
      //当前渲染的配置项
      type: Object,
      default: () => ({}),
    },
    index: {
      //当前在数组中的下标
      type: Number,
    },
    form: {
      // 当前form项
      type: Array,
      default: () => ({}),
    },
  },
  setup(props, { attrs }) {
    const form = props.form;
    const index = props.index;
    const item = props.item;
    // 如果为函数调用，并回显，如果为数组，直接使用
    if (typeof item.list === 'function') {
      item.list({ item, index, form });
    }
    const upDateCurrent = () => {
      const currentLabel =
        typeof item.currentLabel === 'function'
          ? item.currentLabel({ form, index, item })
          : item.currentLabel;

      //只有配置了currentLabel的才会去请求接口
      if (!currentLabel) return;
      // 获取allSelectorTypes中固定的配置，多选 可模糊等
      const { selectOption } = mappingUrl[currentLabel] || {};
      if (!selectOption) {
        console.error(currentLabel, '不存在配置中');
        // 如果为请求接口，则需要清除原先下拉内容，否则为固定内容
        if (currentLabel) {
          item.list = [];
        }
        return;
      }
      // 把 allSelectorTypes下selectOption中定义的属性混入a-select中
      for (const key in selectOption) {
        if (selectOption.hasOwnProperty.call(selectOption, key)) {
          item[key] = selectOption[key];
        }
      }
      //getParamsFromOut 为 从外部获取的参数；
      const ajaxOption = handleAjaxOption(mappingUrl[currentLabel], () =>
        item.getParamsFromOut({ form, index, item })
      );
      // 如果当前项,为联动,则判断联动所需前置条件是否存在 不存在则不请求
      if (item.isLinkage && item.linkageKeys) {
        let keys = form.formItem.filter((e) => {
          return item.linkageKeys.includes(e.currentKey);
        });
        if (keys.length === 0) return {};
        let t = keys.map((el) => {
          return { [el.currentKey]: el[el.currentKey] };
        });
        let b = keys.reduce((a, b) => {
          a[b.currentKey] = b[b.currentKey];
          return a;
        }, {});
        for (let key in b) {
          if (b[key] === undefined) return;
        }
      }
      allSelector
        .selector(ajaxOption)
        .then((res) => {
          // 第四列下拉列表 异步组件
          item.list = handleList(res, mappingUrl[currentLabel].listOption);
        })
        .catch((error) => {
          // 避免删除时连续请求错误接口
          console.error(currentLabel, '请求出错！', error);
          item.list = [];
        });
    };

    watch(() => item.currentLabel, upDateCurrent, {
      immediate: true,
    });

    // if (item.isLinkage && item.linkageKeys) {
    //   let keys = form.formItem.filter((e) => {
    //     return item.linkageKeys.includes(e.currentKey);
    //   });
    //   let oldlinkageKeys = cloneDeep(
    //     keys.map((el) => {
    //       return { [el.currentKey]: el[el.currentKey] };
    //     })
    //   );
    //   watch(form.formItem, () => {
    //     let keys = form.formItem.filter((e) => {
    //       return item.linkageKeys.includes(e.currentKey);
    //     });
    //     let newLinkageKeys = keys.map((el) => {
    //       return { [el.currentKey]: el[el.currentKey] };
    //     });
    //     let flag = false;
    //     for (let key in oldlinkageKeys) {
    //       // if (newLinkageKeys[key] === undefined) return (flag = false);
    //       if (oldlinkageKeys[key] === newLinkageKeys[key]) {
    //         continue;
    //       } else {
    //         flag = true;
    //       }
    //     }
    //     if (!flag) return;
    //     oldlinkageKeys = cloneDeep(
    //       keys.map((el) => {
    //         return { [el.currentKey]: el[el.currentKey] };
    //       })
    //     );
    //     console.log('更新', oldlinkageKeys);
    //   });
    //   // watch(keys,()=>{
    //   //   console.log('a')
    //   // })
    // }
    //#region  全选相关
    const indeterminate = ref(false);
    const checkedAll = ref(false);
    const handleChangeAll = (value) => {
      indeterminate.value = false;
      if (value) {
        checkedAll.value = true;
        form.data
          ? (form.data[item.currentKey] = item.list.map(
              (curItem) => curItem.value
            ))
          : (form.formItem[index].currentValue = item.list.map(
              (curItem) => curItem.value
            ));
      } else {
        checkedAll.value = false;
        form.data
          ? (form.data[item.currentKey] = [])
          : (form.formItem[index].currentValue = []);
      }
    };
    const handlePopupVisibleChange = (visible) => {
      if (item.isLinkage && item.linkageKeys && visible) {
        let keys = form.formItem.filter((e) => {
          return item.linkageKeys.includes(e.currentKey);
        });
        for (let el of keys) {
          if (el[el.currentKey] === undefined) {
            return;
          }
        }
        upDateCurrent()
      }
    };
    const handleChange = ({ item: selectItem, index, form, e }) => {
      if (e.length === item.list.length) {
        checkedAll.value = true;
        indeterminate.value = false;
      } else if (e.length === 0) {
        checkedAll.value = false;
        indeterminate.value = false;
      } else {
        checkedAll.value = false;
        indeterminate.value = true;
      }
      isFunction(selectItem.change)
        ? item.change({ item, index, form, e })
        : '';
    };
    //#endregion
    // 判断当前标签取值
    const showFillPlaceholder = (type, params) => {
      return isFunction(type) ? type(params) : type || '';
    };
    // 设置当前是否可用
    const setFormItemDisabled = (type, params) => {
      return typeof type === 'function' ? type(params) : !!type;
    };
    return () =>
      form.data ? (
        <a-select
          v-model={form.data[item.currentKey]}
          {...item}
          disabled={setFormItemDisabled(item.disabled, {
            item,
            index,
            form: attrs.form,
          })}
          multiple={
            isFunction(item.multiple)
              ? item.multiple({ item, index, form })
              : item.multiple
          }
          max-tag-count={item.maxTagCount || 2}
          onChange={(e) => handleChange({ item, index, form, e })}
          onPopupVisibleChange={handlePopupVisibleChange}
          placeholder={
            item.placeholder ||
            '请选择' +
              showFillPlaceholder(item.showLabel, { item, index, form })
          }
        >
          {item.multiple ? (
            <a-checkbox
              style={{ marginLeft: '12px', padding: '5px' }}
              v-model={checkedAll.value}
              indeterminate={indeterminate.value}
              onChange={handleChangeAll}
            >
              全选
            </a-checkbox>
          ) : null}
          {isArray(item.list)
            ? item.list.map((itemList) => {
                return (
                  <a-option
                    value={itemList.value}
                    label={itemList.label}
                  ></a-option>
                );
              })
            : null}
        </a-select>
      ) : (
        <a-select
          v-model={form.formItem[index][item.currentKey]}
          {...item}
          disabled={setFormItemDisabled(item.disabled, {
            item,
            index,
            form: attrs.form,
          })}
          multiple={
            isFunction(item.multiple)
              ? item.multiple({ item, index, form })
              : item.multiple
          }
          onChange={(e) => handleChange({ item, index, form, e })}
          max-tag-count={item.maxTagCount || 2}
          onPopupVisibleChange={handlePopupVisibleChange}
          placeholder={
            item.placeholder ||
            '请选择' +
              showFillPlaceholder(item.showLabel, { item, index, form })
          }
        >
          {item.multiple ? (
            <a-checkbox
              style={{ marginLeft: '12px', padding: '5px' }}
              v-model={checkedAll.value}
              indeterminate={indeterminate.value}
              onChange={handleChangeAll}
            >
              全选
            </a-checkbox>
          ) : null}
          {isArray(item.list)
            ? item.list.map((itemList) => {


                return (
                  <a-option
                    value={itemList.value}
                    label={itemList.label}
                  ></a-option>
                );
              })
            : ()=>{
              return null
            }}
        </a-select>
      );
  },
};
