import './index.less';
import { h, renderSlot, isVNode, watch, onMounted, ref } from 'vue';
import { isFunction, isArray } from 'lodash';
import Select from './select/index.jsx';
import { nanoid } from 'nanoid';
export default {
  name: 'DwForm',
  inheritAttrs: false,
  props: {
    layout: {
      //label 排列方式
      type: String,
    },
    disabled: {
      type: Object,
      default: () => ({}),
    },
    gutter: {
      type: Number,
      default: 20,
    },
    width: {
      type: Number,
      default: 600,
    },
  },
  components: {
    Select,
  },
  setup(props, { slots, attrs }) {
    //!!! 如果存在form.data则为静态表单，需要把data中数据与v-model绑定起来便于后续操作，
    watch(
      () => attrs.form.formItem.length,
      () => {
        if (attrs.form.data) {
          //form.data中所有的key
          const formDataKeys = Object.keys(attrs.form.data);
          const collectUndefinedKeys = {};
          // 循环收集未定义的 data
          attrs.form.formItem.forEach((keyItem) => {
            if (
              !formDataKeys.includes(keyItem.currentKey) &&
              keyItem.currentKey
            ) {
              collectUndefinedKeys[keyItem.currentKey] = '';
            }
          });
          if (JSON.stringify(collectUndefinedKeys) !== '{}') {
            console.error(
              collectUndefinedKeys,
              '请补全form.data中的字段，会引起响应式问题'
            );
          }
        }
      },
      {
        immediate: true,
      }
    );
    // 判断当前标签取值  //  如果参数是字符串直接返回，如果是函数，则调用
    const showFillPlaceholder = (type, params) => {
      return isFunction(type) ? type(params) : type || '';
    };
    const setInnerFormItemDisabled = (type, params) => {
      return isFunction(type) ? type(params) : type;
    };
    // 处理formItem渲染组件 展示undefined问题
    const handleFormItem = (item, index, parent) => {
      // 传入类型
      let type = item.itemType;
      if (isFunction(type)) {
        type = type(item, index);
      }
      // 如果为虚拟dom  直接渲染
      if (isVNode(type)) {
        return type;
      }
      // !!!如果为静态form 需要更换双向绑定方式 attrs.form.data---> attrs.form.data[item.currentKey]
      const vModelKeyValue = attrs.form.formItem[index][item.currentKey];
      const isStaticForm = !!attrs.form.data;

      // 失去焦点事件
      const bindBlur = (args) =>
        isFunction(item.blur)
          ? item.blur(args)
          : '';
      switch (type) {
        case 'input':
          return isStaticForm ? (
            <a-input
              v-model={attrs.form.data[item.currentKey]}
              {...item}
              disabled={setInnerFormItemDisabled(item.innerFormItemDisabled, {
                item,
                index,
                form: attrs.form,
              })}
              placeholder={
                item.placeholder ||
                '请输入' +
                  showFillPlaceholder(item.showLabel, {
                    item,
                    index,
                    form: attrs.form,
                  })
              }
              onBlur={(e) =>{
                bindBlur.call(null, { item, index, form: attrs.form,e })
              }
                
              }
            />
          ) : (
            <a-input
              v-model={attrs.form.formItem[index][item.currentKey]}
              {...item}
              disabled={setInnerFormItemDisabled(item.innerFormItemDisabled, {
                item,
                index,
                form: attrs.form,
              })}
              placeholder={
                item.placeholder ||
                '请输入' +
                  showFillPlaceholder(item.showLabel, {
                    item,
                    index,
                    form: attrs.form,
                  })
              }
            />
          );
        case 'input-number':
          return isStaticForm ? (
            <a-input
              v-model={attrs.form.data[item.currentKey]}
              {...item}
              disabled={setInnerFormItemDisabled(item.innerFormItemDisabled, {
                item,
                index,
                form: attrs.form,
              })}
              placeholder={
                item.placeholder ||
                '请输入' +
                  showFillPlaceholder(item.showLabel, {
                    item,
                    index,
                    form: attrs.form,
                  })
              }
            />
          ) : (
            <a-input
              v-model={attrs.form.formItem[index][item.currentKey]}
              {...item}
              disabled={setInnerFormItemDisabled(item.innerFormItemDisabled, {
                item,
                index,
                form: attrs.form,
              })}
              placeholder={
                item.placeholder ||
                '请输入' +
                  showFillPlaceholder(item.showLabel, {
                    item,
                    index,
                    form: attrs.form,
                  })
              }
            />
          );
        case 'textarea':
          return isStaticForm ? (
            <a-textarea
              v-model={attrs.form.data[item.currentKey]}
              {...item}
              disabled={setInnerFormItemDisabled(item.innerFormItemDisabled, {
                item,
                index,
                form: attrs.form,
              })}
              allow-clear
              placeholder={
                item.placeholder ||
                '请输入' +
                  showFillPlaceholder(item.showLabel, {
                    item,
                    index,
                    form: attrs.form,
                  })
              }
            />
          ) : (
            <a-textarea
              v-model={attrs.form.formItem[index][item.currentKey]}
              {...item}
              disabled={setInnerFormItemDisabled(item.innerFormItemDisabled, {
                item,
                index,
                form: attrs.form,
              })}
              allow-clear
              placeholder={
                item.placeholder ||
                '请输入' +
                  showFillPlaceholder(item.showLabel, {
                    item,
                    index,
                    form: attrs.form,
                  })
              }
            />
          );
        case 'radio':
          return isStaticForm ? (
            <a-radio-group
              v-model={attrs.form.data[item.currentKey]}
              {...item}
              disabled={setInnerFormItemDisabled(item.innerFormItemDisabled, {
                item,
                index,
                form: attrs.form,
              })}
              onChange={(e) =>
                isFunction(item.change)
                  ? item.change({ item, index, form: attrs.form, e })
                  : ''
              }
            >
              {item.list.map((itemList) => (
                <a-radio value={itemList.value}>{itemList.label}</a-radio>
              ))}
            </a-radio-group>
          ) : (
            <a-radio-group
              v-model={attrs.form.formItem[index][item.currentKey]}
              {...item}
              disabled={setInnerFormItemDisabled(item.innerFormItemDisabled, {
                item,
                index,
                form: attrs.form,
              })}
              onChange={(e) =>
                isFunction(item.change)
                  ? item({ item, index, form: attrs.form, e })
                  : ''
              }
            >
              {item.list.map((itemList) => (
                <a-radio value={itemList.value}>{itemList.label}</a-radio>
              ))}
            </a-radio-group>
          );
        case 'select':
          // ! 当前配置项 item 当前渲染下标 index， attrs当前form整体配置 getParamsFromOut从外部获取的参数
          return (
            <Select item={item} index={index} form={attrs.form}></Select>
          );
        case 'door-point':
          // ! 当前配置项 item 当前渲染下标 index， attrs当前form整体配置 getParamsFromOut从外部获取的参数 //doorPoint为文本，回显所需
          return isStaticForm ? (
            <DoorPoint
              v-model:doorPointId={attrs.form.data[item.currentKey]}
              v-model:doorPoint={attrs.form.data[item.doorPoint]}
            ></DoorPoint>
          ) : (
            <DoorPoint
              v-model:doorPointId={attrs.form.formItem[index][item.currentKey]}
              v-model:doorPoint={attrs.form.formItem[index][item.doorPoint]}
            ></DoorPoint>
          );
        case 'cascader':
          return (
            // field-names={fieldNames}
            <a-cascader
              options={options}
              v-model={vModelKeyValue}
              {...item}
              disabled={setInnerFormItemDisabled(item.innerFormItemDisabled, {
                item,
                index,
                form: attrs.form,
              })}
              placeholder="Please select ..."
            />
          );

        case 'time-picker':
          return isStaticForm ? (
            <a-time-picker
              class="full_width"
              v-model={attrs.form.data[item.currentKey]}
              {...item}
              disabled={setInnerFormItemDisabled(item.innerFormItemDisabled, {
                item,
                index,
                form: attrs.form,
              })}
            />
          ) : (
            <a-time-picker
              class="full_width"
              v-model={attrs.form.formItem[index][item.currentKey]}
              {...item}
              disabled={setInnerFormItemDisabled(item.innerFormItemDisabled, {
                item,
                index,
                form: attrs.form,
              })}
            />
          );
        case 'data-picker':
          return isStaticForm ? (
            <a-date-picker
              class="full_width"
              v-model={attrs.form.data[item.currentKey]}
              {...item}
              disabled={setInnerFormItemDisabled(item.innerFormItemDisabled, {
                item,
                index,
                form: attrs.form,
              })}
              placeholder={
                item.placeholder ||
                '请选择' +
                  showFillPlaceholder(item.showLabel, {
                    item,
                    index,
                    form: attrs.form,
                  })
              }
            />
          ) : (
            <a-date-picker
              class="full_width"
              v-model={attrs.form.formItem[index][item.currentKey]}
              {...item}
              disabled={setInnerFormItemDisabled(item.innerFormItemDisabled, {
                item,
                index,
                form: attrs.form,
              })}
              placeholder={
                item.placeholder ||
                '请选择' +
                  showFillPlaceholder(item.showLabel, {
                    item,
                    index,
                    form: attrs.form,
                  })
              }
            />
          );
        case 'range-picker':
          return isStaticForm ? (
            <a-range-picker
              class="full_width"
              v-model={attrs.form.data[item.currentKey]}
              {...item}
              disabled={setInnerFormItemDisabled(item.innerFormItemDisabled, {
                item,
                index,
                form: attrs.form,
              })}
              // placeholder={
              //   item.placeholder ||
              //   '请选择' +
              //     showFillPlaceholder(item.showLabel, {
              //       item,
              //       index,
              //       form: attrs.form,
              //     })
              // }
            />
          ) : (
            <a-range-picker
              class="full_width"
              v-model={attrs.form.formItem[index][item.currentKey]}
              {...item}
              disabled={setInnerFormItemDisabled(item.innerFormItemDisabled, {
                item,
                index,
                form: attrs.form,
              })}
              // placeholder={
              //   item.placeholder ||
              //   '请选择' +
              //     showFillPlaceholder(item.showLabel, {
              //       item,
              //       index,
              //       form: attrs.form,
              //     })
              // }
            />
          );
        case 'slot':
          return h('span', {}, [renderSlot(slots, item.value + index, item)]);
        default:
          if (isFunction(item.itemType)) {
            //
            return type;
          }
          return <div></div>;
      }
    };

    const formRef = ref(null);
    onMounted(() => {
      attrs.form.formRef = formRef;
    });
    // 处理表单校验 isFunction(item.rules)?item.rules()
    const handleRules = ({ item, form, index }) => {
      //todo 待补充完整自定义验证规则
      const rulesArr = [];
      if (item.required) {
        //必填选项
        rulesArr.push({
          required: true,
          message:
            showFillPlaceholder(item.showLabel, {
              item,
              index,
              form: attrs.form,
            }) + '不能为空！',
        });
      }
      if (!isArray(item.rules)) {
        item.rules = [];
      }
      // item.rules.push.apply(item.rules,rulesArr)
      return item.rules.concat(rulesArr);
    };
    // 校验失败触发的函数
    const submitFailed = () => {
      console.log(6666, '失败了');
    };
    // 设置当前是否可用
    const setFormItemDisabled = (type, params) => {
      if (props.disabled.isEdit) return true;
      return typeof type === 'function' ? type(params) : !!type;
    };

    //  处理展示和隐藏逻辑
    // const formItem = filter(item=>isFunction(item.show)?item.show({ form: attrs.form}):true)
    return () => (
      <a-form
        disabled={props.disabled.isEdit}
        model={attrs.form}
        layout={props.layout}
        width={props.width}
        ref={formRef}
        submit-failed={submitFailed}
        class="dw_form"
      >
        <a-row gutter={props.gutter} wrap>
          {attrs.form.formItem.map((item, index) => {
            // const formItemOption = item.formItemOption || {};
            // item.itemType !== 'slot' ?
            return attrs.form.data
              ? // 存在配置项show函数，并为真或者不存在配置项时才展示formItem
                // todo show 应该使用单独函数，把当前不存在的剔除掉
                ((isFunction(item.show) &&
                  item.show({ item, form: attrs.form })) ||
                  !item.show) && (
                  <a-col
                    span={item.span}
                    // key={nanoid()}
                    class={item.itemType === 'line-empty' ? 'line_empty' : ''}
                  >
                    {/* label={showFillPlaceholder(item.showLabel,{item, index, form:attrs.form})} */}
                    <a-form-item
                      // key={nanoid()}
                      class="dw_form_item"
                      field={'data.' + [item.currentKey]}
                      hide-label={item.hideLabel}
                      label-col-flex={item.labelColFlex || '100px'}
                      {...item}
                      disabled={setFormItemDisabled(item.disabled, {
                        item,
                        index,
                        form: attrs.form,
                      })}
                      v-slots={{
                        label: (curProps, node) => {
                          return (
                            <div class="label_wrapper">
                              {showFillPlaceholder(item.showLabel, {
                                item,
                                index,
                                form: attrs.form,
                              })}
                              <div>
                                {item.appendLabel && item.appendLabel()}
                              </div>
                            </div>
                          );
                        },
                      }}
                      rules={handleRules({ item, form: attrs.form, index })}
                    >
                      {handleFormItem(item, index, attrs.form.formItem)}
                    </a-form-item>
                  </a-col>
                )
              : // 存在配置项show函数，并为真或者不存在配置项时才展示formItem
                ((isFunction(item.show) &&
                  item.show({ item, form: attrs.form })) ||
                  !item.show) && (
                  <a-col span={item.span} key={nanoid()}>
                    {/* //todo 校验规则待完善
                     */}
                    <a-form-item
                      class="dw_form_item"
                      field={
                        'attrs.form.formItem' +
                        [index] +
                        '.' +
                        [item.currentKey]
                      }
                      hide-label={item.hideLabel}
                      label-col-flex={item.labelColFlex || '100px'}
                      {...item}
                      disabled={setFormItemDisabled(item.disabled, {
                        item,
                        index,
                        form: attrs.form,
                      })}
                      v-slots={{
                        label: (props) => {
                          return (
                            <span>
                              {showFillPlaceholder(item.showLabel, {
                                item,
                                index,
                                form: attrs.form,
                              })}
                            </span>
                          );
                        },
                      }}
                      rules={handleRules({ item, form: attrs.form })}
                    >
                      {handleFormItem(item, index, attrs.form.formItem)}
                    </a-form-item>
                  </a-col>
                );
          })}
        </a-row>
      </a-form>
    );
  },
};
