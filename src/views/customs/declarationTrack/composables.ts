import {
  ref,
} from 'vue'

export enum UploadTypeEnum {
  TAXBILL,
  CUSTOMSDECLARATION,
  QUARANTINECERTIFICATE,
}
export type UploadType = {
  value: UploadTypeEnum | undefined,
}

export const useUploadType = () => {
  const uploadType: UploadType = ref()
  const toggleUploadType = (type: UploadTypeEnum) => uploadType.value = type
  return {
    uploadType,
    toggleUploadType,
  }
}

export const useVisible = () => {
  const visible = ref(false)
  const setVisible = (v: boolean) => visible.value = v
  const toggleVisible = () => visible.value = !visible.value

  return {
    visible,
    setVisible,
    toggleVisible,
  }
}

export enum ExceptionTypeNum {
  DECLARATION,
  ONSITEDECLARATION,
  INSPECTION,
}

let exceptionType = ref<ExceptionTypeNum>()
export const useExceptionTypeSingleton = () => {
  const toggleExceptionType = (et: ExceptionTypeNum) => exceptionType.value = et
  return {
    exceptionType,
    toggleExceptionType,
  }
}

export enum CommonOperationMode {
  CREATE,
  UPDATE,
}
