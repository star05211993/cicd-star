import {
  ref,
} from 'vue'

export const useVisible = () => {
  const visible = ref(false)
  const toggleVisible = () => visible.value = !visible.value

  return {
    visible,
    toggleVisible,
  }
}
