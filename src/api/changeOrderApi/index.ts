import axios from '@/api/interceptor';
import { scmBase, basicdataBase } from '@/api/api.config';
export const changeOrderApi = {
  //运单详情
  mosRunOrderDetail: (params?: any) => {
    return axios({
      url: `${scmBase}/logistics-exchangedo-detail/get-exchangedo-detail`,
      method: 'GET',
      params,
    });
  },
  //换单交接记录
  mosChangeOrderDetailGetDocument: (params?: any) => {
    return axios({
      url: `${scmBase}/logistics-exchangedo-detail/record-list`,
      method: 'GET',
      params,
    });
  },
  //单证交接签收
  mosChangeOrderDetailSign: (data?: any) => {
    return axios({
      url: `${scmBase}/logistics-exchangedo-detail/sign`,
      method: 'POST',
      data,
    });
  },
  //单证交接退回
  mosChangeOrderDetailBack: (data?: any) => {
    return axios({
      url: `${scmBase}/logistics-exchangedo-detail/back`,
      method: 'POST',
      data,
    });
  },
  //换单准备页面信息
  mosChangeOrderDetailGetPreparation: (params?: any) => {
    return axios({
      url: `${scmBase}/logistics-exchangedo-detail/get-preparation`,
      method: 'GET',
      params,
    });
  },
  //海运费登记
  detailSeaFreightRegister: (data?: any) => {
    return axios({
      url: `${scmBase}/logistics-exchangedo-detail/sea-freight-register`,
      method: 'POST',
      data,
    });
  },
  // 海运费异常新增
  detailSaveException: (data?: any) => {
    return axios({
      url: `${scmBase}/logistics-exchangedo-detail/save-exception`,
      method: 'POST',
      data,
    });
  },
  //换单准备完成
  detailDispatchExpense: (params?: any) => {
    return axios({
      url: `${scmBase}/logistics-exchangedo-detail/preparation-complete`,
      method: 'GET',
      params,
    });
  },
  //换单排出
  detailDispatchComplete: (params?: any) => {
    return axios({
      url: `${scmBase}/logistics-exchangedo-detail/dispatch-expense`,
      method: 'GET',
      params,
    });
  },
  // 换单执行反馈
  detailFeedback: (data?: any) => {
    return axios({
      url: `${scmBase}/logistics-exchangedo-detail/execute-feedback`,
      method: 'POST',
      data,
    });
  },
  //换单执行页面信息
  getRecordDetail: (params?: any) => {
    return axios({
      url: `${scmBase}/logistics-exchangedo-detail/execute-record`,
      method: 'GET',
      params,
    });
  },  
  //批量单证反馈列表
  getFeedbackList: (data?: any) => {
    return axios({
      url: `${scmBase}/logistics-exchangedo/document-feedback-list`,
      method: 'POST',
      data,
    });
  },  
  //批量换单反馈操作
  postFeedbackEdit: (data?: any) => {
    return axios({
      url: `${scmBase}/logistics-exchangedo/document-feedback-list/batch`,
      method: 'POST',
      data,
    });
  },

  //码头
  getPortList: (params?: any) => {
    return axios({
      url: `${basicdataBase}/basicdata-wharf-port-area/get-by-port-code`,
      method: 'GET',
      params,
    });
  },
  //换单派出
  getExpenseList: (params?: any) => {
    return axios({
      url: `${scmBase}/logistics-exchangedo/dispatch-expense-list`,
      method: 'GET',
      params,
    });
  },
  //换单完成
  getCompleteList: (params?: any) => {
    return axios({
      url: `${scmBase}/logistics-exchangedo/preparation-complete-list`,
      method: 'GET',
      params,
    });
  },

 //查询批量单证交接
  postHandoverList: (data?: any) => {
    return axios({
      url: `${scmBase}/logistics-exchangedo/exchangedo-handover-list`,
      method: 'POST',
      data,
    });
  },
  //批量单证交接
  postFeedbackEditDocument: (data?: any) => {
    return axios({
      url: `${scmBase}/logistics-exchangedo/document-handover-list`,
      method: 'POST',
      data,
    });
  },
  //运单列表，详情编辑
  updateLogisticsName : (data?: any) => {
    return axios({
      url: `${scmBase}/logistics-mos-view/update-logistics-name`,
      method: 'POST',
      data,
    });
  },
  //换单信息登记
  registerLogisticsDetail: (data?: any) => {
    return axios({
      url: `${scmBase}/logistics-exchangedo-detail/register`,
      method: 'POST',
      data,
    });
  },
  //换单导出
  exportList: (data?: any) => {
    return axios({
      url: `${scmBase}/logistics-mos-view/exchangedo-tracking-list/export`,
      method: 'POST',
      data,
      responseType: "blob",
    });
  },
  // /scm/logistics-mos-view/exchangedo-tracking-list/export
  // /scm/logistics-exchangedo-detail/register
  // /scm/logistics-mos-view/update-logistics-name
}