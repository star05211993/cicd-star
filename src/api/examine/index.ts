import axios from '@/api/interceptor';
import { scmBase,crmBase,memberBase} from '@/api/api.config';
export const examineApi ={
  //显示视图
  mosExamineViewPage: (data?: any) => {
    return axios({
      url: `${scmBase}/logistics-audit-view/page`,
      method: 'POST',
      data,
    });
  },
  //审单详情
  examineOrder: (params?: any) => {
    return axios({
      url: `${scmBase}/order-logistics/detail/simple-detail`,
      method: 'GET',
      params,
    });
  },
  //查询运单的单证与可添加的单证
  getAuditDocumentList: (params?: any) => {
    return axios({
      url: `${scmBase}/logistics-audit/audit-document-list`,
      method: 'GET',
      params,
    });
  },
  //审单详情-添加单证按钮弹窗
  windowAddDocument: (params?: any) => {
    return axios({
      url: `${scmBase}/logistics-audit/window-add-document`,
      method: 'GET',
      params,
    });
  },
  //批量审单单证新增或修改
  batchSaveUpdateAuditDocument: (data?: any) => {
    return axios({
      url: `${scmBase}/logistics-audit/batch-save-or-update-audit-document`,
      method: 'POST',
      data,
    });
  },
  //审单单证新增或修改
  SaveUpdateAuditDocument: (data?: any) => {
    return axios({
      url: `${scmBase}/logistics-audit/save-or-update-audit-document`,
      method: 'POST',
      data,
    });
  },
  //批量下载单证
  batchDownloadAuditFile: (params?: any) => {
    return axios({
      url: `${scmBase}/logistics-audit/batch-download-audit-file`,
      method: 'GET',
      params,
      responseType: "blob",
    });
  },
  //批量删除审单单证信息
  batchDeleteAuditDocument: (params?: any) => {
    return axios({
      url: `${scmBase}/logistics-audit/batch-delete-audit-document`,
      method: 'GET',
      params,
    });
  },
  //删除审单单证信息中的文件
  deleteDocumentFile: (params?: any) => {
    return axios({
      url: `${scmBase}/logistics-audit/delete-document-file`,
      method: 'GET',
      params,
    });
  },
  //新增审单文件
  addDocumentFile: (data?: any) => {
    return axios({
      url: `${scmBase}/logistics-audit/add-document-file`,
      method: 'POST',
      data,
    });
  },
  //审单d单证历史信息查询
  getExamineHistory: (data?: any) => {
    return axios({
      url: `${scmBase}/logistics-audit/queryLogisticsAuditHistory`,
      method: 'POST',
      data,
    });
  },
  //删除单证文件
  deleteDocument: (params?: any) => {
    return axios({
      url: `${scmBase}/logistics-audit/deleteAuditDocument`,
      method: 'GET',
      params,
    });
  },
  //审单备注信息
  getMarkList: (params?: any) => {
    return axios({
      url: `${scmBase}/logistics-audit/queryAuditRemark`,
      method: 'GET',
      params,
    });
  },
  //保存审单备注
  saveMark: (data?: any) => {
    return axios({
      url: `${scmBase}/logistics-audit/saveAuditRemark`,
      method: 'POST',
      data,
    });
  },
  //保存，审单完成,操作流转
  examineSaveSubmit: (data?: any) => {
    return axios({
      url: `${scmBase}/logistics-audit/saveAuditsubmit`,
      method: 'POST',
      data,
    });
  },

  // 批量处理审单的保存提交【保存，审单完成，操作流转】
  saveAuditSubmitList: (data?: any) => {
    return axios({
      url: `${scmBase}/logistics-audit/saveAuditSubmitList`,
      method: 'POST',
      data,
    });
  },
  //不准入
  toAccess: (params?: any) => {
    return axios({
      url: `${scmBase}/logistics-audit/no-access`,
      method: 'GET',
      params,
    });
  },
  // 审单详情编辑
  examineEdit: (data?: any) => {
    return axios({
      url: `${scmBase}/logistics-audit/editOrderLogisticsByMail`,
      method: 'POST',
      data,
    });
  },
  //审单列表导出
  mosLogisticsViewExcel: (data?: any) => {
    return axios({
      url: `${scmBase}/logistics-audit-view/list/excel`,
      method: 'POST',
      data,
      responseType: "blob",
    });
  },



  //邮件页签列表
  examineEmailList: (params?: any) => {
    return axios({
      url: `${scmBase}/mailinfo/queryMailByOrderLogisticsNo`,
      method: 'GET',
      params,
    });
  },
  
  
}