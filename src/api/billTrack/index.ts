import axios from '@/api/interceptor';
import {
  memberBase,
  scmBase,
  basicdataBase,
  settleBase,
} from '@/api/api.config';
export const billTrackApi = {
  //客户账单跟踪列表
  receivablePage: (data?: any) => {
    return axios({
      url: `${settleBase}/api/receivable/page?type=1`,
      method: 'POST',
      data,
    })
  },
  //供应商账单跟踪列表
  receivableSupplierPage: (data?: any) => {
    return axios({
      url: `${settleBase}/api/receivable/page?type=2`,
      method: 'POST',
      data,
    })
  },
  // 自定义账单模板分页
  billTrackTemplatePage: (data?: any) => {
    return axios({
      url: `${settleBase}/api/receivable_template/page`,
      method: 'POST',
      data,
    })
  },

  // 根据客户id获取模板集合
  listByClientCompanyId: (data?: any) => {
    return axios({
      url: `${settleBase}/api/receivable_template/enables`,
      method: 'POST',
      data,
    });
  },

  // 账单详情导出
  receivableExportSupplier: (params?: any) => {
    return axios({
      url: `${settleBase}/api/receivable/export`,
      method: 'GET',
      params,
    });
  },
  //账单详情页-详情接口
  receivableDetail: (params?: any) => {
    return axios({
      url: `${settleBase}/api/receivable/detail`,
      method: 'GET',
      params,
    });
  },
  //账单详情页-复核
  receivableReview: (data?: any) => {
    return axios({
      url: `${settleBase}/api/receivable/review`,
      method: 'POST',
      data,
    });
  },
  //账单详情页-减免费用
  receivableDiscount: (data?: any) => {
    return axios({
      url: `${settleBase}/api/receivable/discount`,
      method: 'POST',
      data,
    });
  },
  //账单详情页-复核完成
  reviewFinish: (data?: any) => {
    return axios({
      url: `${settleBase}/api/receivable/review-finish`,
      method: 'POST',
      data,
    });
  },
  // 账单详情页-取消确认
  receivableRollbackConfirm: (data?: any) => {
    return axios({
      url: `${settleBase}/api/receivable/cancelconfirmation`,
      method: 'POST',
      data,
    });
  },
  //账单详情页-批量确认费用
  receivableBatchConfirm: (data?: any) => {
    return axios({
      url: `${settleBase}/api/receivable/batch-confirm`,
      method: 'POST',
      data,
    });
  },
  // 供应商账单详情页-作废
  receivableDiscard: (data?: any) => {
    return axios({
      url: `${settleBase}/api/receivable/discard?id=${data}`,
      method: 'DELETE',
      data,
    });
  },
  // 供应商账单详情页-删除结算单
  receivableStatementDelete: (data?: any) => {
    return axios({
      url: `${settleBase}/api/receivable/remove-statement`,
      method: 'POST',
      data,
    });
  },
  // 供应商账单详情页-搜索可加入账单结算单列表
  statementCostCanJoinReceivableStatements: (params?: any) => {
    return axios({
      url: `${settleBase}/api/statement/cost/can-join-receivable/statements`,
      method: 'GET',
      params,
    });
  },
  // 添加账单
  receivableStatementAddTo: (data?: any) => {
    return axios({
      url: `${settleBase}/api/receivable/statement`,
      method: 'POST',
      data,
    });
  },
  //数据字典-所属公司
  getCompanyOption :(params?: any) => {
    return axios({
      url: `${scmBase}/relation-view/queryRelationByDropDown`,
      method: 'GET',
      params,
    });
  },
  //新增账单页-查询可加入账单的收入列表
  canJoinReceivableDetails: (data?: any) => {
    return axios({
      url: `${settleBase}/api/statement/income/can-join-receivable/mergers`,
      method: 'POST',
      data,
    });
  },
}