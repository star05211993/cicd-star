import axios from '@/api/interceptor';
import { memberBase ,scmBase, wmsBase, businessBase, basicdataBase, crmBase,process} from '@/api/api.config';
export const common = {
  
   // 原产国列表
  getCountry: (params: any) => {
    return axios({
      url: `${memberBase}/country/list`,
      method: 'GET',
      params,
    });
  },
  //省份
  getProvince: (params: any) => {
    return axios({
      url: `${scmBase}/relation/list-of-province`,
      method: 'GET',
      params,
    });
  },
  //城市
  getCity: (params: any) => {
    return axios({
      url: `${scmBase}/relation/list-of-city`,
      method: 'GET',
      params,
    });
  },
  //县区
  getCounty: (params: any) => {
    return axios({
      url: `${scmBase}/relation/list-of-county`,
      method: 'GET',
      params,
    });
  },
  //街镇
  getTownship: (params: any) => {
    return axios({
      url: `${scmBase}/relation/list-of-township`,
      method: 'GET',
      params,
    });
  },
  //币别
  getCurrency: (params: any) => {
    return axios({
      url: `${scmBase}/relation/list-of-currency`,
      method: 'GET',
      params,
    });
  },
  //获取下载链接
  getFileDownload: (params: any) => {
    return axios({
      url: `${scmBase}/file-process/get-file-download`,
      method: 'GET',
      params,
      responseType: "blob",
    });
  },

  //港口
  getPortList: (params: any) => {
    return axios({
      url: `${memberBase}/basicdata-seaport/list`,
      method: 'GET',
      params,
    });
  },
  //国内港口
  portAreaList: (params:any) => {
    return axios({
      url: `${basicdataBase}/basicdata-seaport/list-port`, 
      method: 'GET', 
      params
    })
  },
  //单证类型
  getDocType: (params: any) => {
    return axios({
      url: `${basicdataBase}/basicdata-code/list?codeTypeCode=businessdocuments`,
      method: 'GET',
      params,
    });
  },
  //上传文件
  fileUpload: (data?: any) => {
    return axios({
      url: `${scmBase}/file/upload`,
      method: 'POST',
      data,
      paramsType: true,
      headerType: 'form',
    });
  },
  //取收件人
  getReceiver: (params: any) => {
    return axios({
      url: `${basicdataBase}/mailinfo/querySendMailAddressByOrderLogisticsNo`,
      method: 'GET',
      params,
    });
  },
  //菜单代办数量
  commonTodo: (params: any) => {
    return axios({
      url: `${process}/common/todo`,
      method: 'GET',
      params,
    });
  },
}
