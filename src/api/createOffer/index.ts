import axios from '@/api/interceptor';
import {
  crmBase,
  memberBase,
  basicdataBase,
  scmBase,
  productBase,
  settleBase,
} from '@/api/api.config';
export const createOffer = {
  // !获取品类

  getCategory: (params?: any) => {
    return axios({
      url: `${productBase}/saleProduct/select-all-category-name`,
      method: 'GET',
      params,
    });
  },

  //!根据品类获取品名
  getCategoryName: (params?: any) => {
    return axios({
      url: `${productBase}/saleProduct/select-by-category-name`,
      method: 'GET',
      params,
    });
  },

  // !根据品名查询报价方式
  crmGetTonnage: (data?: any) => {
    return axios({
      url: `${crmBase}/goods-tons/getTonnage`,
      method: 'POST',
      data,
    });
  },

  // !销售
  salesRole: (data?: any) => {
    return axios({
      url: `${memberBase}/business-role-config/select-by-role`,
      method: 'POST',
      data,
    });
  },

  //!根据业务类型获取服务项目列表
  serveList: (params?: any) => {
    return axios({
      url: `${basicdataBase}/basicdata-code/relation-list`,
      method: 'GET',
      params,
    });
  },
  //!根据业务类型获取服务项目列表
  getPackingTypeCode: (params?: any) => {
    return axios({
      url: `${memberBase}/basicdata-code/list`,
      method: 'GET',
      params,
    });
  },
  // !新增报价书基本信息
  addBasicInfo: (data?: any) => {
    return axios({
      url: `${crmBase}/contract-offer-confirmation/add-basicinfo`,
      method: 'POST',
      data,
    });
  },

  // !修改报价书基本信息
  updateBasicInfo: (data?: any) => {
    return axios({
      url: `${crmBase}/contract-offer-confirmation/update-basicinfo`,
      method: 'POST',
      data,
    });
  },
  //客户报价书-清关运输-报价明细列表-查询
  quoteDetailList: (params?: any) => {
    return axios({
      url: `${crmBase}/contract-offer-confirmation/quote-detail-list`,
      method: 'GET',
      params,
    });
  },
  //!清关运输-添加报价明细-保存价明细
  addQuoteDetail: (data?: any) => {
    return axios({
      url: `${crmBase}/contract-offer-confirmation/add-quote-detail`,
      method: 'POST',
      data,
    });
  },
  // !清关运输-修改报价明细-保存
  updateQuoteDetail: (data?: any) => {
    return axios({
      url: `${crmBase}/contract-offer-confirmation/update-quote-detail`,
      method: 'POST',
      data,
    });
  },
  // !查询合同报价确认书表 --报价明细
  quoteBookInfo: (params?: any) => {
    return axios({
      url: `${crmBase}/contract-offer-confirmation/info`,
      method: 'GET',
      params,
    });
  },

  //!客户报价书-清关运输-删除报价明细
  deleteQuoteDetail: (data?: any) => {
    return axios({
      url: `${crmBase}/contract-offer-confirmation/delete-quote-detail`,
      method: 'POST',
      data,
    });
  },

  //!清关运输-报价测算-查询成本费用 -默认
  costItemList: (params?: any) => {
    return axios({
      url: `${crmBase}/basicdata-cost-item/selectList`,
      method: 'GET',
      params,
    });
  },
  // !添加附件
  addAttachment: (data?: any) => {
    return axios({
      url: `${crmBase}/contract-offer-confirmation/addAttachment`,
      method: 'POST',
      data,
    });
  },
  // !删除
  deleteAttachment: (data?: any) => {
    return axios({
      url: `${crmBase}/contract-offer-confirmation/deleteAttachment`,
      method: 'POST',
      data,
    });
  },
  //#endregion
};
