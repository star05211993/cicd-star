import axios from '@/api/interceptor';
import {
  scmBase,
  customsBase,
} from '@/api/api.config';

const DeclarationAPI = {
  queryDeclarationTrackList: (data: any) => axios({
    url: `${scmBase}/api/customsclearance/page`,
    method: 'POST',
    data,
  }),
  handleExport: (data: any) => axios({
    url: `${scmBase}/api/customsclearance/page/export`,
    method: 'POST',
    data,
  }),
  dispatchOnSite: (data: any): Promise<any> => axios({
    url: `${scmBase}/api/customsclearance/site/dispatch`,
    method: 'POST',
    data,
  }),
  releaseDeclaration: (data: any): Promise<any> => axios({
    url: `${scmBase}/api/customsclearance/release`,
    method: 'POST',
    data,
  }),
  registerExceptionForDeclaration: (data: any): Promise<any> => axios({
    url: `${scmBase}/api/customsclearance/register/exception`,
    method: 'POST',
    data,
  }),
  uploadTaxBill: (data: any): Promise<any> => axios({
    url: `${scmBase}/api/customsclearance/upload/taxbill`,
    method: 'POST',
    data,
  }),
  uploadCustomsDeclaration: (data: any): Promise<any> => axios({
    url: `${scmBase}/api/customsclearance/upload/customsdeclaration`,
    method: 'POST',
    data,
  }),
  uploadQuarantineCertificate: (data: any): Promise<any> => axios({
    url: `${scmBase}/api/customsclearance/upload/quarantinecertificate`,
    method: 'POST',
    data,
  }),
  uploadFile: (data: any): Promise<any> => axios({
    url: `${scmBase}/file/upload`,
    method: 'POST',
    data,
    paramsType: true,
    headerType: 'form',
  }),
  cellEdit: (data: any): Promise<any> => axios({
    url: `${scmBase}/api/customsclearance/page/lineedit`,
    method: 'POST',
    data,
  }),
  qeuryCustomsReceipt: (params: any): Promise<any> => axios({
    url: `${customsBase}/api/EDICustoms/getEdiFeedbackList`,
    method: 'GET',
    params,
  }),
  queryCommonSelector: (params: any): Promise<any> => axios({
    url: `${scmBase}/logistics-mos-view/selector`,
    method: 'GET',
    params,
  }),
  queryInspectionDetail: (params: any): Promise<any> => axios({
    url: `${scmBase}/api/customsclearance/inspection/details`,
    method: 'GET',
    params,
  }),
  createInspectionDetail: (data: any): Promise<any> => axios({
    url: `${scmBase}/api/customsclearance/inspection`,
    method: 'POST',
    data,
  }),
  queryContainerNoByName: (params: any): Promise<any> => axios({
    url: `${scmBase}/api/customsclearance/inspection/unaddded/containernos`,
    method: 'get',
    params,
  }),
  deleteInspectionDetail: (data: any): Promise<any> => axios({
    url: `${scmBase}/api/customsclearance/inspection/delete`,
    method: 'post',
    data,
  }),
}

const OrderPreparationAPI = {
  exportWithPagination: (data?: any): Promise<any> => axios({
    url: `${scmBase}/api/makeorder/page/export`,
    method: 'POST',
    data,
  }),
  queryOrderPreparationList: (data?: any): Promise<any> => axios({
    url: `${scmBase}/api/makeorder/page`,
    method: 'POST',
    data,
  }),
  finishOrderPreparation: (data?: any): Promise<any> => axios({
    url: `${scmBase}/api/makeorder/complete`,
    method: 'POST',
    data,
  }),
  querySingleWindowSwitch: (params?: any): Promise<any> => axios({
    url: `${scmBase}/api/makeorder/singlewindow/switch`,
    method: 'get',
    params,
  }),
  handoverDocument: (data?: any): Promise<any> => axios({
    url: `${scmBase}/api/makeorder/document/handover`,
    method: 'post',
    data,
  }),
  columnUpdate: (data?: any): Promise<any> => axios({
    url: `${scmBase}/api/makeorder/page/lineedit`,
    method: 'post',
    data,
  }),
  batchCreateCustomsDeclaration: (data?: any): Promise<any> => axios({
    url: `${scmBase}/api/makeorder/batch/customsclearance`,
    method: 'post',
    data,
  }),
  registerExceptionForOrderPreparation: (data?: any): Promise<any> => axios({
    url: `${scmBase}/api/makeorder/register/exception`,
    method: 'post',
    data,
  }),
  queryMatchingInfo: (params?: any): Promise<any> => axios({
    url: `${customsBase}/api/Manifest/getComparisonDetail`,
    method: 'get',
    params,
  }),
  correctManifestData: (data?: any): Promise<any> => axios({
    url: `${customsBase}/api/Manifest/update-dec-head`,
    method: 'post',
    data,
  }),
  correctContainerData: (data?: any): Promise<any> => axios({
    url: `${customsBase}/api/Manifest/update-dec-container`,
    method: 'post',
    data,
  }),
  queryTrackingNoListByName: (params?: any): Promise<any> => axios({
    url: `${scmBase}/api/makeorder/can/append/customs/logisticsnos`,
    method: 'get',
    params,
  }),
  addDecHead: (data?: any): Promise<any> => axios({
    url: `${customsBase}/api/Declaration/add-dec-head`,
    method: 'post',
    data,
  }),
  queryJumpInfo: (params?: any): Promise<any> => axios({
    url: `${scmBase}/api/customsclearance/jump/details`,
    method: 'get',
    params,
  })
}

export default {
  ...DeclarationAPI,
  ...OrderPreparationAPI,
}
