import axios from '@/api/interceptor';
import { scmBase,crmBase,memberBase} from '@/api/api.config';
export const emailApi ={
  //用户邮箱
  getEmails: (params?: any) => {
    return axios({
      url: `${scmBase}/mailinfo/queryMailAccount`,
      method: 'GET',
      params,
    });
  },

  //各邮箱邮件情况
  getEmailCount: (params?: any) => {
    return axios({
      url: `${scmBase}/mailinfo/queryMailCount`,
      method: 'GET',
      params,
    });
  },

  //邮箱列表
  getEmailList: (data?: any) => {
    return axios({
      url: `${scmBase}/mailinfo/queryMailDataList`,
      method: 'POST',
      data,
    });
  },
  //邮件详情
  getEmailDetail: (params?: any) => {
    return axios({
      url: `${scmBase}/mailinfo/mailDetail`,
      method: 'GET',
      params,
    });
  },
  //国内买家
  getMosBuyerList2: (params?: any) => {
    return axios({
      url: `${scmBase}/relation/queryRelationByLabelCode`,
      method: 'GET',
      params,
    });
  },
  //业务角色--换单员制单员
  getRole: (data?: any) => {
    return axios({
      url: `${memberBase}/business-role-config/select-by-role`,
      method: 'POST',
      data,
    });
  },

  //审单邮件新建运单
  newEmailExamine: (data?: any) => {
    return axios({
      url: `${scmBase}/logistics-audit/addOrderLogisticsByMail`,
      method: 'POST',
      data,
    });
  },

  //关联运单列表
  getRelationOrderList: (data?: any) => {
    return axios({
      url: `${scmBase}/logistics-mos/list/page-document`,
      method: 'POST',
      data,
    });
  },

  //上传单证文件
  uploadDocument: (data?: any) => {
    return axios({
      url: `${scmBase}/logistics-audit/saveLogisticsAuditDocument`,
      method: 'POST',
      data,
    });
  },
  //取收件人
  getReceiver: (params: any) => {
    return axios({
      url: `${scmBase}/mailinfo/querySendMailAddressByOrderLogisticsNo`,
      method: 'GET',
      params,
    });
  },
  //获取邮件人
  getReceiverList: (params: any) => {
    return axios({
      url: `${scmBase}/mailinfo/querySendMailAddressByEmailAddress`,
      method: 'GET',
      params,
    });
  },
  //写邮件
  writeEmail: (data?: any) => {
    return axios({
      url: `${scmBase}/mailinfo/addMailInfo`,
      method: 'POST',
      data,
    });
  },
  //还原
  emailOpt: (data?: any) => {
    return axios({
      url: `${scmBase}/mailinfo/operationMailFalg`,
      method: 'POST',
      data,
    });
  },
  //取消邮件关联运单
  cancleRelationMail: (params: any) => {
    return axios({
      url: `${scmBase}/logistics-audit/cancleRelationMail`,
      method: 'GET',
      params,
    });
  },
  //根据邮件地址获取邮件
  downMailForAddress: (params: any) => {
    return axios({
      url: `${scmBase}/mailinfo/downMailForAddress`,
      method: 'GET',
      params,
    });
  },
}