/**
 * *@2022-06-28
 * *@author lss
 * *@describe CRM接口仓库
 */

import axios from '@/api/interceptor';
import {
  crmBase,
  productBase,
  settleBase,
  basicdataBase,
  scmBase,
  memberBase,
  processBase
} from '@/api/api.config';
import { AnyKindOfDictionary } from 'lodash';
export const crmApi = {
  /**
   * 成本维护
  **/

  // 成本列表表头
  costCenterTitle: (params: any) => {
    return axios({
      url: `${crmBase}/cost/getCostImpactVo`,
      method: 'GET',
      params
    });
  },

  // 成本列表
  costCenterList: (data: any) => {
    return axios({ 
      url: `${crmBase}/cost/pageList`,
      method: 'POST',
      data,
    });
  },

  // 成本添加
  costCenterAdd: (data: any) => {
    return axios({
      url: `${crmBase}/cost/add`, 
      method: 'POST',
      data,
    }); 
  },

  // 成本编辑
  costCenterEdit:(data:any)=>{
    return axios({
      url: `${crmBase}/cost/edit`,
      method: 'POST',
      data ,
    });
  },
  // 成本详情页
  costDetailList:(params:any)=>{
    return axios({
      url: `${crmBase}/cost/detail`,
      method: 'GET',
      params
    });
  },

  // 校验成本基本信息是否唯一
  checkCost: (data: any) => {
    return axios({
      url: `${crmBase}/cost/checkCost`, 
      method: 'POST',
      data,
    });
  },

  // 成本页面左侧树形结构
  costCenterFei(data: any) {
    return axios({
      url: `${settleBase}/api/basicdata/fee-group-stage`, 
      method: 'POST',
      data,
    })
  },

  // 成本删除
  costCenterDelete: (params: any) => {
    return axios({
      url: `${crmBase}/cost/delete`, 
      method: 'GET', 
      params
    });
  },
  // 港口
  port: (params:any) => {
    return axios({
      url: `${basicdataBase}/basicdata-seaport/list`, 
      method: 'GET', 
      params
    });
  },
  // 港区
  portArea: (params:any) => {
    return axios({
      url: `${basicdataBase}/basicdata-wharf-port-area/get-by-port-code-distinct`, 
      method: 'GET', 
      params
    })
  },
  // 查询港口 港区一起返回的接口
  portAreaList: (params:any) => {
    return axios({
      url: `${basicdataBase}/basicdata-seaport/list-port`, 
      method: 'GET', 
      params
    })
  },

  //箱型 箱类
  containerSizeList: (params:any) => {
    return axios({
      url: `${basicdataBase}/basicdata-code/list`, 
      method: 'GET', 
      params
    })
  },
  // 获取常用箱型箱量
  containerSizeTypeOption: () => {
    return axios({
      url: `${basicdataBase}/basicdata-code/commonly-used-box`, 
      method: 'POST', 
    })
  },
  
  // 品类
  category:()=>{
    return axios({
      url: `${productBase}/saleProduct/select-category`, 
      method: 'GET'
    })
  },
  // 销售品类
  categoryBy:(params:any)=>{
    return axios({
      url: `${productBase}/saleProduct/select-by-category-name`, 
      method: 'GET', 
      params
    })
  },
  // 获取所有品名
  getAllDescription:()=>{
    return axios({
      url: `${productBase}/saleProduct/select-all-sale-name`, 
      method: 'GET', 
    })
  },

  // 查询企业币别
  costCenterDropDown: () => {
    return axios({
      url: `${basicdataBase}/basicdata-currency/list`, 
      method: 'GET', 
    }); 
  },
  // 供应商列表
  supplier: (params:any) => {
    return axios({
      url: `${scmBase}/relation/queryRelationByLabelCode`, 
      method: 'GET', 
      params,
    }); 
  },
  // 船公司
  shippingCompany:(params:any)=>{
    return axios({
      url: `${basicdataBase}/basic-data-shipping-company/list`, 
      method: 'GET', 
      params,
    }); 
  },
  // 查询费用项单位
  getCostUnit:(data:any)=>{
    return axios({
      url: `${crmBase}/cost-unit/getCostUnit`, 
      method: 'POST',
      data,
    })
  },



  /**
   * 询报价管理
   *  标准产品
   *  报价模版
   * **/
  
  // 查询标准产品列表
  getstandardProductList: (data:any) => {
    return axios({
      url:`${crmBase}/standard-products/pageList`, 
      method: 'POST',
      data,
    }) 
  },

  // 校验标准产品名称
  checkStandardName: (data:any) => {
    return axios({
      url:`${crmBase}/standard-products/checkStandardName`, 
      method: 'POST',
      data,
    }) 
  },

  // 标准产品详情
  productsDetail: (params:any) => {
    return axios({
      url: `${crmBase}/standard-products/detail`, 
      method: 'GET', 
      params,
    }); 
  },

  // 查询基础费用项表
  costSelectList: (params:any) => {
    return axios({
      url: `${crmBase}/basicdata-cost-item/selectList`, 
      method: 'GET', 
      params,
    });
  },

  // 引入成本
  costList: (data:any) => {
    return axios({
      url:`${crmBase}/standard-products/costList`, 
      method: 'POST',
      data,
    }) 
  },

  // 添加标准产品
  addProducts: (data:any) => {
    return axios({
      url:`${crmBase}/standard-products/add`, 
      method: 'POST',
      data,
    }) 
  },

  // 修改编辑产品
  productsEdit: (data:any) => {
    return axios({
      url:`${crmBase}/standard-products/edit`, 
      method: 'POST',
      data,
    }) 
  },

  // 删除标准产品
  productsDelete: (params:any) => {
    return axios({
      url: `${crmBase}/standard-products/delete`, 
      method: 'GET', 
      params,
    });
  },
  




  /**
   * 合同管理
   *  客户合同列表
   *  供应商合同列表
   * **/
  // 供应商名称   
  supplierName: (params?: any) => {
    return axios({
      url: `${scmBase}/relation/queryRelationByTypesAndLabelCodes`,
      method: 'GET',
      params,
    });
  },
  // 签约抬头
  relationList:(params:any)=>{
    return axios({
      url: `${scmBase}/relation-dock/relation-list`,
      method: 'GET',
      params,
    });
  },

  // 查询用户所属部门列表
  userOrganizationList:(params:any)=>{
    return axios({
      url: `${memberBase}/organization-management/userOrganizationList`,
      method: 'GET',
      params,
    });
  },

  // 根据业务类型查服务项目
  getRelationCodeTypeByCode (params:any) {
    return axios({
      url: `${basicdataBase}/basicdata-code/relation-list`,
      method: 'GET',
      params,
    });
  },

  // 新增双方协议基本信息
  addInfo:(data:any)=>{
    return axios({
      url:`${crmBase}/supplier-contracts-info/addBasic`, 
      method: 'POST',
      data,
    }) 
  },

  // 编辑双方协议基本信息
  editInfo:(data:any)=>{
    return axios({
      url:`${crmBase}/supplier-contracts-info/editBasic`, 
      method: 'POST',
      data,
    }) 
  },

  // 保存&修改双方协议信息
  saveContract:(data:any)=>{
    return axios({
      url:`${crmBase}/supplier-contracts-info/save`, 
      method: 'POST',
      data,
    }) 
  },

  // 补充协议
  // 通过供应商ID获取供应商合同列表
  supplierContractList:(params:any)=>{
    return axios({
      url: `${crmBase}/supplier-contracts-info/supplierContractList`,
      method: 'GET',
      params,
    });
  },

  // 供应商合同签约销售
  salesRole:(data:any)=>{
    return axios({
      url:`${memberBase}/business-role-config/select-by-role`, 
      method: 'POST',
      data,
    })
  },

  // 新增补充协议基本信息
  addSideAgreement:(data:any)=>{
    return axios({
      url:`${crmBase}/supplier-contracts-info/addSideAgreementBasic`, 
      method: 'POST',
      data,
    }) 
  },

  // 修改补充协议基本信息
  editSideAgreementBasic:(data:any)=>{
    return axios({
      url:`${crmBase}/supplier-contracts-info/editSideAgreementBasic`, 
      method: 'POST',
      data,
    }) 
  },

  // 保存补充协议
  saveSideAgreement:(data:any)=>{
    return axios({
      url:`${crmBase}/supplier-contracts-info/saveSideAgreement`, 
      method: 'POST',
      data,
    }) 
  },

  // 修改补充协议
  editSideAgreement:(data:any)=>{
    return axios({
      url:`${crmBase}/supplier-contracts-info/editSideAgreement`, 
      method: 'POST',
      data,
    })
  },
  
  // 供应商合同详情（包含补充协议）
  supplierDetail:(params:any)=>{
    return axios({
      url: `${crmBase}/supplier-contracts-info/detail`,
      method: 'get',
      params
    })
  },

  // 撤回供应商合同
  recallContract:(params:any)=>{
    return axios({
      url: `${crmBase}/supplier-contracts-info/recall`,
      method: 'get',
      params
    })
  },




  // 控货管理解控
  postunControlOrder(data:any) {
    return axios({
      url: `${scmBase}/trade-control/unControlOrder`,
      method: 'POST',
      data
    })
  },
  // 弹窗控货单位下拉列表
     getCompanyList (params:any) {
       return axios({
         url: `${scmBase}/relation/queryRelationByLabelCode`,
         method: 'get',
         params
       })

  },
  // 指定委托人
  getCompanyDataList: (params:any) => {
    return axios({
      // url: `${crmBase}/contract-info/getCustomberVO`,
      url:`${scmBase}/relation-view/queryRelationByDropDown`,
      method: 'get',
      params
    })
  },
  //保存经营单位的控货
  postSaveTradeControlt: (data:any) => {
    return axios({
      url: `${scmBase}/trade-control/saveTradeControl`,
      method: 'post',
      data
    })
  },
  postControlTradeBusinessUnitList: (data?: any) => { //经营单位的控货管理
    return axios({
      url: `${scmBase}/trade-control/queryPageView`,
      method: 'POST',
      data
    })
    },
  posthandControlByOrderlist: (data?: any) => {//控货管理列表
    return axios({
      url: `${scmBase}/trade-control/selectPageByTradeOrderView`,
      method: 'POST',
      data
    })
  },
  postReceivablelist: (data?: any) => {//控货管理列表
    return axios({
      url: `${scmBase}/trade-control/selectPageByReceivableControlView`,
      method: 'POST',
      data
    })
  },
  postSeaTransportlist:  (data ?: any) => {//控货管理列表
  return axios({
    url: `${scmBase}/trade-control/selectPageBySeaTransportView`,
    method: 'POST',
    data
  })
},
  postSaveSeaTransport: (data?: any) => {////保存海运
    return axios({
      url: `${scmBase}/trade-control/saveSeaTransportControl`,
      method: 'POST',
      data
    })
  },
  postDeleteTradeControl: (params: any) => {//删除海运
      return axios({
        url: `${scmBase}/trade-control/deleteTradeControlById`,
        method: 'get',
        params
      })
  },
  getOrderLogisticsNo: (params: any) => {//运单号查询
    return axios({
      url: `${scmBase}/logistics-mos/queryLiskOrderLogisticsNo`,
      method: 'get',
      params
    })
  },
   postSaveReceivable: (data:any) => {
     return axios({
       url: `${scmBase}/trade-control/saveReceivableControl`,
       method: 'POST',
       data
     })
      //保存应收
  },
  postExportBusinessUnit: (data: any) => {   //导出经营单位
    return axios({
      url: `${scmBase}/trade-control/tradeControlExportView`,
      method: 'POST',
      data,
      responseType:"blob" ,
    })

  },
  postExportReceivable: (data: any) => {   //导出应收单位
    return axios({
      url: `${scmBase}/trade-control/exportReceivableControlView`,
      method: 'POST',
      data,
      responseType: "blob",
    })

  },
  postExportSeaTransport: (data: any) => {   //导出海运单位
    return axios({
      url: `${scmBase}/trade-control/exportSeaTransportView`,
      method: 'POST',
      data,
      responseType: "blob",
    })

  },
  postexportTradeOrderControl: (data: any) => {   //导出控货管理
    return axios({
      url: `${scmBase}/trade-control/exportTradeOrderControlView`,
      method: 'POST',
      data,
      responseType: "blob",
    })
  },
  postTaskRecord: (data: any) => {   //客户合同申请审批查询
    return axios({
      url: `${processBase}/mos-task/task-record`,
      method: 'POST',
      data,
    })

  },
  crmSupplierReview: (data: any) => {  //供应商审批
    return axios({
      url: `${crmBase}/process-business/supplierReview`,
      method: 'POST',
      data,
    })

  },
  //  加签人
  getenterpriseidList: (params:any) => {
    return axios({
      url: `${memberBase}/enterprise-user/get-members-by-enterpriseid`,
      method: 'get',
      params,
    })
  },
  getRole: (data: any) => { //业务角色--换单员制单员
    return axios({
      url: `${memberBase}/business-role-config/select-by-role`,
      method: 'POST',
      data,
    })
  },
  //  根据合同ID获取合同报价确认书信息
  getcontractOfferConfirmationList: (params:any) => {
    return axios({
      url: `${crmBase}/contract-offer-confirmation/contractOfferConfirmationList`,
      method: 'get',
      params,
    })
  },
  //  根据合同ID获取合同报价确认书报价分配信息
  getcontractOfferDistributionList: (params:any) => {
    return axios({
      url: `${crmBase}/contract-offer-confirmation/contractOfferDistributionList`,
      method: 'get',
      params,
    })
  },
    //客户合同申请审批
  postcontractReview: (data:any) => {
    return axios({
      url: `${crmBase}/process-business/contractReview`,
      method: 'POST',
      data,
    })
    // return fetch(`${crmBase}/process-business/contractReview`, params, 'post');  //审批
  },
  //联系人
  // /crm/contract-contact/contactList
  contactList: (params: any) => {
    return axios({
      url: `${crmBase}/contract-contact/contactList`,
      method: 'get',
      params,
    })
  },
  // //客户合同申请审批
  // postcontractReview: (params) => {
  //   return fetch(`${crmBase}/process-business/contractReview`, params, 'post');  //审批
  // },

  // 导出供应商合同列表
  exportContractSupplierDownload :(data?: any) => {
    return axios({
      url: `${crmBase}/supplier-contracts-info/exportContracts`,
      method: 'POST',
      data,
      responseType:'blob',
    });
  },

}