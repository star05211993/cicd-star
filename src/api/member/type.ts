// member types
export interface LoginData {
  loginName: string;
  password: string;
  tenantId: string | number;
  token?: string;
  type?: string | number;
}