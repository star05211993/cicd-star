import axios from '@/api/interceptor';
import { memberBase, scmBase } from '@/api/api.config';
import type { LoginData } from './type';
export const memberUser = {
  login: (data: LoginData) => {
    return axios({
      url: `${memberBase}/member-user/login`,
      method: 'POST',
      data,
    });
  },
  // 文件上传
  upload: (data: any) => {
    return axios({
      url: `${memberBase}/upload`,
      method: 'POST',
      data,
      paramsType: true,
      headerType: 'form',
    });
  },
  // 文件上传
  dowload: (params: any) => {
    return axios({
      url: `${memberBase}/dowload`,
      method: 'get',
      params,
      responseType: 'blob',
    });
  },
  loginCode: (data: LoginData) => {
    return axios({
      url: `${memberBase}/user/verify-code-login`,
      method: 'POST',
      data,
    });
  },
  logout: (params?: any) => {
    return axios({
      url: `${memberBase}/member-user/login-out`,
      method: 'GET',
      params,
    });
  },
  getTenantInfo: (data: any) => {
    return axios({
      url: `${memberBase}/tenant-enterprise/selectTenantByDomainName`,
      method: 'POST',
      data,
    });
  },
  // 获取用户信息通过token
  getUserInfoByToken: (params?: any) => {
    return axios({
      url: `${memberBase}/member-user/getMemberUserVOByToken`,
      method: 'GET',
      params,
    });
  },
  getUserInfo: (params?: any) => {
    return axios({
      url: `${memberBase}/member-user/info`,
      method: 'GET',
      params,
    });
  },
  // 获取左侧路由
  getUserRouters: (params?: any) => {
    return axios({
      url: `${memberBase}/authority/basicdata-list`,
      method: 'GET',
      params,
    });
  },
  // 图形验证码
  getCodePic: (params?: any) => {
    return axios({
      url: `${memberBase}/member-user/captcha`,
      method: 'GET',
      params,
    });
  },
  // 获取手机验证码
  sendSmsCode: (data?: any) => {
    return axios({
      url: `${memberBase}/member-user/send-sms-code`,
      method: 'POST',
      data,
    });
  },
  //员工手机号注册
  platformPhoneRegister: (data?: any) => {
    return axios({
      url: `${memberBase}/user-register/platform-phone-register`,
      method: 'POST',
      data,
    });
  },
  //企业手机号注册
  enterprisePhoneRegister: (data?: any) => {
    return axios({
      url: `${memberBase}/user-register/enterprise-phone-register`,
      method: 'POST',
      data,
    });
  },
  //平台邮箱注册
  platformMailRegister: (data?: any) => {
    return axios({
      url: `${memberBase}/user-register/platform-mail-register`,
      method: 'POST',
      data,
    });
  },
  //企业成员邮箱注册
  enterpriseMailRegister: (data?: any) => {
    return axios({
      url: `${memberBase}/user-register/enterprise-mail-register`,
      method: 'POST',
      data,
    });
  },
  //营业执照
  businessLicenseAnalysis: (data?: any) => {
    return axios({
      url: `${memberBase}/enterprise-setting/businessLicense-analysis`,
      method: 'POST',
      data,
      paramsType: true,
      headerType: 'file',
    });
  },
  //企业身份
  enterpriseList: (params?: any) => {
    return axios({
      url: `${memberBase}/basicdata-code/enterprise-identity-list`,
      method: 'GET',
      params,
    });
  },
  //企业认证 -数据校验
  enterpriseData: (data?: any) => {
    return axios({
      url: `${memberBase}/enterprise-setting/enterprise-verification-data-check`,
      method: 'POST',
      data,
    });
  },
  //企业认证 - 身份证识别
  idCardAnalysis: (data?: any) => {
    return axios({
      url: `${memberBase}/enterprise-setting/idCard-analysis`,
      method: 'POST',
      data,
    });
  },
  //企业认证 - 企业注册
  enterpriseRegister: (data?: any) => {
    return axios({
      url: `${memberBase}/enterprise-setting/enterprise-verification-register`,
      method: 'POST',
      data,
    });
  },
  //上传文件
  fileUpload: (data?: any) => {
    return axios({
      url: `${scmBase}/file/upload`,
      method: 'POST',
      data,
      paramsType: true,
      headerType: 'form',
    });
  },
  phoneReset: (data?: any) => {
    return axios({
      url: `${memberBase}/user/phone-retrieve-password`,
      method: 'POST',
      data,
    });
  },
  //手机号-重置密码
  phoneResetPassword: (data?: any) => {
    return axios({
      url: `${memberBase}/user/phone-reset-password`,
      method: 'POST',
      data,
    });
  },
  emailReset: (data?: any) => {
    return axios({
      url: `${memberBase}/user/email-retrieve-password`,
      method: 'POST',
      data,
    });
  },
  //邮箱-重置密码
  emailResetPassword: (data?: any) => {
    return axios({
      url: `${memberBase}/user/email-reset-password`,
      method: 'POST',
      data,
    });
  },
  //首次登录修改密码
  firstLoginPassword: (data?: any) => {
    return axios({
      url: `${memberBase}/user/login-password`,
      method: 'POST',
      data,
    });
  },
  //邮箱验证码
  getEmailCode: (data?: any) => {
    return axios({
      url: `${memberBase}/member-user/send-email-code`,
      method: 'POST',
      data,
    });
  },
  //按钮权限
  getRoleByPage: (params?: any) => {
    return axios({
      url: `${memberBase}/enterprise-role-function/getRoleFunctionListByPage`,
      method: 'GET',
      params,
    });
  },
};
