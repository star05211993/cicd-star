import axios from '@/api/interceptor';
import {
  memberBase,
  scmBase,
  basicdataBase,
  settleBase,
  crmBase,
} from '@/api/api.config';
export const settlementApi = {
  //所属公司
  memberQueryUserAuthorithPart: (params?: any) => {
    return axios({
      url: `${scmBase}/relation-view/queryRelationByDropDown`,
      method: 'GET',
      params,
    });
  },
  //获取企业下的所有成员信息
  getMembersByEnterpriseid: (params?: any) => {
    return axios({
      url: `${memberBase}/enterprise-user/get-members-by-enterpriseid`,
      method: 'GET',
      params,
    });
  },
  //开户银行
  queryRelationBankByEnterpriseId: (params?: any) => {
    return axios({
      url: `${scmBase}/relation/queryRelationBankByEnterpriseId`,
      method: 'GET',
      params,
    });
  },
  //根据企业ID查询开票范围
  queryRelationInvoiceRangeByEnterpriseId: (params?: any) => {
    return axios({
      url: `${scmBase}/relation/queryRelationInvoiceRangeByEnterpriseId`,
      method: 'GET',
      params,
    });
  },

  //根据会员企业ID开票信息详情
  queryInvoiceTitleByEnterpriseId: (params?: any) => {
    return axios({
      url: `${scmBase}/relation/queryInvoiceTitleByEnterpriseId`,
      method: 'GET',
      params,
    });
  },
  //码表
  basicdataCodeSelect: (data?: any) => {
    return axios({
      url: `${basicdataBase}/basicdata-code/select`,
      method: 'POST',
      data,
    });
  },
  //支票号
  getSettleListCheque: (params?: any) => {
    return axios({
      url: `${settleBase}/api/basicdata/list-cheque`,
      method: 'GET',
      params,
    });
  },
  //汇票号
  getSettleListDraft: (params?: any) => {
    return axios({
      url: `${settleBase}/api/basicdata/list-draft`,
      method: 'GET',
      params,
    });
  },
  //请款支付
  paymentrequestPay: (data?: any) => {
    return axios({
      url: `${settleBase}/api/paymentrequest/pay`,
      method: 'POST',
      data,
    });
  },
  //退款详情
  settleReceiptRefundDetail: (params?: any) => {
    return axios({
      url: `${settleBase}/receipt/refund/detail`,
      method: 'GET',
      params,
    });
  },
  //退款详情支付
  settleReceiptRefundPayment: (data?: any) => {
    return axios({
      url: `${settleBase}/receipt/refund/payment`,
      method: 'POST',
      data,
    });
  },
  //退款详情-驳回
  settleReceiptRefundReject: (params?: any) => {
    return axios({
      url: `${settleBase}/receipt/refund/reject`,
      method: 'GET',
      params,
    });
  },
  //退款详情-审批
  settleReceiptRefundConsent: (params?: any) => {
    return axios({
      url: `${settleBase}/receipt/refund/consent`,
      method: 'GET',
      params,
    });
  },
  //退款详情-撤销
  settleReceiptRefundRevoke: (params?: any) => {
    return axios({
      url: `${settleBase}/receipt/refund/revoke`,
      method: 'GET',
      params,
    });
  },
  //退款导出
  settleReceiptRefundExcelExport: (data?: any) => {
    return axios({
      url: `${settleBase}/receipt/refund/excel/export`,
      method: 'POST',
      data,
    });
  },
  //获取币别
  settleBasicEnterpriseCurrencyAll: (params?: any) => {
    return axios({
      url: `${basicdataBase}/currency/all`,
      method: 'GET',
      params,
    });
  },
  //借款详情
  settleLoanDetail: (params?: any) => {
    return axios({
      url: `${settleBase}/api/loan/detail`,
      method: 'GET',
      params,
    });
  },
  //借款支付
  settleLoanPay: (data?: any) => {
    return axios({
      url: `${settleBase}/api/loan/pay`,
      method: 'POST',
      data,
    });
  },
  //借款审批
  settleLoanApproval: (params?: any) => {
    return axios({
      url: `${settleBase}/api/loan/approval`,
      method: 'GET',
      params,
    });
  },
  //撤销借款
  settleLoanRevocation: (params?: any) => {
    return axios({
      url: `${settleBase}/api/loan/revocation`,
      method: 'GET',
      params,
    });
  },
  //借款驳回
  settleLoanReject: (data?: any) => {
    return axios({
      url: `${settleBase}/api/loan/reject`,
      method: 'POST',
      data,
    });
  },
  //申请部门
  departmentsCanapply: (params?: any) => {
    return axios({
      url: `${settleBase}/api/sign/departments/that/canapply`,
      method: 'GET',
      params,
    });
  },
  // 添加业务借款
  settleAddBusinessLoan: (data?: any) => {
    return axios({
      url: `${settleBase}/api/loan/addBusinessLoan`,
      method: 'POST',
      data,
    });
  },
  //添加非业务借款
  settleAddNoBusinessLoan: (data?: any) => {
    return axios({
      url: `${settleBase}/api/loan/addNoBusinessLoan`,
      method: 'POST',
      data,
    });
  },
  //借款列表
  settleLoanPage: (data?: any) => {
    return axios({
      url: `${settleBase}/api/loan/page`,
      method: 'POST',
      data,
    });
  },
  //借款导出
  settleLoanExport: (data?: any) => {
    return axios({
      url: `${settleBase}/api/loan/export`,
      method: 'POST',
      data,
    });
  },
  //退款列表
  settleReceiptRefundPage: (data?: any) => {
    return axios({
      url: `${settleBase}/receipt/refund/page`,
      method: 'POST',
      data,
    });
  },
  //未关账的结算单列表
  statementUnclosed: (params?: any) => {
    return axios({
      url: `${settleBase}/api/statement/unclosed`,
      method: 'GET',
      params,
    });
  },
  //获取关系客户
  queryRelationByLabelCode: (params?: any) => {
    return axios({
      url: `${scmBase}/relation/queryRelationByLabelCode`,
      method: 'GET',
      params,
    });
  },
  //上传附件
  fileToUpload: (data?: any) => {
    return axios({
      url: `${settleBase}/payment/fileToUpload`,
      method: 'POST',
      data,
    });
  },
  //报销列表
  settleReimbursementPage: (data?: any) => {
    return axios({
      url: `${settleBase}/api/reimbursement/page`,
      method: 'POST',
      data,
    });
  },
  //报销导出
  settleReimbursementExport: (data?: any) => {
    return axios({
      url: `${settleBase}/api/reimbursement/export`,
      method: 'POST',
      data,
    });
  },
  //报销查询列表统计
  settleReimbursePageStatistics: (data?: any) => {
    return axios({
      url: `${settleBase}/api/reimbursement/pageStatistics`,
      method: 'POST',
      data,
    });
  },
  //申请报销
  settleReimburseApply: (data?: any) => {
    return axios({
      url: `${settleBase}/api/reimbursement/apply`,
      method: 'POST',
      data,
    });
  },
  //查询报销详情
  settleReimburseDetail: (params?: any) => {
    return axios({
      url: `${settleBase}/api/reimbursement/detail`,
      method: 'GET',
      params,
    });
  },
  //撤销报销
  settleReimburseRevocation: (params?: any) => {
    return axios({
      url: `${settleBase}/api/reimbursement/revocation`,
      method: 'GET',
      params,
    });
  },
  //作废报销
  settleReimburseInvalid: (params?: any) => {
    return axios({
      url: `${settleBase}/api/reimbursement/invalid`,
      method: 'GET',
      params,
    });
  },
  //核对完成
  settleReimburseCheckCompleted: (params?: any) => {
    return axios({
      url: `${settleBase}/api/reimbursement/check_completed`,
      method: 'GET',
      params,
    });
  },
  //报销审批
  settleReimburseApproval: (data?: any) => {
    return axios({
      url: `${settleBase}/api/reimbursement/approval`,
      method: 'POST',
      data,
    });
  },
  // 报销驳回
  settleReimburseReject: (data?: any) => {
    return axios({
      url: `${settleBase}/api/reimbursement/reject`,
      method: 'POST',
      data,
    });
  },
  // 报销支付完成
  settleReimbursePayCompleted: (data?: any) => {
    return axios({
      url: `${settleBase}/api/reimbursement/pay_completed`,
      method: 'POST',
      data,
    });
  },
  // 报销支付
  settleReimbursePay: (data?: any) => {
    return axios({
      url: `${settleBase}/api/reimbursement/pay`,
      method: 'POST',
      data,
    });
  },
  // 根据报销公司id 查询借款单数据
  settleReimburseLoan: (data?: any) => {
    return axios({
      url: `${settleBase}/api/reimbursement/loan`,
      method: 'POST',
      data,
    });
  },
  //业务结算详情-查询可报销成本列表
  statementCostCanReimbursement: (data?: any) => {
    return axios({
      url: `${settleBase}/api/statement/cost/can-reimbursement/details`,
      method: 'POST',
      data,
    });
  },
  // 业务结算详情-批量删除成本
  settleTrackBatchCost: (data?: any) => {
    return axios({
      url: `${settleBase}/api/statement/cost/batch-remove`,
      method: 'POST',
      data,
    });
  },
  //根据企业查询发票开票项信息
  settleBasicEnterpriseInvoiceConfig: (data?: any) => {
    return axios({
      url: `${settleBase}/api/basicdata/enterprise-invoice-config`,
      method: 'POST',
      data,
    });
  },
  // 根据客户和形式协议id绑定合同形式协议（结算） - 申请开票 - 绑定付款方 - 提交
  bindingPayment: (data?: any) => {
    return axios({
      url: `${crmBase}/contract-info/bindingPayment`,
      method: 'POST',
      data,
    });
  },
  // 根据客户和签约公司查询形式协议（结算）
  queryProtocols: (data?: any) => {
    return axios({
      url: `${crmBase}/contract-info/queryProtocols`,
      method: 'POST',
      data,
    });
  },
  // 结算系统更新开票抬头与银行信息
  updateRelationBankAndTitle: (data?: any) => {
    return axios({
      url: `${scmBase}/relation/updateRelationBankAndTitle`,
      method: 'POST',
      data,
    });
  },
  //查询企业发票
  settleBasicEnterpriseInvoice: (data?: any) => {
    return axios({
      url: `${settleBase}/api/basicdata/enterprise-invoice`,
      method: 'POST',
      data,
    });
  },
  // 申请开票-对账单号列表
  invoiceApplicationFilter: (params?: any) => {
    return axios({
      url: `${settleBase}/invoice/application/filter`,
      method: 'GET',
      params,
    });
  },
  // 申请开票-开票
  invoiceApplication: (data?: any) => {
    return axios({
      url: `${settleBase}/invoice/application`,
      method: 'POST',
      data,
    });
  },
  // 申请开票-开票项目
  invoiceApplicationItems: (params?: any) => {
    return axios({
      url: `${settleBase}/invoice/application/items`,
      method: 'GET',
      params,
    });
  },
  //申请线下业务开票-开票
  invoiceApplicationOther: (data?: any) => {
    return axios({
      url: `${settleBase}/invoice/application/other`,
      method: 'POST',
      data,
    });
  },
  //申请报销页-批量修改发票
  statementCostBatchInvoice: (data?: any) => {
    return axios({
      url: `${settleBase}/api/statement/cost/batch/invoice`,
      method: 'POST',
      data,
    });
  },

  //申请报销页-拆分费用
  statementCostSplit: (data?: any) => {
    return axios({
      url: `${settleBase}/api/statement/cost/split`,
      method: 'POST',
      data,
    });
  },
  // 业务结算详情-添加成本
  settleTrackCost: (data?: any) => {
    return axios({
      url: `${settleBase}/api/statement/cost`,
      method: 'POST',
      data,
    });
  },
  //汇率管理列表
  basicdataCurrencyRate: (data?: any) => {
    return axios({
      url: `${settleBase}/api/basicdata/page-enterprise-currency-rate`,
      method: 'POST',
      data,
    });
  },

  basicdataSaveCurrencyRate: (data?: any) => {
    return axios({
      url: `${settleBase}/api/basicdata/save-enterprise-currency-rate`,
      method: 'POST',
      data,
    });
  },
  // 查询汇率
  basicdataEnterpriseCurrencyRate: (data?: any) => {
    return axios({
      url: `${settleBase}/api/basicdata/enterprise-currency-rate`,
      method: 'POST',
      data,
    });
  },
  //出票公司
  memberQueryInvoiceCompany: (params?: any) => {
    return axios({
      url: `${scmBase}/relation-view/queryRelationByDropDown`,
      method: 'GET',
      params,
    });
  },

  // 发票管理列表
  settleInvoicePage: (data?: any) => {
    return axios({
      url: `${settleBase}/invoice/page`,
      method: 'POST',
      data,
    });
  },
  // 发票交接-签收
  invoiceConnecteExecute: (params?: any) => {
    return axios({
      url: `${settleBase}/invoice/connect/execute`,
      method: 'GET',
      params,
    });
  },
  // 发票详情
  invoiceDetail: (params?: any) => {
    return axios({
      url: `${settleBase}/invoice/detail`,
      method: 'GET',
      params,
    });
  },
  // 发票详情-撤销发票
  invoiceDetailRevoke: (params?: any) => {
    return axios({
      url: `${settleBase}/invoice/detail/revoke`,
      method: 'GET',
      params,
    });
  },
  // 发票详情-打印发票
  invoiceDetailPrint: (params?: any) => {
    return axios({
      url: `${settleBase}/invoice/detail/print`,
      method: 'GET',
      params,
    });
  },
  // 发票详情-打印信息
  invoicePrintMessage: (params?: any) => {
    return axios({
      url: `${settleBase}/invoice/detail/print/message`,
      method: 'GET',
      params,
    });
  },
  // 发票详情-确认开票
  invoiceDetailExecute: (params?: any) => {
    return axios({
      url: `${settleBase}/invoice/detail/execute`,
      method: 'GET',
      params,
    });
  },
  // 发票详情-发票号列表
  invoiceDetaiFapiaoList: (params?: any) => {
    return axios({
      url: `${settleBase}/invoice/detail/fapiaoList`,
      method: 'GET',
      params,
    });
  },
  // 发票详情-退回
  invoiceDetaRollback: (data?: any) => {
    return axios({
      url: `${settleBase}/invoice/detail/rollback`,
      method: 'POST',
      data,
    });
  },
  // 预览发票
  invoiceDetailDownload: (data?: any) => {
    return axios({
      url: `${settleBase}/invoice/detail/download`,
      method: 'GET',
      data,
    });
  },

  // 发票池-状态统计
  invoicePoolStatistics: (data?: any) => {
    return axios({
      url: `${settleBase}/invoice/pool/statistics`,
      method: 'POST',
      data,
    });
  },

  // 发票池-分页
  invoicePoolPage: (data?: any) => {
    return axios({
      url: `${settleBase}/invoice/pool/page`,
      method: 'POST',
      data,
    });
  },
  // 发票池-编辑发票
  invoicePoolUpdate: (data?: any) => {
    return axios({
      url: `${settleBase}/invoice/pool/update`,
      method: 'POST',
      data,
    });
  },
  // 发票池-删除发票
  invoicePoolDelete: (params?: any) => {
    return axios({
      url: `${settleBase}/invoice/pool/delete`,
      method: 'GET',
      params,
    });
  },
  // 发票池-批量新增发票
  invoicePoolAdd: (data?: any) => {
    return axios({
      url: `${settleBase}/invoice/pool/add`,
      method: 'POST',
      data,
    });
  },

  // 发票池-刷新库存
  invoicePoolRefresh: (params?: any) => {
    return axios({
      url: `${settleBase}/invoice/pool/refresh`,
      method: 'GET',
      params,
    });
  },

  // 申请红字编码
  invoiceApplyRedcode: (params?: any) => {
    return axios({
      url: `${settleBase}/invoice/apply/redCode`,
      method: 'GET',
      params,
    });
  },

  // 查询红字编码
  invoicePoolQueryRedcode: (params?: any) => {
    return axios({
      url: `${settleBase}/invoice/query/redCode`,
      method: 'GET',
      params,
    });
  },

  //请款分页
  paymentrequestList: (data?: any) => {
    return axios({
      url: `${settleBase}/api/paymentrequest/page`,
      method: 'POST',
      data,
    });
  },
  // 业务结算详情-查询可请款成本列表
  statementCostCanPaymentRequest: (data?: any) => {
    return axios({
      url: `${settleBase}/api/statement/cost/can-payment-request/details`,
      method: 'POST',
      data,
    });
  },
  // 作废发票 已生效-> 作废中,
  invoiceInvalid: (params?: any) => {
    return axios({
      url: `${settleBase}/invoice/invalid`,
      method: 'GET',
      params,
    });
  },

  // 发票导出
  invoiceExport: (data?: any) => {
    return axios({
      url: `${settleBase}/invoice/excel/export`,
      method: 'POST',
      data,
    });
  },
  // 批量开票
  invoiceExecute: (params?: any) => {
    return axios({
      url: `${settleBase}/invoice/pending/execute`,
      method: 'GET',
      params,
    });
  },
  // 批量驳回
  invoiceRevoke: (params?: any, data?: any) => {
    return axios({
      url: `${settleBase}/invoice/pending/revoke`,
      method: 'POST',
      params,
      data,
    });
  },
  // 查询企业产品列表
  settleBasicEnterpriseProduct: (params?: any) => {
    return axios({
      url: `${settleBase}/api/basicdata/enterprise-product`,
      method: 'GET',
      params,
    });
  },
  // 申请开票页-查询可开票的收入列表
  incomeCanInvoiceDetails: (data?: any) => {
    return axios({
      url: `${settleBase}/api/statement/income/can-invoice/details`,
      method: 'POST',
      data,
    });
  },

  //请款详情
  paymentrequestDetail: (params?: any) => {
    return axios({
      url: `${settleBase}/api/paymentrequest/detail`,
      method: 'GET',
      params,
    });
  },
  //请款间接驳回显示
  paymentrequestIndirectlyRejectedDetail: (params?: any) => {
    return axios({
      url: `${settleBase}/api/paymentrequest/indirectlyRejected-detail`,
      method: 'GET',
      params,
    });
  },
  //请款申请提交
  paymentrequestSubmit: (data?: any) => {
    return axios({
      url: `${settleBase}/api/paymentrequest/submit`,
      method: 'POST',
      data,
    });
  },
  //间接驳回再次提交
  paymentrequestSubmitRepeat: (data?: any) => {
    return axios({
      url: `${settleBase}/api/paymentrequest/submit-repeat`,
      method: 'POST',
      data,
    });
  },
  //请款撤销
  paymentrequestRevoke: (params?: any) => {
    return axios({
      url: `${settleBase}/api/paymentrequest/revoke`,
      method: 'GET',
      params,
    });
  },
  //请款作废
  paymentrequestCancel: (params?: any) => {
    return axios({
      url: `${settleBase}/api/paymentrequest/cancel`,
      method: 'GET',
      params,
    });
  },
  //申请请款-查询可请款成本列表
  canPaymentRequestDetails: (data?: any) => {
    return axios({
      url: `${settleBase}/api/statement/cost/can-payment-request/details`,
      method: 'POST',
      data,
    });
  },
  // 请款管理 导出
  paymentrequestExport: (data?: any) => {
    return axios({
      url: `${settleBase}/api/paymentrequest/export`,
      method: 'POST',
      data,
    });
  },
  // 请款驳回
  paymentrequestRejected: (params?: any) => {
    return axios({
      url: `${settleBase}/api/paymentrequest/rejected`,
      method: 'GET',
      params,
    });
  },
  //申请请款-间接驳回
  paymentrequestIndirectlyRejected: (params?: any) => {
    return axios({
      url: `${settleBase}/api/paymentrequest/indirectly-rejected`,
      method: 'GET',
      params,
    });
  },
  //请款审批
  paymentrequestReviewed: (params?: any) => {
    return axios({
      url: `${settleBase}/api/paymentrequest/reviewed`,
      method: 'GET',
      params,
    });
  },
  // 获取费用默认发票
  getLogisticsFeeDefaultInvoice: (params?: any) => {
    return axios({
      url: `${settleBase}/api/logistics/fee/default-invoice`,
      method: 'GET',
      params,
    });
  },
  // 成本费用录入页-费用是否重复
  statementCostRepeat: (params?: any) => {
    return axios({
      url: `${settleBase}/api/statement/cost/repeat`,
      method: 'GET',
      params,
    });
  },
  // 查询企业费用项-成本
  settleBasicEnterpriseCostFee: (data?: any) => {
    return axios({
      url: `${settleBase}/api/basicdata/enterprise-cost-fee`,
      method: 'POST',
      data,
    });
  },
  // 业务结算详情-根据发票号查询发票文件列表
  settleTrackCostInvoiceFiles: (params?: any) => {
    return axios({
      url: `${settleBase}/api/statement/cost/invoice/files`,
      method: 'GET',
      params,
    });
  },
  // 成本费用录入-批量添加成本
  statementCostBatchSave: (data?: any) => {
    return axios({
      url: `${settleBase}/api/statement/cost/batch/save`,
      method: 'POST',
      data,
    });
  },
  //下载excel费用模板
  settleImportFeeGetTemplate: (params?: any) => {
    return axios({
      url: `${settleBase}/api/import-fee/get-template`,
      method: 'GET',
      params,
    });
  },
  // 供应商审批-审核供应商费用
  settleSupplierReview:(data?: any) => {
    return axios({
      url: `${settleBase}/api/supplier/review`,
      method: 'POST',
      data,
    });
  },
  // 获取未导入成本的rpa成本
  settleSupplierListRPANoImport: (params?: any) => {
    return axios({
      url: `${settleBase}/api/supplier/list-rpa-no-import`,
      method: 'GET',
      params,
    });
  },
  // 根据业务编号查询mbl
  settleImportFeeGetMbl: (params?: any) => {
    return axios({
      url: `${settleBase}/api/import-fee/get-mbl`,
      method: 'GET',
      params,
    });
  },
  // 根据mbl编号查询业务编号
  settleImportFeeGetNo: (params?: any) => {
    return axios({
      url: `${settleBase}/api/import-fee/get-no`,
      method: 'GET',
      params,
    });
  },
  // 导入上港港区费用模板数据
  settleImportFeeUploadPort: (data?: any) => {
    return axios({
      url: `${settleBase}/api/import-fee/upload-port`,
      method: 'POST',
      data,
      paramsType: true,
      headerType: 'form',
    });
  },
  // 导入标准费用模板数据
  settleImportFeeUploadStandard: (data?: any) => {
    return axios({
      url: `${settleBase}/api/import-fee/upload-standard`,
      method: 'POST',
      data,
      paramsType: true,
      headerType: 'form',
    });
  },
  // 成本费用查询
  getCostPage: (data?: any) => {
    return axios({
      url: `${settleBase}/api/statement/cost/page`,
      method: 'POST',
      data,
    });
  },
  // 成本费用导出
  getCostExport: (data?: any) => {
    return axios({
      url: `${settleBase}/api/statement/cost/page/export`,
      method: 'POST',
      data,
    });
  },
  // rpa费用分页
  settleSupplierListRPA: (data?: any) => {
    return axios({
      url: `${settleBase}/api/supplier/list-rpa`,
      method: 'POST',
      data,
    });
  },
  // 供应商审批-分页显示供应商费用
  settleSupplierList: (data?: any) => {
    return axios({
      url: `${settleBase}/api/supplier/list`,
      method: 'POST',
      data,
    });
  },
};
