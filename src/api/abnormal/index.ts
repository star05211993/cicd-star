import axios from '@/api/interceptor';
import {
    memberBase,
    scmBase,
    basicdataBase,
    settleBase,
    process
} from '@/api/api.config';
export const abnormalApi = {
    mosRiskPage: (data?: any) => {
        return axios({
            url: `${process}/ult-risk/page`,
            method: 'POST',
            data,
        })
    },
}