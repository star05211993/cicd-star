import axios from '@/api/interceptor';
import { scmBase,memberBase} from '@/api/api.config';
export const receiptDocumentsApi={
    //单证类型查询
    documentTypeList:(params?:any) => {
        return axios({
            url: `${memberBase}/basicdata-code/list`,
            method: 'GET',
            params,
        });
    } ,
    //运单号查询
    getbusinessList:(params?:any) => {
        return axios({
            url: `${scmBase}/logistics-mos/queryLiskOrderLogisticsNo`,
            method: 'GET',
            params,
        });
    },
    //海关编号查询
    getCustomsByNoList:(params?:any) => {
        return axios({
            url: `${scmBase}/logistics-mos/queryOrderLogisticsAndCustomsByNo`,
            method: 'GET',
            params,
        });
    },
    //单证登记
    documentRegisterSave:(data?:any) => {
        return axios({
            url: `${scmBase}/qc/save`,
            method: 'POST',
            data,
        });
    },
    //单证删除
    documentRegisterDelete:(params?:any) => {
        return axios({
            url: `${scmBase}/qc/delete`,
            method: 'GET',
            params,
        });
    },
    //确认签收
    documentConfirm:(data?:any) => {
        return axios({
            url: `${scmBase}/qc/updateSign`,
            method: 'POST',
            data,
        });
    },
    // 打印签收单
    printReceiptList:(params?:any) => {
        return axios({
            url: `${scmBase}/qc/queryListByIds`,
            method: 'GET',
            params,
        });
    },
    // 待领取运单
    unReceiveOrder:(data?:any) => {
        return axios({
            url: `${scmBase}/nanbei/batch-receive-logistics`,
            method: 'POST',
            data,
        });
    },
}