import axios from '@/api/interceptor';
import { memberBase ,scmBase, wmsBase, businessBase, basicdataBase, crmBase} from '@/api/api.config';

// 省份
export function province(params: { countryId: string }) {
  return axios.get(`${basicdataBase}/province/list/${params}`);
}

// 市
export function city(params: { provinceId: string }) {
  return axios.get(`${basicdataBase}/city/list/${params}`);
}

// 区
export function county(params: { cityId: string }) {
  return axios.get(`${basicdataBase}/county/list/${params}`);
}

// 镇
export function getTownListById(params: { countyId: string }) {
  return axios.get(`${basicdataBase}/town/list`, {params});
}