import axios from '@/api/interceptor';
import {  businessBase } from '@/api/api.config';
// 视图详情页-获取所有字段
export function allField(params: { viewSystemId: string }) {
  return axios.get(`${businessBase}/view/all-field`,{ params });
}
// 视图详情页-自定义列保存
export function saveCustomColumn(params: { viewUserId: Number|string ,chooseFields:any[]}) {
  return axios.post(`${businessBase}/view/save-custom-column`,{...params });
}
// 视图详情(列表\编辑\复制)
export function detail(params: { viewUserId: string }) {
  return axios.get(`${businessBase}/view/tab/detail`,{ params });
}