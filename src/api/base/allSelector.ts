import axios from '@/api/interceptor';
export const allSelector = {
  selector: (ajaxOption: any) => axios[ajaxOption.method](`${ajaxOption.url}`, ajaxOption.method == 'get' ? { params: ajaxOption.staticParams } : ajaxOption.staticParams )
}