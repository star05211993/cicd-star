/**
 * *@2022-06-20
 * *@author lss
 * *@describe 自定义列 / 视图管理接口仓库
 */

import axios from '@/api/interceptor';
import { scmBase, businessBase, processBase, notificationBase, crmBase } from '@/api/api.config';

// 获取视图列表
export interface tabViewList<T = any> {
  viewUserId: any;
  viewSystemId: any;
  viewName: any;
  keywordDescription: any;
  data: T;
  code: number;
  message: string;
}

export function getTabViewList(params: { viewType: string }) {
  return axios.get<tabViewList[]>(`${businessBase}/view/tab/list`,{ params });
}

// 视图重命名
/**
  viewUserId	用户视图ID	
  viewName	视图名称
**/
export function rename(params: { viewUserId: string, viewName: string }) {
  return axios.post(`${businessBase}/view/tab/rename`,{ ...params });
}

// 视图详情(列表\编辑\复制)
export function tabDetail(params: { viewUserId: string }) {
  return axios.get(`${businessBase}/view/tab/detail`,{ params });
}

// 删除视图 TODO
export function deleteTab(params: { viewUserId: string }) {
  return axios.get(`${businessBase}/view/tab/delete`,{ params });
}


// 运单视图列表查询 /scm/logistics-mos-view/list/page
export function runOrderList(params: {
  viewUserId: string, // 用户视图ID
  keyword: string, // 搜索关键字
  pageNum: number, // 当前页-默认为1	
  pageSize: number, // 每页大小-默认为10	
  viewFilterRule: '', //筛选规则: 整体为字符串数组JSON格式数组，单条规则之间分割符为|||
  orderByViewSystemFieldId:'', // 排序字段的系统视图字段ID	
  orderByType: '',//排序类型: 降序-desc 升序-asc
}) {
  return axios.post(`${scmBase}/logistics-mos-view/list/page`,{ ...params });
}

// 单证签收视图列表查询 /scm/logistics-mos-view/list/page
export function receiptDocumentList(params: {
  viewUserId: string, // 用户视图ID
  keyword: string, // 搜索关键字
  pageNum: number, // 当前页-默认为1	
  pageSize: number, // 每页大小-默认为10	
  viewFilterRule: '', //筛选规则: 整体为字符串数组JSON格式数组，单条规则之间分割符为|||
  orderByViewSystemFieldId:'', // 排序字段的系统视图字段ID	
  orderByType: '',//排序类型: 降序-desc 升序-asc
}) {
  return axios.post(`${scmBase}/logistics-mos-view/document-sign-list`,{ ...params });
}

// 待领取运单视图列表查询 
export function unReceiveList(params: {
  viewUserId: string, // 用户视图ID
  keyword: string, // 搜索关键字
  pageNum: number, // 当前页-默认为1	
  pageSize: number, // 每页大小-默认为10	
  viewFilterRule: '', //筛选规则: 整体为字符串数组JSON格式数组，单条规则之间分割符为|||
  orderByViewSystemFieldId:'', // 排序字段的系统视图字段ID	
  orderByType: '',//排序类型: 降序-desc 升序-asc
}) {
  return axios.post(`${scmBase}/nanbei/receive-page-view`,{ ...params });
}
// 条件查询集装箱跟踪列表视图
export function receiptContainerTrackingList(params: {
  viewUserId: string, // 用户视图ID
  keyword: string, // 搜索关键字
  pageNum: number, // 当前页-默认为1	
  pageSize: number, // 每页大小-默认为10	
  viewFilterRule: '', //筛选规则: 整体为字符串数组JSON格式数组，单条规则之间分割符为|||
  orderByViewSystemFieldId:'', // 排序字段的系统视图字段ID	
  orderByType: '',//排序类型: 降序-desc 升序-asc
}) {
  return axios.post(`${scmBase}/logistics-mos-view/container-tracking-list`,{ ...params });
}
// 更新视图字段数据（宽度）
// viewUserFieldId: '',	// 用户视图字段ID
export function updateFieldWidth(params: {viewUserFieldId: string,fieldShowsWidth:any}) {
  return axios.post(`${businessBase}/view/update-field`,{ ...params });
}


// 批量获取任务数量
export function getTaskNum(params: { businessNoList: any }) {
  return axios.get(`${processBase}/ult-task/get-num-by-no-list`,{ params });
}
// 批量获取异常数量
export function getRiskNum(params: { businessNoList: any }) {
  return axios.get(`${processBase}/ult-risk/get-num-by-no-list`,{ params });
}
// 批量获取消息数量
export function getInfoNum(params: { businessNoList: any }) {
  return axios.get(`${notificationBase}/stand-message/get-num-by-no-list`,{ params });
}

// 视图新增或保存
export function saveView(params: {
  viewUserId: string, // 用户视图ID
  viewType: number,  // 视图类型	
  viewName: string,	  // 视图名称	
  viewFilterRule: string,	// 筛选规则: 整体为字符串数组JSON格式数组，单条规则之间分割符为|||
  chooseFields: [], //用户选择字段
}) {
  return axios.post(`${businessBase}/view/tab/save-or-update`,{ ...params });
}

// 标准产品视图列表  /crm/standard-products/pageListView
export function standardProduct(params: {
  viewUserId: string, // 用户视图ID
  keyword: string, // 搜索关键字
  pageNum: number, // 当前页-默认为1	
  pageSize: number, // 每页大小-默认为10	
  viewFilterRule: '', //筛选规则: 整体为字符串数组JSON格式数组，单条规则之间分割符为|||
  orderByViewSystemFieldId:'', // 排序字段的系统视图字段ID	
  orderByType: '',//排序类型: 降序-desc 升序-asc
}) {
  return axios.post(`${crmBase}/standard-products/pageListView`,{ ...params });
}

// 供应商合同列表
// /crm/supplier-contracts-info/pageList
export function supplierList(params: {
  viewUserId: string, // 用户视图ID
  keyword: string, // 搜索关键字
  pageNum: number, // 当前页-默认为1	
  pageSize: number, // 每页大小-默认为10	
  viewFilterRule: '', //筛选规则: 整体为字符串数组JSON格式数组，单条规则之间分割符为|||
  orderByViewSystemFieldId:'', // 排序字段的系统视图字段ID	
  orderByType: '',//排序类型: 降序-desc 升序-asc
}) {
  return axios.post(`${crmBase}/supplier-contracts-info/pageList`,{ ...params });
}