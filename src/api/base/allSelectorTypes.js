import { scmBase, basicdataBase, productBase, memberBase, settleBase, crmBase, processBase } from '@/api/api.config';
import { selectorType } from '@/api/base/customSearch';
import { isArray,isFunction } from 'lodash';
// 返回处理后的ajax配置项
export const handleAjaxOption = (params = {}, getParamsFromOut) => {
  const paramFromOut =isFunction(getParamsFromOut)? getParamsFromOut():{};//为数组，或者对象
  if(isArray(paramFromOut)){
    return paramFromOut
  }

  const { ajaxOption } = params
  return { ...ajaxOption, staticParams: { ...ajaxOption.staticParams, ...paramFromOut } }
}
// 返回下拉列表
export const handleList = (res = {}, listOption = {}) => {
  const { dataPath, label,
    value } = listOption
  // 动态取出数据
  const pathArr = dataPath.split('.')
  let list = res || {}
  pathArr.forEach(item => {
    list = list[item] || []
  });
  return list.map(item => ({
    ...item,
    label: label ? item[label] : item,
    value: value ? item[value].toString() : item,
  }))
}
// 
export const fillStaticParams = async () => {
  await selectorType({}).then((res) => {
    res.data.selectorMenuList.forEach(item => {
      try {
        if (mappingUrl[item.name]) {
          mappingUrl[item.name].ajaxOption.staticParams.type = item.type
        }
      } catch (error) {
        console.error(item, '不存在')
      }

    });
  });
  return mappingUrl
}

// 根据选择器类型查询
export const mappingUrl = {
  //#region //!组织部门
  '组织部门': {
    selectOption: { //select 额外配置
      // isSelect:true,//是否选择框 //!选择器A（基础用法/单选）选择器B（星标/单选）选择器C（多选）选择器D（可模糊搜索/多选）选择器E（可模糊搜索/单选) 选择器F（图标组合样式）
      isTreeSelect: true,//是否为级联 //!级联选择器A（四级联选择） 级联选择器B（含模糊搜索选择）
      customSelectType: '',//!S开头代表下拉选择，C开头代表级联：例如SA->选择器A（基础用法/单选）
      multiple: true,// 多选 multiple, 
      subDataPath: 'childrenList',
      subLabel: 'organizationAbbreviation',
      subValue: 'organizationId'
      // allowSearch: true,//模糊 allowSearch
    },
    ajaxOption: {
      url: `${memberBase}/organization-management/enterprise-or-organization-list`,//请求地址
      method: 'get',//请求方式
      staticParams: { //固定参数 
        // countryId:104
      }
    },
    listOption: {//返回数据的路径（dataPath）、下拉列表取值（value）和展示（label）的字段、
      dataPath: 'data',
      label: 'organizationAbbreviation',
      value: 'organizationId'
    },
  },
  //#endregion

  //#region //! 星标 启用 禁用
  '星标': {//多选 
    selectOption: { //select 额外配置
      isSelect: true,//是否选择框 //!选择器A（基础用法/单选）选择器B（星标/单选）选择器C（多选）选择器D（可模糊搜索/多选）选择器E（可模糊搜索/单选) 选择器F（图标组合样式）
      // isCascader:true,//是否为级联 //!级联选择器A（四级联选择） 级联选择器B（含模糊搜索选择）
      customSelectType: '',//!S开头代表下拉选择，C开头代表级联：例如SA->选择器A（基础用法/单选）
      multiple: true// 多选 multiple
    },
    ajaxOption: {
      url: `${scmBase}/logistics-mos-view/selector`,//请求地址
      method: 'get',//请求方式
      staticParams: {
        type: 'star'
      }
    },
    listOption: {//返回数据的路径（dataPath）、下拉列表取值（value）和展示（label）的字段、
      dataPath: 'data.selectorInfoList',
      label: 'name',
      value: 'code'
    }
  },

  //#endregion

  //#region //! 选择器B（多选）选择器B（多选）

  '运单状态': {//选择器B（多选）
    selectOption: { //select 额外配置
      isSelect: true,//是否选择框 //!选择器A（基础用法/单选）选择器B（星标/单选）选择器C（多选）选择器D（可模糊搜索/多选）选择器E（可模糊搜索/单选) 选择器F（图标组合样式）
      // isCascader:true,//是否为级联 //!级联选择器A（四级联选择） 级联选择器B（含模糊搜索选择）
      customSelectType: 'SB',//!S开头代表下拉选择，C开头代表级联：例如SA->选择器A（基础用法/单选）
      multiple: true// 多选 multiple
    },
    ajaxOption: {
      url: `${scmBase}/logistics-mos-view/selector`,//请求地址
      method: 'get',//请求方式
      staticParams: { //固定参数
        type: 'waybill_status'
      }
    },
    listOption: {//返回数据的路径（dataPath）、下拉列表取值（value）和展示（label）的字段、
      dataPath: 'data.selectorInfoList',
      label: 'name',
      value: 'code'
    }
  },
  '运单类型': {
    selectOption: { //select 额外配置
      isSelect: true,//是否选择框 //!选择器A（基础用法/单选）选择器B（星标/单选）选择器C（多选）选择器D（可模糊搜索/多选）选择器E（可模糊搜索/单选) 选择器F（图标组合样式）
      // isCascader:true,//是否为级联 //!级联选择器A（四级联选择） 级联选择器B（含模糊搜索选择）
      customSelectType: 'SB',//!S开头代表下拉选择，C开头代表级联：例如SA->选择器A（基础用法/单选）
      multiple: true// 多选 multiple
    },
    ajaxOption: {
      url: `${basicdataBase}/basicdata-code/list`,//请求地址
      method: 'get',//请求方式
      staticParams: { //固定参数
        codeTypeCode: 'customerbusinesstype'
      }
    },
    listOption: {//返回数据的路径（dataPath）、下拉列表取值（value）和展示（label）的字段、
      dataPath: 'data',
      label: 'codeName',
      value: 'codesValues'
    }
  },
  '提单方式': {
    selectOption: { //select 额外配置
      isSelect: true,//是否选择框 //!选择器A（基础用法/单选）选择器B（星标/单选）选择器C（多选）选择器D（可模糊搜索/多选）选择器E（可模糊搜索/单选) 选择器F（图标组合样式）
      // isCascader:true,//是否为级联 //!级联选择器A（四级联选择） 级联选择器B（含模糊搜索选择）
      customSelectType: 'SB',//!S开头代表下拉选择，C开头代表级联：例如SA->选择器A（基础用法/单选）
      multiple: true// 多选 multiple
    },
    ajaxOption: {
      url: `${scmBase}/logistics-mos-view/selector`,//请求地址
      method: 'get',//请求方式
      staticParams: { //固定参数
        type: 'bl_way'
      }
    },
    listOption: {//返回数据的路径（dataPath）、下拉列表取值（value）和展示（label）的字段、
      dataPath: 'data.selectorInfoList',
      label: 'name',
      value: 'code'
    }
  },
  '电放信息': {
    selectOption: { //select 额外配置
      isSelect: true,//是否选择框 //!选择器A（基础用法/单选）选择器B（星标/单选）选择器C（多选）选择器D（可模糊搜索/多选）选择器E（可模糊搜索/单选) 选择器F（图标组合样式）
      // isCascader:true,//是否为级联 //!级联选择器A（四级联选择） 级联选择器B（含模糊搜索选择）
      customSelectType: 'SB',//!S开头代表下拉选择，C开头代表级联：例如SA->选择器A（基础用法/单选）
      multiple: true// 多选 multiple
    },
    ajaxOption: {
      url: `${scmBase}/logistics-mos-view/selector`,//请求地址
      method: 'get',//请求方式
      staticParams: { //固定参数
        type: 'electronic_release'
      }
    },
    listOption: {//返回数据的路径（dataPath）、下拉列表取值（value）和展示（label）的字段、
      dataPath: 'data.selectorInfoList',
      label: 'name',
      value: 'code'
    }
  },
  '海运费条款': {
    selectOption: { //select 额外配置
      isSelect: true,//是否选择框 //!选择器A（基础用法/单选）选择器B（星标/单选）选择器C（多选）选择器D（可模糊搜索/多选）选择器E（可模糊搜索/单选) 选择器F（图标组合样式）
      // isCascader:true,//是否为级联 //!级联选择器A（四级联选择） 级联选择器B（含模糊搜索选择）
      customSelectType: 'SB',//!S开头代表下拉选择，C开头代表级联：例如SA->选择器A（基础用法/单选）
      multiple: true// 多选 multiple
    },
    ajaxOption: {
      url: `${scmBase}/logistics-mos-view/selector`,//请求地址
      method: 'get',//请求方式
      staticParams: { //固定参数
        type: 'freight_term'
      }
    },
    listOption: {//返回数据的路径（dataPath）、下拉列表取值（value）和展示（label）的字段、
      dataPath: 'data.selectorInfoList',
      label: 'name',
      value: 'code'
    }
  },
  '品类': {///!!!显示字段:<String> list 
    // 回传字段: String 
    selectOption: { //select 额外配置
      isSelect: true,//是否选择框 //!选择器A（基础用法/单选）选择器B（星标/单选）选择器C（多选）选择器D（可模糊搜索/多选）选择器E（可模糊搜索/单选) 选择器F（图标组合样式）
      // isCascader:true,//是否为级联 //!级联选择器A（四级联选择） 级联选择器B（含模糊搜索选择）
      customSelectType: 'SB',//!S开头代表下拉选择，C开头代表级联：例如SA->选择器A（基础用法/单选）
      multiple: true// 多选 multiple
    },
    ajaxOption: {
      url: `${productBase}/saleProduct/select-category`,//请求地址
      method: 'get',//请求方式
      staticParams: { //固定参数

      }
    },
    listOption: {//返回数据的路径（dataPath）、下拉列表取值（value）和展示（label）的字段、
      dataPath: 'data',
      label: '',
      value: ''
    }
  },
  '货箱类型': {
    selectOption: { //select 额外配置
      isSelect: true,//是否选择框 //!选择器A（基础用法/单选）选择器B（星标/单选）选择器C（多选）选择器D（可模糊搜索/多选）选择器E（可模糊搜索/单选) 选择器F（图标组合样式）
      // isCascader:true,//是否为级联 //!级联选择器A（四级联选择） 级联选择器B（含模糊搜索选择）
      customSelectType: 'SB',//!S开头代表下拉选择，C开头代表级联：例如SA->选择器A（基础用法/单选）
      multiple: true// 多选 multiple
    },
    ajaxOption: {
      url: `${memberBase}/basicdata-code/list`,//请求地址
      method: 'get',//请求方式
      staticParams: { //固定参数
        codeTypeCode: 'shippingtype'
      }
    },
    listOption: {//返回数据的路径（dataPath）、下拉列表取值（value）和展示（label）的字段、
      dataPath: 'data',
      label: 'codeName',
      value: 'codesValues'
    }
  },
  '最近审单结果': {
    selectOption: { //select 额外配置
      isSelect: true,//是否选择框 //!选择器A（基础用法/单选）选择器B（星标/单选）选择器C（多选）选择器D（可模糊搜索/多选）选择器E（可模糊搜索/单选) 选择器F（图标组合样式）
      // isCascader:true,//是否为级联 //!级联选择器A（四级联选择） 级联选择器B（含模糊搜索选择）
      customSelectType: 'SB',//!S开头代表下拉选择，C开头代表级联：例如SA->选择器A（基础用法/单选）
      multiple: true// 多选 multiple
    },
    ajaxOption: {
      url: `${scmBase}/logistics-mos-view/selector`,//请求地址
      method: 'get',//请求方式
      staticParams: { //固定参数
        type: 'document_review_result'
      }
    },
    listOption: {//返回数据的路径（dataPath）、下拉列表取值（value）和展示（label）的字段、
      dataPath: 'data.selectorInfoList',
      label: 'name',
      value: 'code'
    }
  },
  '审单状态': {
    selectOption: { //select 额外配置
      isSelect: true,//是否选择框 //!选择器A（基础用法/单选）选择器B（星标/单选）选择器C（多选）选择器D（可模糊搜索/多选）选择器E（可模糊搜索/单选) 选择器F（图标组合样式）
      // isCascader:true,//是否为级联 //!级联选择器A（四级联选择） 级联选择器B（含模糊搜索选择）
      customSelectType: 'SB',//!S开头代表下拉选择，C开头代表级联：例如SA->选择器A（基础用法/单选）
      multiple: true// 多选 multiple
    },
    ajaxOption: {
      url: `${scmBase}/logistics-mos-view/selector`,//请求地址
      method: 'get',//请求方式
      staticParams: { //固定参数
        type: 'document_review_status'
      }
    },
    listOption: {//返回数据的路径（dataPath）、下拉列表取值（value）和展示（label）的字段、
      dataPath: 'data.selectorInfoList',
      label: 'name',
      value: 'code'
    }
  },
  '货物清单审核状态': {
    selectOption: { //select 额外配置
      isSelect: true,//是否选择框 //!选择器A（基础用法/单选）选择器B（星标/单选）选择器C（多选）选择器D（可模糊搜索/多选）选择器E（可模糊搜索/单选) 选择器F（图标组合样式）
      // isCascader:true,//是否为级联 //!级联选择器A（四级联选择） 级联选择器B（含模糊搜索选择）
      customSelectType: 'SB',//!S开头代表下拉选择，C开头代表级联：例如SA->选择器A（基础用法/单选）
      multiple: true// 多选 multiple
    },
    ajaxOption: {
      url: `${scmBase}/logistics-mos-view/selector`,//请求地址
      method: 'get',//请求方式
      staticParams: { //固定参数
        type: 'cargo_list_approve_status'
      }
    },
    listOption: {//返回数据的路径（dataPath）、下拉列表取值（value）和展示（label）的字段、
      dataPath: 'data.selectorInfoList',
      label: 'name',
      value: 'code'
    }
  },
  'EHC状态': {
    selectOption: { //select 额外配置
      isSelect: true,//是否选择框 //!选择器A（基础用法/单选）选择器B（星标/单选）选择器C（多选）选择器D（可模糊搜索/多选）选择器E（可模糊搜索/单选) 选择器F（图标组合样式）
      // isCascader:true,//是否为级联 //!级联选择器A（四级联选择） 级联选择器B（含模糊搜索选择）
      customSelectType: 'SB',//!S开头代表下拉选择，C开头代表级联：例如SA->选择器A（基础用法/单选）
      multiple: true// 多选 multiple
    },
    ajaxOption: {
      url: `${scmBase}/logistics-mos-view/selector`,//请求地址
      method: 'get',//请求方式
      staticParams: { //固定参数
        type: 'ehc_status'
      }
    },
    listOption: {//返回数据的路径（dataPath）、下拉列表取值（value）和展示（label）的字段、
      dataPath: 'data.selectorInfoList',
      label: 'name',
      value: 'code'
    }
  },
  '正本签收状态': {
    selectOption: { //select 额外配置
      isSelect: true,//是否选择框 //!选择器A（基础用法/单选）选择器B（星标/单选）选择器C（多选）选择器D（可模糊搜索/多选）选择器E（可模糊搜索/单选) 选择器F（图标组合样式）
      // isCascader:true,//是否为级联 //!级联选择器A（四级联选择） 级联选择器B（含模糊搜索选择）
      customSelectType: 'SB',//!S开头代表下拉选择，C开头代表级联：例如SA->选择器A（基础用法/单选）
      multiple: true// 多选 multiple
    },
    ajaxOption: {
      url: `${scmBase}/logistics-mos-view/selector`,//请求地址
      method: 'get',//请求方式
      staticParams: { //固定参数
        type: 'original_signature'
      }
    },
    listOption: {//返回数据的路径（dataPath）、下拉列表取值（value）和展示（label）的字段、
      dataPath: 'data.selectorInfoList',
      label: 'name',
      value: 'code'
    }
  },
  '换单派单类型': {
    selectOption: { //select 额外配置
      isSelect: true,//是否选择框 //!选择器A（基础用法/单选）选择器B（星标/单选）选择器C（多选）选择器D（可模糊搜索/多选）选择器E（可模糊搜索/单选) 选择器F（图标组合样式）
      // isCascader:true,//是否为级联 //!级联选择器A（四级联选择） 级联选择器B（含模糊搜索选择）
      customSelectType: 'SB',//!S开头代表下拉选择，C开头代表级联：例如SA->选择器A（基础用法/单选）
      multiple: true// 多选 multiple
    },
    ajaxOption: {
      url: `${scmBase}/logistics-mos-view/selector`,//请求地址
      method: 'get',//请求方式
      staticParams: { //固定参数
        type: 'urgent'
      }
    },
    listOption: {//返回数据的路径（dataPath）、下拉列表取值（value）和展示（label）的字段、
      dataPath: 'data.selectorInfoList',
      label: 'name',
      value: 'code'
    }
  },
  '换单阶段': {
    selectOption: { //select 额外配置
      isSelect: true,//是否选择框 //!选择器A（基础用法/单选）选择器B（星标/单选）选择器C（多选）选择器D（可模糊搜索/多选）选择器E（可模糊搜索/单选) 选择器F（图标组合样式）
      // isCascader:true,//是否为级联 //!级联选择器A（四级联选择） 级联选择器B（含模糊搜索选择）
      customSelectType: 'SB',//!S开头代表下拉选择，C开头代表级联：例如SA->选择器A（基础用法/单选）
      multiple: true// 多选 multiple
    },
    ajaxOption: {
      url: `${scmBase}/logistics-mos-view/selector`,//请求地址
      method: 'get',//请求方式
      staticParams: { //固定参数
        type: 'exchangedo_stage'
      }
    },
    listOption: {//返回数据的路径（dataPath）、下拉列表取值（value）和展示（label）的字段、
      dataPath: 'data.selectorInfoList',
      label: 'name',
      value: 'code'
    }
  },
  '换单进度': {
    selectOption: { //select 额外配置
      isSelect: true,//是否选择框 //!选择器A（基础用法/单选）选择器B（星标/单选）选择器C（多选）选择器D（可模糊搜索/多选）选择器E（可模糊搜索/单选) 选择器F（图标组合样式）
      // isCascader:true,//是否为级联 //!级联选择器A（四级联选择） 级联选择器B（含模糊搜索选择）
      customSelectType: 'SB',//!S开头代表下拉选择，C开头代表级联：例如SA->选择器A（基础用法/单选）
      multiple: true// 多选 multiple
    },
    ajaxOption: {
      url: `${scmBase}/logistics-mos-view/selector`,//请求地址
      method: 'get',//请求方式
      staticParams: { //固定参数
        type: 'exchangedo_progress'
      }
    },
    listOption: {//返回数据的路径（dataPath）、下拉列表取值（value）和展示（label）的字段、
      dataPath: 'data.selectorInfoList',
      label: 'name',
      value: 'code'
    }
  },
  '换单状态': {
    selectOption: { //select 额外配置
      isSelect: true,//是否选择框 //!选择器A（基础用法/单选）选择器B（星标/单选）选择器C（多选）选择器D（可模糊搜索/多选）选择器E（可模糊搜索/单选) 选择器F（图标组合样式）
      // isCascader:true,//是否为级联 //!级联选择器A（四级联选择） 级联选择器B（含模糊搜索选择）
      customSelectType: 'SB',//!S开头代表下拉选择，C开头代表级联：例如SA->选择器A（基础用法/单选）
      multiple: true// 多选 multiple
    },
    ajaxOption: {
      url: `${scmBase}/logistics-mos-view/selector`,//请求地址
      method: 'get',//请求方式
      staticParams: { //固定参数
        type: 'exchangedo_status'
      }
    },
    listOption: {//返回数据的路径（dataPath）、下拉列表取值（value）和展示（label）的字段、
      dataPath: 'data.selectorInfoList',
      label: 'name',
      value: 'code'
    }
  },
  '海运费支付类型': {
    selectOption: { //select 额外配置
      isSelect: true,//是否选择框 //!选择器A（基础用法/单选）选择器B（星标/单选）选择器C（多选）选择器D（可模糊搜索/多选）选择器E（可模糊搜索/单选) 选择器F（图标组合样式）
      // isCascader:true,//是否为级联 //!级联选择器A（四级联选择） 级联选择器B（含模糊搜索选择）
      customSelectType: 'SB',//!S开头代表下拉选择，C开头代表级联：例如SA->选择器A（基础用法/单选）
      multiple: true// 多选 multiple
    },
    ajaxOption: {
      url: `${scmBase}/logistics-mos-view/selector`,//请求地址
      method: 'get',//请求方式
      staticParams: { //固定参数
        type: 'sea_payment_type'
      }
    },
    listOption: {//返回数据的路径（dataPath）、下拉列表取值（value）和展示（label）的字段、
      dataPath: 'data.selectorInfoList',
      label: 'name',
      value: 'code'
    }
  },
  '海运费状态': {
    selectOption: { //select 额外配置
      isSelect: true,//是否选择框 //!选择器A（基础用法/单选）选择器B（星标/单选）选择器C（多选）选择器D（可模糊搜索/多选）选择器E（可模糊搜索/单选) 选择器F（图标组合样式）
      // isCascader:true,//是否为级联 //!级联选择器A（四级联选择） 级联选择器B（含模糊搜索选择）
      customSelectType: 'SB',//!S开头代表下拉选择，C开头代表级联：例如SA->选择器A（基础用法/单选）
      multiple: true// 多选 multiple
    },
    ajaxOption: {
      url: `${scmBase}/logistics-mos-view/selector`,//请求地址
      method: 'get',//请求方式
      staticParams: { //固定参数
        type: 'sea_freight_status'
      }
    },
    listOption: {//返回数据的路径（dataPath）、下拉列表取值（value）和展示（label）的字段、
      dataPath: 'data.selectorInfoList',
      label: 'name',
      value: 'code'
    }
  },
  '制单派单类型': {
    selectOption: { //select 额外配置
      isSelect: true,//是否选择框 //!选择器A（基础用法/单选）选择器B（星标/单选）选择器C（多选）选择器D（可模糊搜索/多选）选择器E（可模糊搜索/单选) 选择器F（图标组合样式）
      // isCascader:true,//是否为级联 //!级联选择器A（四级联选择） 级联选择器B（含模糊搜索选择）
      customSelectType: 'SB',//!S开头代表下拉选择，C开头代表级联：例如SA->选择器A（基础用法/单选）
      multiple: true// 多选 multiple
    },
    ajaxOption: {
      url: `${scmBase}/logistics-mos-view/selector`,//请求地址
      method: 'get',//请求方式
      staticParams: { //固定参数
        type: 'preparation_urgent_flag'
      }
    },
    listOption: {//返回数据的路径（dataPath）、下拉列表取值（value）和展示（label）的字段、
      dataPath: 'data.selectorInfoList',
      label: 'name',
      value: 'code'
    }
  },
  '查验类型': {
    selectOption: { //select 额外配置
      isSelect: true,//是否选择框 //!选择器A（基础用法/单选）选择器B（星标/单选）选择器C（多选）选择器D（可模糊搜索/多选）选择器E（可模糊搜索/单选) 选择器F（图标组合样式）
      // isCascader:true,//是否为级联 //!级联选择器A（四级联选择） 级联选择器B（含模糊搜索选择）
      customSelectType: 'SB',//!S开头代表下拉选择，C开头代表级联：例如SA->选择器A（基础用法/单选）
      multiple: true// 多选 multiple
    },
    ajaxOption: {
      url: `${scmBase}/logistics-mos-view/selector`,//请求地址
      method: 'get',//请求方式
      staticParams: { //固定参数
        type: 'inspection_type'
      }
    },
    listOption: {//返回数据的路径（dataPath）、下拉列表取值（value）和展示（label）的字段、
      dataPath: 'data.selectorInfoList',
      label: 'name',
      value: 'code'
    }
  },
  '查验方式': {
    selectOption: { //select 额外配置
      isSelect: true,//是否选择框 //!选择器A（基础用法/单选）选择器B（星标/单选）选择器C（多选）选择器D（可模糊搜索/多选）选择器E（可模糊搜索/单选) 选择器F（图标组合样式）
      // isCascader:true,//是否为级联 //!级联选择器A（四级联选择） 级联选择器B（含模糊搜索选择）
      customSelectType: 'SB',//!S开头代表下拉选择，C开头代表级联：例如SA->选择器A（基础用法/单选）
      multiple: true// 多选 multiple
    },
    ajaxOption: {
      url: `${scmBase}/logistics-mos-view/selector`,//请求地址
      method: 'get',//请求方式
      staticParams: { //固定参数
        type: 'inspection_mode'
      }
    },
    listOption: {//返回数据的路径（dataPath）、下拉列表取值（value）和展示（label）的字段、
      dataPath: 'data.selectorInfoList',
      label: 'name',
      value: 'code'
    }
  },
  '检查类型': {
    selectOption: { //select 额外配置
      isSelect: true,//是否选择框 //!选择器A（基础用法/单选）选择器B（星标/单选）选择器C（多选）选择器D（可模糊搜索/多选）选择器E（可模糊搜索/单选) 选择器F（图标组合样式）
      // isCascader:true,//是否为级联 //!级联选择器A（四级联选择） 级联选择器B（含模糊搜索选择）
      customSelectType: 'SB',//!S开头代表下拉选择，C开头代表级联：例如SA->选择器A（基础用法/单选）
      multiple: true// 多选 multiple
    },
    ajaxOption: {
      url: `${scmBase}/logistics-mos-view/selector`,//请求地址
      method: 'get',//请求方式
      staticParams: { //固定参数
        type: 'inspection_type_name'
      }
    },
    listOption: {//返回数据的路径（dataPath）、下拉列表取值（value）和展示（label）的字段、
      dataPath: 'data.selectorInfoList',
      label: 'name',
      value: 'code'
    }
  },
  '入库类型': {
    selectOption: { //select 额外配置
      isSelect: true,//是否选择框 //!选择器A（基础用法/单选）选择器B（星标/单选）选择器C（多选）选择器D（可模糊搜索/多选）选择器E（可模糊搜索/单选) 选择器F（图标组合样式）
      // isCascader:true,//是否为级联 //!级联选择器A（四级联选择） 级联选择器B（含模糊搜索选择）
      customSelectType: 'SB',//!S开头代表下拉选择，C开头代表级联：例如SA->选择器A（基础用法/单选）
      multiple: true// 多选 multiple
    },
    ajaxOption: {
      url: `${scmBase}/logistics-mos-view/selector`,//请求地址
      method: 'get',//请求方式
      staticParams: { //固定参数
        type: 'ware_housing_type'
      }
    },
    listOption: {//返回数据的路径（dataPath）、下拉列表取值（value）和展示（label）的字段、
      dataPath: 'data.selectorInfoList',
      label: 'name',
      value: 'code'
    }
  },
  '签收单类型': {
    selectOption: { //select 额外配置
      isSelect: true,//是否选择框 //!选择器A（基础用法/单选）选择器B（星标/单选）选择器C（多选）选择器D（可模糊搜索/多选）选择器E（可模糊搜索/单选) 选择器F（图标组合样式）
      // isCascader:true,//是否为级联 //!级联选择器A（四级联选择） 级联选择器B（含模糊搜索选择）
      customSelectType: 'SB',//!S开头代表下拉选择，C开头代表级联：例如SA->选择器A（基础用法/单选）
      multiple: true// 多选 multiple
    },
    ajaxOption: {
      url: `${scmBase}/logistics-mos-view/selector`,//请求地址
      method: 'get',//请求方式
      staticParams: { //固定参数
        type: 'receipt_type'
      }
    },
    listOption: {//返回数据的路径（dataPath）、下拉列表取值（value）和展示（label）的字段、
      dataPath: 'data.selectorInfoList',
      label: 'name',
      value: 'code'
    }
  },
  '集装箱状态': {
    selectOption: { //select 额外配置
      isSelect: true,//是否选择框 //!选择器A（基础用法/单选）选择器B（星标/单选）选择器C（多选）选择器D（可模糊搜索/多选）选择器E（可模糊搜索/单选) 选择器F（图标组合样式）
      // isCascader:true,//是否为级联 //!级联选择器A（四级联选择） 级联选择器B（含模糊搜索选择）
      customSelectType: 'SB',//!S开头代表下拉选择，C开头代表级联：例如SA->选择器A（基础用法/单选）
      multiple: true// 多选 multiple
    },
    ajaxOption: {
      url: `${scmBase}/logistics-mos-view/selector`,//请求地址
      method: 'get',//请求方式
      staticParams: { //固定参数
        type: 'container_status'
      }
    },
    listOption: {//返回数据的路径（dataPath）、下拉列表取值（value）和展示（label）的字段、
      dataPath: 'data.selectorInfoList',
      label: 'name',
      value: 'code'
    }
  },
  '税金支付类型': {
    selectOption: { //select 额外配置
      isSelect: true,//是否选择框 //!选择器A（基础用法/单选）选择器B（星标/单选）选择器C（多选）选择器D（可模糊搜索/多选）选择器E（可模糊搜索/单选) 选择器F（图标组合样式）
      // isCascader:true,//是否为级联 //!级联选择器A（四级联选择） 级联选择器B（含模糊搜索选择）
      customSelectType: 'SB',//!S开头代表下拉选择，C开头代表级联：例如SA->选择器A（基础用法/单选）
      multiple: true// 多选 multiple
    },
    ajaxOption: {
      url: `${scmBase}/logistics-mos-view/selector`,//请求地址
      method: 'get',//请求方式
      staticParams: { //固定参数
        type: 'tax_payment_type'
      }
    },
    listOption: {//返回数据的路径（dataPath）、下拉列表取值（value）和展示（label）的字段、
      dataPath: 'data.selectorInfoList',
      label: 'name',
      value: 'code'
    }
  },
  '税金状态': {
    selectOption: { //select 额外配置
      isSelect: true,//是否选择框 //!选择器A（基础用法/单选）选择器B（星标/单选）选择器C（多选）选择器D（可模糊搜索/多选）选择器E（可模糊搜索/单选) 选择器F（图标组合样式）
      // isCascader:true,//是否为级联 //!级联选择器A（四级联选择） 级联选择器B（含模糊搜索选择）
      customSelectType: 'SB',//!S开头代表下拉选择，C开头代表级联：例如SA->选择器A（基础用法/单选）
      multiple: true // 多选 multiple
    },
    ajaxOption: {
      url: `${scmBase}/logistics-mos-view/selector`,//请求地址
      method: 'get',//请求方式
      staticParams: { //固定参数
        type: 'tax_status'
      }
    },
    listOption: {//返回数据的路径（dataPath）、下拉列表取值（value）和展示（label）的字段、
      dataPath: 'data.selectorInfoList',
      label: 'name',
      value: 'code'
    }
  },
  '客商状态': {
    selectOption: { //select 额外配置
      isSelect: true,//是否选择框 //!选择器A（基础用法/单选）选择器B（星标/单选）选择器C（多选）选择器D（可模糊搜索/多选）选择器E（可模糊搜索/单选) 选择器F（图标组合样式）
      // isCascader:true,//是否为级联 //!级联选择器A（四级联选择） 级联选择器B（含模糊搜索选择）
      customSelectType: 'SB',//!S开头代表下拉选择，C开头代表级联：例如SA->选择器A（基础用法/单选）
      multiple: true// 多选 multiple
    },
    ajaxOption: {
      url: `${scmBase}/logistics-mos-view/selector`,//请求地址
      method: 'get',//请求方式
      staticParams: { //固定参数
        type: 'data_status'
      }
    },
    listOption: {//返回数据的路径（dataPath）、下拉列表取值（value）和展示（label）的字段、
      dataPath: 'data.selectorInfoList',
      label: 'name',
      value: 'code'
    }
  },
  '所在区域': {
    selectOption: { //select 额外配置
      isSelect: true,//是否选择框 //!选择器A（基础用法/单选）选择器B（星标/单选）选择器C（多选）选择器D（可模糊搜索/多选）选择器E（可模糊搜索/单选) 选择器F（图标组合样式）
      // isCascader:true,//是否为级联 //!级联选择器A（四级联选择） 级联选择器B（含模糊搜索选择）
      customSelectType: 'SB',//!S开头代表下拉选择，C开头代表级联：例如SA->选择器A（基础用法/单选）
      multiple: true// 多选 multiple
    },
    ajaxOption: {
      url: `${scmBase}/logistics-mos-view/selector`,//请求地址
      method: 'get',//请求方式
      staticParams: { //固定参数
        type: 'region'
      }
    },
    listOption: {//返回数据的路径（dataPath）、下拉列表取值（value）和展示（label）的字段、
      dataPath: 'data.selectorInfoList',
      label: 'name',
      value: 'code'
    }
  },
  '企业属性大类': {
    selectOption: { //select 额外配置
      isSelect: true,//是否选择框 //!选择器A（基础用法/单选）选择器B（星标/单选）选择器C（多选）选择器D（可模糊搜索/多选）选择器E（可模糊搜索/单选) 选择器F（图标组合样式）
      // isCascader:true,//是否为级联 //!级联选择器A（四级联选择） 级联选择器B（含模糊搜索选择）
      customSelectType: 'SB',//!S开头代表下拉选择，C开头代表级联：例如SA->选择器A（基础用法/单选）
      multiple: true// 多选 multiple
    },
    ajaxOption: {
      url: `${scmBase}/logistics-mos-view/selector`,//请求地址
      method: 'get',//请求方式
      staticParams: { //固定参数
        type: 'enterprise_attributes'
      }
    },
    listOption: {//返回数据的路径（dataPath）、下拉列表取值（value）和展示（label）的字段、
      dataPath: 'data.selectorInfoList',
      label: 'name',
      value: 'code'
    }
  },
  '企业属性小类': {
    selectOption: { //select 额外配置
      isSelect: true,//是否选择框 //!选择器A（基础用法/单选）选择器B（星标/单选）选择器C（多选）选择器D（可模糊搜索/多选）选择器E（可模糊搜索/单选) 选择器F（图标组合样式）
      // isCascader:true,//是否为级联 //!级联选择器A（四级联选择） 级联选择器B（含模糊搜索选择）
      customSelectType: 'SB',//!S开头代表下拉选择，C开头代表级联：例如SA->选择器A（基础用法/单选）
      multiple: true// 多选 multiple
    },
    ajaxOption: {
      url: `${scmBase}/relation/query-relation-label-all`,//请求地址
      method: 'get',//请求方式
      staticParams: { //固定参数
        type: ''
      }
    },
    listOption: {//返回数据的路径（dataPath）、下拉列表取值（value）和展示（label）的字段、
      dataPath: 'data',
      label: 'labelName',
      value: 'labelCode'
    }
  },
  '控货状态': {
    selectOption: { //select 额外配置
      isSelect: true,//是否选择框 //!选择器A（基础用法/单选）选择器B（星标/单选）选择器C（多选）选择器D（可模糊搜索/多选）选择器E（可模糊搜索/单选) 选择器F（图标组合样式）
      // isCascader:true,//是否为级联 //!级联选择器A（四级联选择） 级联选择器B（含模糊搜索选择）
      customSelectType: 'SB',//!S开头代表下拉选择，C开头代表级联：例如SA->选择器A（基础用法/单选）
      multiple: true// 多选 multiple
    },
    ajaxOption: {
      url: `${scmBase}/logistics-mos-view/selector`,//请求地址
      method: 'get',//请求方式
      staticParams: { //固定参数
        type: 'control_status'
      }
    },
    listOption: {//返回数据的路径（dataPath）、下拉列表取值（value）和展示（label）的字段、
      dataPath: 'data.selectorInfoList',
      label: 'name',
      value: 'code'
    }
  },
  '控货类型': {
    selectOption: { //select 额外配置
      isSelect: true,//是否选择框 //!选择器A（基础用法/单选）选择器B（星标/单选）选择器C（多选）选择器D（可模糊搜索/多选）选择器E（可模糊搜索/单选) 选择器F（图标组合样式）
      // isCascader:true,//是否为级联 //!级联选择器A（四级联选择） 级联选择器B（含模糊搜索选择）
      customSelectType: 'SB',//!S开头代表下拉选择，C开头代表级联：例如SA->选择器A（基础用法/单选）
      multiple: true// 多选 multiple
    },
    ajaxOption: {
      url: `${scmBase}/logistics-mos-view/selector`,//请求地址
      method: 'get',//请求方式
      staticParams: { //固定参数
        type: 'control_type'
      }
    },
    listOption: {//返回数据的路径（dataPath）、下拉列表取值（value）和展示（label）的字段、
      dataPath: 'data.selectorInfoList',
      label: 'name',
      value: 'code'
    }
  },
  '控货合同状态': {
    selectOption: { //select 额外配置
      isSelect: true,//是否选择框 //!选择器A（基础用法/单选）选择器B（星标/单选）选择器C（多选）选择器D（可模糊搜索/多选）选择器E（可模糊搜索/单选) 选择器F（图标组合样式）
      // isCascader:true,//是否为级联 //!级联选择器A（四级联选择） 级联选择器B（含模糊搜索选择）
      customSelectType: 'SB',//!S开头代表下拉选择，C开头代表级联：例如SA->选择器A（基础用法/单选）
      multiple: true// 多选 multiple
    },
    ajaxOption: {
      url: `${scmBase}/logistics-mos-view/selector`,//请求地址
      method: 'get',//请求方式
      staticParams: { //固定参数
        type: 'valid_status'
      }
    },
    listOption: {//返回数据的路径（dataPath）、下拉列表取值（value）和展示（label）的字段、
      dataPath: 'data.selectorInfoList',
      label: 'name',
      value: 'code'
    }
  },
  '客户合同审批状态': {
    selectOption: { //select 额外配置
      isSelect: true,//是否选择框 //!选择器A（基础用法/单选）选择器B（星标/单选）选择器C（多选）选择器D（可模糊搜索/多选）选择器E（可模糊搜索/单选) 选择器F（图标组合样式）
      // isCascader:true,//是否为级联 //!级联选择器A（四级联选择） 级联选择器B（含模糊搜索选择）
      customSelectType: 'SC',//!S开头代表下拉选择，C开头代表级联：例如SA->选择器A（基础用法/单选）
      multiple: true// 多选 multiple
    },
    ajaxOption: {
      url: `${crmBase}/common/selectListEnum`,//请求地址
      method: 'get', // 请求方式
      staticParams: { //固定参数
        enumName:"CustomerContractApproverStatusEnum"
      }
    },
    listOption: {//返回数据的路径（dataPath）、下拉列表取值（value）和展示（label）的字段、
      dataPath: 'data',
      label: 'desc',
      value: 'value'
    }
  },
  '客户合同类型': {
    selectOption: { //select 额外配置
      isSelect: true,//是否选择框 //!选择器A（基础用法/单选）选择器B（星标/单选）选择器C（多选）选择器D（可模糊搜索/多选）选择器E（可模糊搜索/单选) 选择器F（图标组合样式）
      // isCascader:true,//是否为级联 //!级联选择器A（四级联选择） 级联选择器B（含模糊搜索选择）
      customSelectType: 'SB',//!S开头代表下拉选择，C开头代表级联：例如SA->选择器A（基础用法/单选）
      multiple: true// 多选 multiple
    },
    ajaxOption: {
      url: `${memberBase}/basicdata-code/list`,//请求地址
      method: 'get',//请求方式
      staticParams: { //固定参数
        codeTypeCode: 'customercontracttype'
      }
    },
    listOption: {//返回数据的路径（dataPath）、下拉列表取值（value）和展示（label）的字段、
      dataPath: 'data',
      label: 'codeName',
      value: 'codesValues'
    }
  },
  '签约方式': {
    selectOption: { //select 额外配置
      isSelect: true,//是否选择框 //!选择器A（基础用法/单选）选择器B（星标/单选）选择器C（多选）选择器D（可模糊搜索/多选）选择器E（可模糊搜索/单选) 选择器F（图标组合样式）
      // isCascader:true,//是否为级联 //!级联选择器A（四级联选择） 级联选择器B（含模糊搜索选择）
      customSelectType: 'SB',//!S开头代表下拉选择，C开头代表级联：例如SA->选择器A（基础用法/单选）
      multiple: true// 多选 multiple
    },
    ajaxOption: {
      url: `${crmBase}/common/selectListEnum`,//请求地址
      method: 'get',//请求方式
      staticParams: { //固定参数
        enumName: 'SignUpNatureEnum'
      }
    },
    listOption: {//返回数据的路径（dataPath）、下拉列表取值（value）和展示（label）的字段、
      dataPath: 'data',
      label: 'desc',
      value: 'value'
    }
  },
  '有效状态': {
    selectOption: { //select 额外配置
      isSelect: true,//是否选择框 //!选择器A（基础用法/单选）选择器B（星标/单选）选择器C（多选）选择器D（可模糊搜索/多选）选择器E（可模糊搜索/单选) 选择器F（图标组合样式）
      // isCascader:true,//是否为级联 //!级联选择器A（四级联选择） 级联选择器B（含模糊搜索选择）
      customSelectType: 'SB',//!S开头代表下拉选择，C开头代表级联：例如SA->选择器A（基础用法/单选）
      multiple: true// 多选 multiple
    },
    ajaxOption: {
      url: `${crmBase}/common/selectListEnum`,//请求地址
      method: 'get',//请求方式
      staticParams: { //固定参数
        enumName: 'EffectiveStatusEnum'
      }
    },
    listOption: {//返回数据的路径（dataPath）、下拉列表取值（value）和展示（label）的字段、
      dataPath: 'data',
      label: 'desc',
      value: 'value'
    }
  },
  '报价服务项目': {
    selectOption: { //select 额外配置
      isSelect: true,//是否选择框 //!选择器A（基础用法/单选）选择器B（星标/单选）选择器C（多选）选择器D（可模糊搜索/多选）选择器E（可模糊搜索/单选) 选择器F（图标组合样式）
      // isCascader:true,//是否为级联 //!级联选择器A（四级联选择） 级联选择器B（含模糊搜索选择）
      customSelectType: 'SB',//!S开头代表下拉选择，C开头代表级联：例如SA->选择器A（基础用法/单选）
      multiple: true// 多选 multiple
    },
    ajaxOption: {
      url: `${memberBase}/basicdata-code/list`,//请求地址
      method: 'get',//请求方式
      staticParams: { //固定参数
        codeTypeCode: 'priceservicetype'
      }
    },
    listOption: {//返回数据的路径（dataPath）、下拉列表取值（value）和展示（label）的字段、
      dataPath: 'data',
      label: 'codeName',
      value: 'codesValues'
    }
  },
  '供应商合同审批状态': {//!!!
    selectOption: { //select 额外配置
      isSelect: true,//是否选择框 //!选择器A（基础用法/单选）选择器B（星标/单选）选择器C（多选）选择器D（可模糊搜索/多选）选择器E（可模糊搜索/单选) 选择器F（图标组合样式）
      // isCascader:true,//是否为级联 //!级联选择器A（四级联选择） 级联选择器B（含模糊搜索选择）
      customSelectType: 'SB',//!S开头代表下拉选择，C开头代表级联：例如SA->选择器A（基础用法/单选）
      multiple: true// 多选 multiple
    },
    ajaxOption: {
      url: `${scmBase}`,//请求地址
      method: 'get',//请求方式
      staticParams: { //固定参数
        type: ''
      }
    },
    listOption: {//返回数据的路径（dataPath）、下拉列表取值（value）和展示（label）的字段、
      dataPath: 'data',
      label: 'name',
      value: 'code'
    }
  },
  '供应商合同类型': {
    selectOption: { //select 额外配置
      isSelect: true,//是否选择框 //!选择器A（基础用法/单选）选择器B（星标/单选）选择器C（多选）选择器D（可模糊搜索/多选）选择器E（可模糊搜索/单选) 选择器F（图标组合样式）
      // isCascader:true,//是否为级联 //!级联选择器A（四级联选择） 级联选择器B（含模糊搜索选择）
      customSelectType: 'SB',//!S开头代表下拉选择，C开头代表级联：例如SA->选择器A（基础用法/单选）
      multiple: true// 多选 multiple
    },
    ajaxOption: {
      url: `${memberBase}/basicdata-code/list`,//请求地址
      method: 'get',//请求方式
      staticParams: { //固定参数
        type: ''
      }
    },
    listOption: {//返回数据的路径（dataPath）、下拉列表取值（value）和展示（label）的字段、
      dataPath: 'data',
      label: 'codeName',
      value: 'codesValues'
    }
  },
  '结算单状态': {
    selectOption: { //select 额外配置
      isSelect: true,//是否选择框 //!选择器A（基础用法/单选）选择器B（星标/单选）选择器C（多选）选择器D（可模糊搜索/多选）选择器E（可模糊搜索/单选) 选择器F（图标组合样式）
      // isCascader:true,//是否为级联 //!级联选择器A（四级联选择） 级联选择器B（含模糊搜索选择）
      customSelectType: 'SB',//!S开头代表下拉选择，C开头代表级联：例如SA->选择器A（基础用法/单选）
      multiple: true// 多选 multiple
    },
    ajaxOption: {
      url: `${settleBase}/api/datadictionary/dropdownbox/values`,//请求地址
      method: 'get',//请求方式
      staticParams: { //固定参数
        type: 'STATEMENT_STATUS'
      }
    },
    listOption: {//返回数据的路径（dataPath）、下拉列表取值（value）和展示（label）的字段、
      dataPath: 'data',
      label: 'name',
      value: 'value'
    }
  },
  '客户对账单状态': {
    selectOption: { //select 额外配置
      isSelect: true,//是否选择框 //!选择器A（基础用法/单选）选择器B（星标/单选）选择器C（多选）选择器D（可模糊搜索/多选）选择器E（可模糊搜索/单选) 选择器F（图标组合样式）
      // isCascader:true,//是否为级联 //!级联选择器A（四级联选择） 级联选择器B（含模糊搜索选择）
      customSelectType: 'SB',//!S开头代表下拉选择，C开头代表级联：例如SA->选择器A（基础用法/单选）
      multiple: true// 多选 multiple
    },
    ajaxOption: {
      url: `${settleBase}/api/datadictionary/dropdownbox/values`,//请求地址
      method: 'get',//请求方式
      staticParams: { //固定参数
        type: 'RECEIVABLE_STATUS'
      }
    },
    listOption: {//返回数据的路径（dataPath）、下拉列表取值（value）和展示（label）的字段、
      dataPath: 'data',
      label: 'name',
      value: 'value'
    }
  },
  '开票状态': {
    selectOption: { //select 额外配置
      isSelect: true,//是否选择框 //!选择器A（基础用法/单选）选择器B（星标/单选）选择器C（多选）选择器D（可模糊搜索/多选）选择器E（可模糊搜索/单选) 选择器F（图标组合样式）
      // isCascader:true,//是否为级联 //!级联选择器A（四级联选择） 级联选择器B（含模糊搜索选择）
      customSelectType: 'SB',//!S开头代表下拉选择，C开头代表级联：例如SA->选择器A（基础用法/单选）
      multiple: true// 多选 multiple
    },
    ajaxOption: {
      url: `${settleBase}/api/datadictionary/dropdownbox/values`,//请求地址
      method: 'get',//请求方式
      staticParams: { //固定参数
        type: 'RECEIVABLE_INVOICED_STATUS'
      }
    },
    listOption: {//返回数据的路径（dataPath）、下拉列表取值（value）和展示（label）的字段、
      dataPath: 'data',
      label: 'name',
      value: 'value'
    }
  },
  '收款状态': {
    selectOption: { //select 额外配置
      isSelect: true,//是否选择框 //!选择器A（基础用法/单选）选择器B（星标/单选）选择器C（多选）选择器D（可模糊搜索/多选）选择器E（可模糊搜索/单选) 选择器F（图标组合样式）
      // isCascader:true,//是否为级联 //!级联选择器A（四级联选择） 级联选择器B（含模糊搜索选择）
      customSelectType: 'SB',//!S开头代表下拉选择，C开头代表级联：例如SA->选择器A（基础用法/单选）
      multiple: true// 多选 multiple
    },
    ajaxOption: {
      url: `${settleBase}/api/datadictionary/dropdownbox/values`,//请求地址
      method: 'get',//请求方式
      staticParams: { //固定参数
        type: 'RECEIVABLE_RECEIPT_STATUS'
      }
    },
    listOption: {//返回数据的路径（dataPath）、下拉列表取值（value）和展示（label）的字段、
      dataPath: 'data',
      label: 'name',
      value: 'value'
    }
  },
  '供应商对账单状态': {
    selectOption: { //select 额外配置
      isSelect: true,//是否选择框 //!选择器A（基础用法/单选）选择器B（星标/单选）选择器C（多选）选择器D（可模糊搜索/多选）选择器E（可模糊搜索/单选) 选择器F（图标组合样式）
      // isCascader:true,//是否为级联 //!级联选择器A（四级联选择） 级联选择器B（含模糊搜索选择）
      customSelectType: 'SB',//!S开头代表下拉选择，C开头代表级联：例如SA->选择器A（基础用法/单选）
      multiple: true// 多选 multiple
    },
    ajaxOption: {
      url: `${scmBase}/api/datadictionary/dropdownbox/values`,//请求地址
      method: 'get',//请求方式
      staticParams: { //固定参数
        type: 'RECEIVABLE_STATUS'
      }
    },
    listOption: {//返回数据的路径（dataPath）、下拉列表取值（value）和展示（label）的字段、
      dataPath: 'data',
      label: 'name',
      value: 'value'
    }
  },
  '应收发票状态': {
    selectOption: { //select 额外配置
      isSelect: true,//是否选择框 //!选择器A（基础用法/单选）选择器B（星标/单选）选择器C（多选）选择器D（可模糊搜索/多选）选择器E（可模糊搜索/单选) 选择器F（图标组合样式）
      // isCascader:true,//是否为级联 //!级联选择器A（四级联选择） 级联选择器B（含模糊搜索选择）
      customSelectType: 'SB',//!S开头代表下拉选择，C开头代表级联：例如SA->选择器A（基础用法/单选）
      multiple: true// 多选 multiple
    },
    ajaxOption: {
      url: `${settleBase}/api/datadictionary/dropdownbox/values`,//请求地址
      method: 'get',//请求方式
      staticParams: { //固定参数
        type: 'INVOICE_STATUS'
      }
    },
    listOption: {//返回数据的路径（dataPath）、下拉列表取值（value）和展示（label）的字段、
      dataPath: 'data',
      label: 'name',
      value: 'value'
    }
  },
  '发票类型': {//!!!
    selectOption: { //select 额外配置
      isSelect: true,//是否选择框 //!选择器A（基础用法/单选）选择器B（星标/单选）选择器C（多选）选择器D（可模糊搜索/多选）选择器E（可模糊搜索/单选) 选择器F（图标组合样式）
      // isCascader:true,//是否为级联 //!级联选择器A（四级联选择） 级联选择器B（含模糊搜索选择）
      customSelectType: 'SB',//!S开头代表下拉选择，C开头代表级联：例如SA->选择器A（基础用法/单选）
      multiple: true// 多选 multiple
    },
    ajaxOption: {
      url: `${settleBase}/api/datadictionary/dropdownbox/values`,//请求地址
      method: 'get',//请求方式
      staticParams: { //固定参数
        type: ''
      }
    },
    listOption: {//返回数据的路径（dataPath）、下拉列表取值（value）和展示（label）的字段、
      dataPath: 'data',
      label: 'name',
      value: 'value'
    }
  },
  '税率': {//!!!
    selectOption: { //select 额外配置
      isSelect: true,//是否选择框 //!选择器A（基础用法/单选）选择器B（星标/单选）选择器C（多选）选择器D（可模糊搜索/多选）选择器E（可模糊搜索/单选) 选择器F（图标组合样式）
      // isCascader:true,//是否为级联 //!级联选择器A（四级联选择） 级联选择器B（含模糊搜索选择）
      customSelectType: 'SB',//!S开头代表下拉选择，C开头代表级联：例如SA->选择器A（基础用法/单选）
      multiple: true// 多选 multiple
    },
    ajaxOption: {
      url: `${settleBase}/api/basicdata/enterprise-invoice`,//请求地址
      method: 'get',//请求方式
      staticParams: { //固定参数
        type: ''
      }
    },
    listOption: {//返回数据的路径（dataPath）、下拉列表取值（value）和展示（label）的字段、
      dataPath: 'data',
      label: 'name',
      value: 'code'
    }
  },
  '发票池状态': {
    selectOption: { //select 额外配置
      isSelect: true,//是否选择框 //!选择器A（基础用法/单选）选择器B（星标/单选）选择器C（多选）选择器D（可模糊搜索/多选）选择器E（可模糊搜索/单选) 选择器F（图标组合样式）
      // isCascader:true,//是否为级联 //!级联选择器A（四级联选择） 级联选择器B（含模糊搜索选择）
      customSelectType: 'SB',//!S开头代表下拉选择，C开头代表级联：例如SA->选择器A（基础用法/单选）
      multiple: true// 多选 multiple
    },
    ajaxOption: {
      url: `${settleBase}/api/datadictionary/dropdownbox/values`,//请求地址
      method: 'get',//请求方式
      staticParams: { //固定参数
        type: 'INVOICE_POOL_STATUS'
      }
    },
    listOption: {//返回数据的路径（dataPath）、下拉列表取值（value）和展示（label）的字段、
      dataPath: 'data',
      label: 'name',
      value: 'value'
    }
  },
  '收款状态': {
    selectOption: { //select 额外配置
      isSelect: true,//是否选择框 //!选择器A（基础用法/单选）选择器B（星标/单选）选择器C（多选）选择器D（可模糊搜索/多选）选择器E（可模糊搜索/单选) 选择器F（图标组合样式）
      // isCascader:true,//是否为级联 //!级联选择器A（四级联选择） 级联选择器B（含模糊搜索选择）
      customSelectType: 'SB',//!S开头代表下拉选择，C开头代表级联：例如SA->选择器A（基础用法/单选）
      multiple: true// 多选 multiple
    },
    ajaxOption: {
      url: `${settleBase}/api/datadictionary/dropdownbox/values`,//请求地址
      method: 'get',//请求方式
      staticParams: { //固定参数
        type: 'RECEIPT_STATUS'
      }
    },
    listOption: {//返回数据的路径（dataPath）、下拉列表取值（value）和展示（label）的字段、
      dataPath: 'data',
      label: 'name',
      value: 'value'
    }
  },
  '收付款方式': {
    selectOption: { //select 额外配置
      isSelect: true,//是否选择框 //!选择器A（基础用法/单选）选择器B（星标/单选）选择器C（多选）选择器D（可模糊搜索/多选）选择器E（可模糊搜索/单选) 选择器F（图标组合样式）
      // isCascader:true,//是否为级联 //!级联选择器A（四级联选择） 级联选择器B（含模糊搜索选择）
      customSelectType: 'SB',//!S开头代表下拉选择，C开头代表级联：例如SA->选择器A（基础用法/单选）
      multiple: true// 多选 multiple
    },
    ajaxOption: {
      url: `${settleBase}/api/datadictionary/dropdownbox/values`,//请求地址
      method: 'get',//请求方式
      staticParams: { //固定参数
        type: 'PAYMENT_METHOD'
      }
    },
    listOption: {//返回数据的路径（dataPath）、下拉列表取值（value）和展示（label）的字段、
      dataPath: 'data',
      label: 'name',
      value: 'value'
    }
  },
  '退款状态': {
    selectOption: { //select 额外配置
      isSelect: true,//是否选择框 //!选择器A（基础用法/单选）选择器B（星标/单选）选择器C（多选）选择器D（可模糊搜索/多选）选择器E（可模糊搜索/单选) 选择器F（图标组合样式）
      // isCascader:true,//是否为级联 //!级联选择器A（四级联选择） 级联选择器B（含模糊搜索选择）
      customSelectType: 'SB',//!S开头代表下拉选择，C开头代表级联：例如SA->选择器A（基础用法/单选）
      multiple: true// 多选 multiple
    },
    ajaxOption: {
      url: `${settleBase}/api/datadictionary/dropdownbox/values`,//请求地址
      method: 'get',//请求方式
      staticParams: { //固定参数
        type: 'REFUND_STATUS'
      }
    },
    listOption: {//返回数据的路径（dataPath）、下拉列表取值（value）和展示（label）的字段、
      dataPath: 'data',
      label: 'name',
      value: 'value'
    }
  },
  '借款状态': {
    selectOption: { //select 额外配置
      isSelect: true,//是否选择框 //!选择器A（基础用法/单选）选择器B（星标/单选）选择器C（多选）选择器D（可模糊搜索/多选）选择器E（可模糊搜索/单选) 选择器F（图标组合样式）
      // isCascader:true,//是否为级联 //!级联选择器A（四级联选择） 级联选择器B（含模糊搜索选择）
      customSelectType: 'SB',//!S开头代表下拉选择，C开头代表级联：例如SA->选择器A（基础用法/单选）
      multiple: true// 多选 multiple
    },
    ajaxOption: {
      url: `${settleBase}/api/datadictionary/dropdownbox/values`,//请求地址
      method: 'get',//请求方式
      staticParams: { //固定参数
        type: 'LOAN_STATUS'
      }
    },
    listOption: {//返回数据的路径（dataPath）、下拉列表取值（value）和展示（label）的字段、
      dataPath: 'data',
      label: 'name',
      value: 'value'
    }
  },
  '借款用途': {
    selectOption: { //select 额外配置
      isSelect: true,//是否选择框 //!选择器A（基础用法/单选）选择器B（星标/单选）选择器C（多选）选择器D（可模糊搜索/多选）选择器E（可模糊搜索/单选) 选择器F（图标组合样式）
      // isCascader:true,//是否为级联 //!级联选择器A（四级联选择） 级联选择器B（含模糊搜索选择）
      customSelectType: 'SB',//!S开头代表下拉选择，C开头代表级联：例如SA->选择器A（基础用法/单选）
      multiple: true// 多选 multiple
    },
    ajaxOption: {
      url: `${settleBase}/api/datadictionary/dropdownbox/values`,//请求地址
      method: 'get',//请求方式
      staticParams: { //固定参数
        type: 'LOAN_USE'
      }
    },
    listOption: {//返回数据的路径（dataPath）、下拉列表取值（value）和展示（label）的字段、
      dataPath: 'data',
      label: 'name',
      value: 'value'
    }
  },
  '借款类型': {
    selectOption: { //select 额外配置
      isSelect: true,//是否选择框 //!选择器A（基础用法/单选）选择器B（星标/单选）选择器C（多选）选择器D（可模糊搜索/多选）选择器E（可模糊搜索/单选) 选择器F（图标组合样式）
      // isCascader:true,//是否为级联 //!级联选择器A（四级联选择） 级联选择器B（含模糊搜索选择）
      customSelectType: 'SB',//!S开头代表下拉选择，C开头代表级联：例如SA->选择器A（基础用法/单选）
      multiple: true// 多选 multiple
    },
    ajaxOption: {
      url: `${settleBase}/api/datadictionary/dropdownbox/values`,//请求地址
      method: 'get',//请求方式
      staticParams: { //固定参数
        type: 'LOAN_TYPE'
      }
    },
    listOption: {//返回数据的路径（dataPath）、下拉列表取值（value）和展示（label）的字段、
      dataPath: 'data',
      label: 'name',
      value: 'value'
    }
  },
  '报销状态': {
    selectOption: { //select 额外配置
      isSelect: true,//是否选择框 //!选择器A（基础用法/单选）选择器B（星标/单选）选择器C（多选）选择器D（可模糊搜索/多选）选择器E（可模糊搜索/单选) 选择器F（图标组合样式）
      // isCascader:true,//是否为级联 //!级联选择器A（四级联选择） 级联选择器B（含模糊搜索选择）
      customSelectType: 'SB',//!S开头代表下拉选择，C开头代表级联：例如SA->选择器A（基础用法/单选）
      multiple: true// 多选 multiple
    },
    ajaxOption: {
      url: `${settleBase}/api/datadictionary/dropdownbox/values`,//请求地址
      method: 'get',//请求方式
      staticParams: { //固定参数
        type: 'REIMBURSEMENT_STATUS'
      }
    },
    listOption: {//返回数据的路径（dataPath）、下拉列表取值（value）和展示（label）的字段、
      dataPath: 'data',
      label: 'name',
      value: 'value'
    }
  },
  '报销类型': {
    selectOption: { //select 额外配置
      isSelect: true,//是否选择框 //!选择器A（基础用法/单选）选择器B（星标/单选）选择器C（多选）选择器D（可模糊搜索/多选）选择器E（可模糊搜索/单选) 选择器F（图标组合样式）
      // isCascader:true,//是否为级联 //!级联选择器A（四级联选择） 级联选择器B（含模糊搜索选择）
      customSelectType: 'SB',//!S开头代表下拉选择，C开头代表级联：例如SA->选择器A（基础用法/单选）
      multiple: true// 多选 multiple
    },
    ajaxOption: {
      url: `${settleBase}/api/datadictionary/dropdownbox/values`,//请求地址
      method: 'get',//请求方式
      staticParams: { //固定参数
        type: 'REIMBURSEMENT_TYPE'
      }
    },
    listOption: {//返回数据的路径（dataPath）、下拉列表取值（value）和展示（label）的字段、
      dataPath: 'data',
      label: 'name',
      value: 'value'
    }
  },
  '请款状态': {
    selectOption: { //select 额外配置
      isSelect: true,//是否选择框 //!选择器A（基础用法/单选）选择器B（星标/单选）选择器C（多选）选择器D（可模糊搜索/多选）选择器E（可模糊搜索/单选) 选择器F（图标组合样式）
      // isCascader:true,//是否为级联 //!级联选择器A（四级联选择） 级联选择器B（含模糊搜索选择）
      customSelectType: 'SB',//!S开头代表下拉选择，C开头代表级联：例如SA->选择器A（基础用法/单选）
      multiple: true// 多选 multiple
    },
    ajaxOption: {
      url: `${settleBase}/api/datadictionary/dropdownbox/values`,//请求地址
      method: 'get',//请求方式
      staticParams: { //固定参数
        type: 'PAYMENT_REQUEST_STATUS'
      }
    },
    listOption: {//返回数据的路径（dataPath）、下拉列表取值（value）和展示（label）的字段、
      dataPath: 'data',
      label: 'name',
      value: 'value'
    }
  },
  '请款类型': {
    selectOption: { //select 额外配置
      isSelect: true,//是否选择框 //!选择器A（基础用法/单选）选择器B（星标/单选）选择器C（多选）选择器D（可模糊搜索/多选）选择器E（可模糊搜索/单选) 选择器F（图标组合样式）
      // isCascader:true,//是否为级联 //!级联选择器A（四级联选择） 级联选择器B（含模糊搜索选择）
      customSelectType: 'SB',//!S开头代表下拉选择，C开头代表级联：例如SA->选择器A（基础用法/单选）
      multiple: true// 多选 multiple
    },
    ajaxOption: {
      url: `${settleBase}/api/datadictionary/dropdownbox/values`,//请求地址
      method: 'get',//请求方式
      staticParams: { //固定参数
        type: 'PAYMENTREQUEST_TYPE'
      }
    },
    listOption: {//返回数据的路径（dataPath）、下拉列表取值（value）和展示（label）的字段、
      dataPath: 'data',
      label: 'name',
      value: 'value'
    }
  },
  '调账状态': {
    selectOption: { //select 额外配置
      isSelect: true,//是否选择框 //!选择器A（基础用法/单选）选择器B（星标/单选）选择器C（多选）选择器D（可模糊搜索/多选）选择器E（可模糊搜索/单选) 选择器F（图标组合样式）
      // isCascader:true,//是否为级联 //!级联选择器A（四级联选择） 级联选择器B（含模糊搜索选择）
      customSelectType: 'SB',//!S开头代表下拉选择，C开头代表级联：例如SA->选择器A（基础用法/单选）
      multiple: true// 多选 multiple
    },
    ajaxOption: {
      url: `${settleBase}/api/datadictionary/dropdownbox/values`,//请求地址
      method: 'get',//请求方式
      staticParams: { //固定参数
        type: 'ADJUSTMENT_STATUS'
      }
    },
    listOption: {//返回数据的路径（dataPath）、下拉列表取值（value）和展示（label）的字段、
      dataPath: 'data',
      label: 'name',
      value: 'value'
    }
  },
  '费用状态': {
    selectOption: { //select 额外配置
      isSelect: true,//是否选择框 //!选择器A（基础用法/单选）选择器B（星标/单选）选择器C（多选）选择器D（可模糊搜索/多选）选择器E（可模糊搜索/单选) 选择器F（图标组合样式）
      // isCascader:true,//是否为级联 //!级联选择器A（四级联选择） 级联选择器B（含模糊搜索选择）
      customSelectType: 'SB',//!S开头代表下拉选择，C开头代表级联：例如SA->选择器A（基础用法/单选）
      multiple: true// 多选 multiple
    },
    ajaxOption: {
      url: `${settleBase}/api/datadictionary/dropdownbox/values`,//请求地址
      method: 'get',//请求方式
      staticParams: { //固定参数
        type: 'FEE_STATUS'
      }
    },
    listOption: {//返回数据的路径（dataPath）、下拉列表取值（value）和展示（label）的字段、
      dataPath: 'data',
      label: 'name',
      value: 'value'
    }
  },
  '标准产品状态': { //
    selectOption: { //select 额外配置
      isSelect: true,//是否选择框 //!选择器A（基础用法/单选）选择器B（星标/单选）选择器C（多选）选择器D（可模糊搜索/多选）选择器E（可模糊搜索/单选) 选择器F（图标组合样式）
      // isCascader:true,//是否为级联 //!级联选择器A（四级联选择） 级联选择器B（含模糊搜索选择）
      customSelectType: 'SB',//!S开头代表下拉选择，C开头代表级联：例如SA->选择器A（基础用法/单选）
      multiple: true// 多选 multiple
    },
    ajaxOption: {
      url: `${crmBase}/common/selectListEnum`,//请求地址
      method: 'get',//请求方式
      staticParams: { //固定参数 [{"name":"StatusEnum"}]
        enumName:"StatusEnum"
      }
    },
    listOption: {//返回数据的路径（dataPath）、下拉列表取值（value）和展示（label）的字段、
      dataPath: 'data',
      label: 'desc',
      value: 'value'
    }
  },
  '启用禁用状态': {//多选 
    selectOption: { //select 额外配置
      isSelect: true,//是否选择框 //!选择器A（基础用法/单选）选择器B（星标/单选）选择器C（多选）选择器D（可模糊搜索/多选）选择器E（可模糊搜索/单选) 选择器F（图标组合样式）
      // isCascader:true,//是否为级联 //!级联选择器A（四级联选择） 级联选择器B（含模糊搜索选择）
      customSelectType: 'SC',//!S开头代表下拉选择，C开头代表级联：例如SA->选择器A（基础用法/单选）
      multiple: true// 多选 multiple
    },
    ajaxOption: {
      url: `${settleBase}/api/datadictionary/dropdownbox/values`,//请求地址
      method: 'get',//请求方式
      staticParams: { //固定参数
        type: 'ENABLE_STATUS'
      }
    },
    listOption: {//返回数据的路径（dataPath）、下拉列表取值（value）和展示（label）的字段、
      dataPath: 'data',
      label: 'name',
      value: 'value'
    }
  },
  '销账类型': {//多选 
    selectOption: { //select 额外配置
      isSelect: true,//是否选择框 //!选择器A（基础用法/单选）选择器B（星标/单选）选择器C（多选）选择器D（可模糊搜索/多选）选择器E（可模糊搜索/单选) 选择器F（图标组合样式）
      // isCascader:true,//是否为级联 //!级联选择器A（四级联选择） 级联选择器B（含模糊搜索选择）
      customSelectType: 'SC',//!S开头代表下拉选择，C开头代表级联：例如SA->选择器A（基础用法/单选）
      multiple: true// 多选 multiple
    },
    ajaxOption: {
      url: `${settleBase}/api/datadictionary/dropdownbox/values`,//请求地址
      method: 'get',//请求方式
      staticParams: { //固定参数
        type: 'RECEIPT_CLAIM_WRITE_OFF_TYPE'
      }
    },
    listOption: {//返回数据的路径（dataPath）、下拉列表取值（value）和展示（label）的字段、
      dataPath: 'data',
      label: 'name',
      value: 'value'
    }
  },
  //#endregion
'所属公司': {//选择器C（可模糊搜索/多选）
  selectOption: { //select 额外配置
    isSelect: true,//是否选择框 //!选择器A（基础用法/单选）选择器B（星标/单选）选择器C（多选）选择器D（可模糊搜索/多选）选择器E（可模糊搜索/单选) 选择器F（图标组合样式）
    isCascader:true,//是否为级联 //!级联选择器A（四级联选择） 级联选择器B（含模糊搜索选择）
    customSelectType: 'C',//!S开头代表下拉选择，C开头代表级联：例如SA->选择器A（基础用法/单选）
    allowSearch: true,//可模糊
    multiple: false
  },
  // /basicdata/basicdata-code/list
  ajaxOption: {
    // url: `${basicdataBase}/basicdata-code/list`,//请求地址
    url: `${scmBase}/relation-view/queryRelationByDropDown`,//请求地址
    // url: `${scmBase}/logistics-mos-view/selector1`,//请求地址
    method: 'get',//请求方式
    staticParams: { //固定参数
      relationTypes: '4',
      name:''
    }
  },
  listOption: {//返回数据的路径（dataPath）、下拉列表取值（value）和展示（label）的字段、
    dataPath: 'data',
    label: 'name',
    value: 'enterpriseId'
  }
},
  //#region //! 选择器C（可模糊搜索/多选）
  '委托人': {//选择器C（可模糊搜索/多选）
    selectOption: { //select 额外配置
      isSelect: true,//是否选择框 //!选择器A（基础用法/单选）选择器B（星标/单选）选择器C（多选）选择器D（可模糊搜索/多选）选择器E（可模糊搜索/单选) 选择器F（图标组合样式）
      // isCascader:true,//是否为级联 //!级联选择器A（四级联选择） 级联选择器B（含模糊搜索选择）
      customSelectType: 'SC',//!S开头代表下拉选择，C开头代表级联：例如SA->选择器A（基础用法/单选）
      allowSearch: true,//可模糊
      multiple: true,
    },
    // /basicdata/basicdata-code/list
    ajaxOption: {
      url: `${scmBase}/relation-view/queryRelationByDropDown`,//请求地址
      method: 'get',//请求方式
      staticParams: { //固定参数
        relationType: 8,
      }
    },
    listOption: {//返回数据的路径（dataPath）、下拉列表取值（value）和展示（label）的字段、
      dataPath: 'data',
      label: 'name',
      value: 'enterpriseId'
    }
  },
  

  //#region //! 选择器C（可模糊搜索/多选）
  '营销部门': {//选择器C（可模糊搜索/多选）
    selectOption: { //select 额外配置
      isSelect: true,//是否选择框 //!选择器A（基础用法/单选）选择器B（星标/单选）选择器C（多选）选择器D（可模糊搜索/多选）选择器E（可模糊搜索/单选) 选择器F（图标组合样式）
      // isCascader:true,//是否为级联 //!级联选择器A（四级联选择） 级联选择器B（含模糊搜索选择）
      customSelectType: 'SC',//!S开头代表下拉选择，C开头代表级联：例如SA->选择器A（基础用法/单选）
      allowSearch: true,//可模糊
      multiple: false
    },
    // /basicdata/basicdata-code/list
    ajaxOption: {
      url: `${memberBase}/business-department-config/select`,//请求地址
      method: 'get',//请求方式
      staticParams: { //固定参数
        departmentType: 1,
      }
    },
    listOption: {//返回数据的路径（dataPath）、下拉列表取值（value）和展示（label）的字段、
      dataPath: 'data',
      label: 'name',
      value: 'enterpriseId'
    }
  },
  
  // /settle/api/statement/salespersons
  '经营单位': {//选择器C（可模糊搜索/多选）
    selectOption: { //select 额外配置
      isSelect: true,//是否选择框 //!选择器A（基础用法/单选）选择器B（星标/单选）选择器C（多选）选择器D（可模糊搜索/多选）选择器E（可模糊搜索/单选) 选择器F（图标组合样式）
      // isCascader:true,//是否为级联 //!级联选择器A（四级联选择） 级联选择器B（含模糊搜索选择）
      customSelectType: 'SC',//!S开头代表下拉选择，C开头代表级联：例如SA->选择器A（基础用法/单选）
      allowSearch: true,//可模糊
      multiple: true// 多选 multiple
    },
    ajaxOption: {
      url: `${scmBase}/relation-view/queryRelationByDropDown`,//请求地址
      method: 'get',//请求方式
      staticParams: { //固定参数
        relationTypes: '8',
        // labelCodes: 'BusinessUnit'
      }
    },
    listOption: {//返回数据的路径（dataPath）、下拉列表取值（value）和展示（label）的字段、
      dataPath: 'data',
      label: 'name',
      value: 'enterpriseId'
    }
  },
  '收货单位': {//选择器C（可模糊搜索/多选）
    selectOption: { //select 额外配置
      isSelect: true,//是否选择框 //!选择器A（基础用法/单选）选择器B（星标/单选）选择器C（多选）选择器D（可模糊搜索/多选）选择器E（可模糊搜索/单选) 选择器F（图标组合样式）
      // isCascader:true,//是否为级联 //!级联选择器A（四级联选择） 级联选择器B（含模糊搜索选择）
      customSelectType: 'SC',//!S开头代表下拉选择，C开头代表级联：例如SA->选择器A（基础用法/单选）
      allowSearch: true,//可模糊
      multiple: true// 多选 multiple
    },
    ajaxOption: {
      url: `${scmBase}/relation-view/queryRelationByDropDown`,//请求地址
      method: 'get',//请求方式
      staticParams: { //固定参数
        relationTypes: '8'
      }
    },
    listOption: {//返回数据的路径（dataPath）、下拉列表取值（value）和展示（label）的字段、
      dataPath: 'data',
      label: 'name',
      value: 'enterpriseId'
    }
  },
  '控货方': {//选择器C（可模糊搜索/多选）
    selectOption: { //select 额外配置
      isSelect: true,//是否选择框 //!选择器A（基础用法/单选）选择器B（星标/单选）选择器C（多选）选择器D（可模糊搜索/多选）选择器E（可模糊搜索/单选) 选择器F（图标组合样式）
      // isCascader:true,//是否为级联 //!级联选择器A（四级联选择） 级联选择器B（含模糊搜索选择）
      customSelectType: 'SC',//!S开头代表下拉选择，C开头代表级联：例如SA->选择器A（基础用法/单选）
      allowSearch: true,//可模糊
      filterOption: false, // 远程搜索时设置
      multiple: true// 多选 multiple
    },
    ajaxOption: {
      url: `${scmBase}/relation-view/queryRelationByDropDown`,//请求地址
      method: 'get',//请求方式
      staticParams: { //固定参数
        relationTypes: '8'
      }
    },
    listOption: {//返回数据的路径（dataPath）、下拉列表取值（value）和展示（label）的字段、
      dataPath: 'data',
      label: 'name',
      value: 'enterpriseId'
    }
  },
  '国内买家': {//选择器C（可模糊搜索/多选）
    selectOption: { //select 额外配置
      isSelect: true,//是否选择框 //!选择器A（基础用法/单选）选择器B（星标/单选）选择器C（多选）选择器D（可模糊搜索/多选）选择器E（可模糊搜索/单选) 选择器F（图标组合样式）
      // isCascader:true,//是否为级联 //!级联选择器A（四级联选择） 级联选择器B（含模糊搜索选择）
      customSelectType: 'SC',//!S开头代表下拉选择，C开头代表级联：例如SA->选择器A（基础用法/单选）
      allowSearch: true,//可模糊
      multiple: true// 多选 multiple
    },
    ajaxOption: {
      url: `${scmBase}/relation-view/queryRelationByDropDown`,//请求地址
      method: 'get',//请求方式
      staticParams: { //固定参数
        relationTypes: '8',
        labelCodes: 'DomesticBuyers'
      }
    },
    listOption: {//返回数据的路径（dataPath）、下拉列表取值（value）和展示（label）的字段、
      dataPath: 'data',
      label: 'name',
      value: 'enterpriseId'
    }
  },
  '港口(全部）': {//选择器C（可模糊搜索/多选）
    selectOption: { //select 额外配置
      isSelect: true,//是否选择框 //!选择器A（基础用法/单选）选择器B（星标/单选）选择器C（多选）选择器D（可模糊搜索/多选）选择器E（可模糊搜索/单选) 选择器F（图标组合样式）
      // isCascader:true,//是否为级联 //!级联选择器A（四级联选择） 级联选择器B（含模糊搜索选择）
      customSelectType: 'SC',//!S开头代表下拉选择，C开头代表级联：例如SA->选择器A（基础用法/单选）
      allowSearch: true,//可模糊
      multiple: true// 多选 multiple
    },
    ajaxOption: {
      url: `${basicdataBase}/basicdata-seaport/list`,//请求地址
      method: 'get',//请求方式
      staticParams: { //固定参数

      }
    },
    listOption: {//返回数据的路径（dataPath）、下拉列表取值（value）和展示（label）的字段、
      dataPath: 'data',
      label: 'seaportName',
      value: 'seaportCode'
    }
  },
  '港口': {//选择器C（可模糊搜索/多选） // 港口中国
    selectOption: { //select 额外配置
      isSelect: true,//是否选择框 //!选择器A（基础用法/单选）选择器B（星标/单选）选择器C（多选）选择器D（可模糊搜索/多选）选择器E（可模糊搜索/单选) 选择器F（图标组合样式）
      // isCascader:true,//是否为级联 //!级联选择器A（四级联选择） 级联选择器B（含模糊搜索选择）
      customSelectType: 'SC',//!S开头代表下拉选择，C开头代表级联：例如SA->选择器A（基础用法/单选）
      allowSearch: true,//可模糊
      multiple: true// 多选 multiple
    },
    ajaxOption: {
      url: `${basicdataBase}/basicdata-seaport/list`,//请求地址
      method: 'get',//请求方式
      staticParams: { //固定参数
        countryId: '104'
      }
    },
    listOption: {//返回数据的路径（dataPath）、下拉列表取值（value）和展示（label）的字段、
      dataPath: 'data',
      label: 'seaportName',
      value: 'seaportCode'
    }
  },
  '港口(中国）': {//选择器C（可模糊搜索/多选） // 港口中国
    selectOption: { //select 额外配置
      isSelect: true,//是否选择框 //!选择器A（基础用法/单选）选择器B（星标/单选）选择器C（多选）选择器D（可模糊搜索/多选）选择器E（可模糊搜索/单选) 选择器F（图标组合样式）
      // isCascader:true,//是否为级联 //!级联选择器A（四级联选择） 级联选择器B（含模糊搜索选择）
      customSelectType: 'SC',//!S开头代表下拉选择，C开头代表级联：例如SA->选择器A（基础用法/单选）
      allowSearch: true,//可模糊
      multiple: true// 多选 multiple
    },
    ajaxOption: {
      url: `${basicdataBase}/basicdata-seaport/list`,//请求地址
      method: 'get',//请求方式
      staticParams: { //固定参数
        countryId: '104'
      }
    },
    listOption: {//返回数据的路径（dataPath）、下拉列表取值（value）和展示（label）的字段、
      dataPath: 'data',
      label: 'seaportName',
      value: 'seaportCode'
    }
  },
  '船公司': {//选择器C（可模糊搜索/多选）
    selectOption: { //select 额外配置
      isSelect: true,//是否选择框 //!选择器A（基础用法/单选）选择器B（星标/单选）选择器C（多选）选择器D（可模糊搜索/多选）选择器E（可模糊搜索/单选) 选择器F（图标组合样式）
      // isCascader:true,//是否为级联 //!级联选择器A（四级联选择） 级联选择器B（含模糊搜索选择）
      customSelectType: 'SC',//!S开头代表下拉选择，C开头代表级联：例如SA->选择器A（基础用法/单选）
      allowSearch: true,//可模糊
      multiple: true// 多选 multiple
    },
    ajaxOption: {
      url: `${memberBase}/basic-data-shipping-company/list`,//请求地址
      method: 'get',//请求方式
      staticParams: { //固定参数

      }
    },
    listOption: {//返回数据的路径（dataPath）、下拉列表取值（value）和展示（label）的字段、
      dataPath: 'data',
      label: 'shippingCompanyName',
      value: 'shippingCompanyCode'
    }
  },
  '销售品名': {//选择器C（可模糊搜索/多选）
    selectOption: { //select 额外配置
      isSelect: true,//是否选择框 //!选择器A（基础用法/单选）选择器B（星标/单选）选择器C（多选）选择器D（可模糊搜索/多选）选择器E（可模糊搜索/单选) 选择器F（图标组合样式）
      // isCascader:true,//是否为级联 //!级联选择器A（四级联选择） 级联选择器B（含模糊搜索选择）
      customSelectType: 'SC',//!S开头代表下拉选择，C开头代表级联：例如SA->选择器A（基础用法/单选）
      allowSearch: true,//可模糊
      multiple: true// 多选 multiple
    },
    ajaxOption: {
      url: `${productBase}/saleProduct/select-all-sale-name`,//请求地址
      method: 'get',//请求方式
      staticParams: { //固定参数
      
      }
    },
    listOption: {//返回数据的路径（dataPath）、下拉列表取值（value）和展示（label）的字段、
      dataPath: 'data',
      label: '',
      value: ''
    }
  },
  '': {//选择器C（可模糊搜索/多选）
    selectOption: { //select 额外配置
      isSelect: true,//是否选择框 //!选择器A（基础用法/单选）选择器B（星标/单选）选择器C（多选）选择器D（可模糊搜索/多选）选择器E（可模糊搜索/单选) 选择器F（图标组合样式）
      // isCascader:true,//是否为级联 //!级联选择器A（四级联选择） 级联选择器B（含模糊搜索选择）
      customSelectType: 'SC',//!S开头代表下拉选择，C开头代表级联：例如SA->选择器A（基础用法/单选）
      allowSearch: true,//可模糊
      multiple: true// 多选 multiple
    },
    ajaxOption: {
      url: `${scmBase}`,//请求地址
      method: 'get',//请求方式
      staticParams: { //固定参数
        type: ''
      }
    },
    listOption: {//返回数据的路径（dataPath）、下拉列表取值（value）和展示（label）的字段、
      dataPath: 'data',
      label: '',
      value: ''
    }
  },
  '任务状态': {//选择器C（可模糊搜索/多选）
    selectOption: { //select 额外配置
      isSelect: true,//是否选择框 //!选择器A（基础用法/单选）选择器B（星标/单选）选择器C（多选）选择器D（可模糊搜索/多选）选择器E（可模糊搜索/单选) 选择器F（图标组合样式）
      // isCascader:true,//是否为级联 //!级联选择器A（四级联选择） 级联选择器B（含模糊搜索选择）
      customSelectType: 'SB',//!S开头代表下拉选择，C开头代表级联：例如SA->选择器A（基础用法/单选）
      allowSearch: true,//可模糊
      multiple: false// 多选 multiple
    },
    ajaxOption: {
      url: `${processBase}/common/selector`,//请求地址
      method: 'get',//请求方式
      staticParams: { //固定参数
        selectorCode: 'taskStatus'
      }
    },
    listOption: {//返回数据的路径（dataPath）、下拉列表取值（value）和展示（label）的字段、
      dataPath: 'data',
      label: 'desc',
      value: 'value'
    }
  },
  '异常状态': {//选择器C（可模糊搜索/多选）
    selectOption: { //select 额外配置
      isSelect: true,//是否选择框 //!选择器A（基础用法/单选）选择器B（星标/单选）选择器C（多选）选择器D（可模糊搜索/多选）选择器E（可模糊搜索/单选) 选择器F（图标组合样式）
      // isCascader:true,//是否为级联 //!级联选择器A（四级联选择） 级联选择器B（含模糊搜索选择）
      customSelectType: 'SB',//!S开头代表下拉选择，C开头代表级联：例如SA->选择器A（基础用法/单选）
      allowSearch: true,//可模糊
      multiple: false// 多选 multiple
    },
    ajaxOption: {
      url: `${processBase}/common/selector`,//请求地址
      method: 'get',//请求方式
      staticParams: { //固定参数
        selectorCode: 'riskStatus'
      }
    },
    listOption: {//返回数据的路径（dataPath）、下拉列表取值（value）和展示（label）的字段、
      dataPath: 'data',
      label: 'desc',
      value: 'value'
    }
  },
  '异常等级': {//选择器C（可模糊搜索/多选）
    selectOption: { //select 额外配置
      isSelect: true,//是否选择框 //!选择器A（基础用法/单选）选择器B（星标/单选）选择器C（多选）选择器D（可模糊搜索/多选）选择器E（可模糊搜索/单选) 选择器F（图标组合样式）
      // isCascader:true,//是否为级联 //!级联选择器A（四级联选择） 级联选择器B（含模糊搜索选择）
      customSelectType: 'SB',//!S开头代表下拉选择，C开头代表级联：例如SA->选择器A（基础用法/单选）
      allowSearch: true, //可模糊
      multiple: false // 多选 multiple
    },
    ajaxOption: {
      url: `${processBase}/common/selector`,//请求地址
      method: 'get',//请求方式
      staticParams: { //固定参数
        selectorCode: 'riskLevel'
      }
    },
    listOption: {//返回数据的路径（dataPath）、下拉列表取值（value）和展示（label）的字段、
      dataPath: 'data',
      label: 'desc',
      value: 'value'
    }
  },
  '商品名': {//选择器C（可模糊搜索/多选）
    selectOption: { //select 额外配置
      isSelect: true,//是否选择框 //!选择器A（基础用法/单选）选择器B（星标/单选）选择器C（多选）选择器D（可模糊搜索/多选）选择器E（可模糊搜索/单选) 选择器F（图标组合样式）
      // isCascader:true,//是否为级联 //!级联选择器A（四级联选择） 级联选择器B（含模糊搜索选择）
      customSelectType: 'SC',//!S开头代表下拉选择，C开头代表级联：例如SA->选择器A（基础用法/单选）
      allowSearch: true,//可模糊
      multiple: true// 多选 multiple
    },
    ajaxOption: {
      url: `${productBase}/saleProduct/select-all-product-name`,//请求地址
      method: 'get',//请求方式
      staticParams: { //固定参数
      }
    },
    listOption: {//返回数据的路径（dataPath）、下拉列表取值（value）和展示（label）的字段、
      dataPath: 'data',
      label: '',
      value: ''
    }
  },
  '经营品名': {//选择器C（可模糊搜索/多选）
    selectOption: { //select 额外配置
      isSelect: true,//是否选择框 //!选择器A（基础用法/单选）选择器B（星标/单选）选择器C（多选）选择器D（可模糊搜索/多选）选择器E（可模糊搜索/单选) 选择器F（图标组合样式）
      // isCascader:true,//是否为级联 //!级联选择器A（四级联选择） 级联选择器B（含模糊搜索选择）
      customSelectType: 'SC',//!S开头代表下拉选择，C开头代表级联：例如SA->选择器A（基础用法/单选）
      allowSearch: true,//可模糊
      multiple: true// 多选 multiple
    },
    ajaxOption: {
      url: `${productBase}/saleProduct/select-category-name`,//请求地址
      method: 'get',//请求方式
      staticParams: { //固定参数
      }
    },
    listOption: {//返回数据的路径（dataPath）、下拉列表取值（value）和展示（label）的字段、
      dataPath: 'data',
      label: '',
      value: ''
    }
  },
  '国家': {//选择器C（可模糊搜索/多选）
    selectOption: { //select 额外配置
      isSelect: true,//是否选择框 //!选择器A（基础用法/单选）选择器B（星标/单选）选择器C（多选）选择器D（可模糊搜索/多选）选择器E（可模糊搜索/单选) 选择器F（图标组合样式）
      // isCascader:true,//是否为级联 //!级联选择器A（四级联选择） 级联选择器B（含模糊搜索选择）
      customSelectType: 'SC',//!S开头代表下拉选择，C开头代表级联：例如SA->选择器A（基础用法/单选）
      allowSearch: true,//可模糊
      multiple: true// 多选 multiple
    },
    ajaxOption: {
      url: `${memberBase}/country/list`,//请求地址
      method: 'get',//请求方式
      staticParams: { //固定参数
        type: ''
      }
    },
    listOption: {//返回数据的路径（dataPath）、下拉列表取值（value）和展示（label）的字段、
      dataPath: 'data',
      label: 'countryName',
      value: 'countryCode'
    }
  },
  '箱型箱类': {//选择器C（可模糊搜索/多选）
    selectOption: { //select 额外配置
      isSelect: true,//是否选择框 //!选择器A（基础用法/单选）选择器B（星标/单选）选择器C（多选）选择器D（可模糊搜索/多选）选择器E（可模糊搜索/单选) 选择器F（图标组合样式）
      // isCascader:true,//是否为级联 //!级联选择器A（四级联选择） 级联选择器B（含模糊搜索选择）
      customSelectType: 'SC',//!S开头代表下拉选择，C开头代表级联：例如SA->选择器A（基础用法/单选）
      allowSearch: true,//可模糊
      multiple: true// 多选 multiple
    },
    ajaxOption: {
      url: `${scmBase}`,//请求地址
      method: 'get',//请求方式
      staticParams: { //固定参数
        type: ''
      }
    },
    listOption: {//返回数据的路径（dataPath）、下拉列表取值（value）和展示（label）的字段、
      dataPath: 'data',
      label: '',
      value: ''
    }
  },
  '包装': {//选择器C（可模糊搜索/多选）
    selectOption: { //select 额外配置
      isSelect: true,//是否选择框 //!选择器A（基础用法/单选）选择器B（星标/单选）选择器C（多选）选择器D（可模糊搜索/多选）选择器E（可模糊搜索/单选) 选择器F（图标组合样式）
      // isCascader:true,//是否为级联 //!级联选择器A（四级联选择） 级联选择器B（含模糊搜索选择）
      customSelectType: 'SC',//!S开头代表下拉选择，C开头代表级联：例如SA->选择器A（基础用法/单选）
      allowSearch: true,//可模糊
      multiple: true// 多选 multiple
    },
    ajaxOption: {
      url: `${memberBase}/basicdata-code/list`,//请求地址
      method: 'get',//请求方式
      staticParams: { //固定参数
        codeTypeCode: 'cargopackagingtype'
      }
    },
    listOption: {//返回数据的路径（dataPath）、下拉列表取值（value）和展示（label）的字段、
      dataPath: 'data',
      label: 'codeName',
      value: 'codesValues'
    }
  },
  '币别': {//选择器C（可模糊搜索/多选）
    selectOption: { //select 额外配置
      isSelect: true,//是否选择框 //!选择器A（基础用法/单选）选择器B（星标/单选）选择器C（多选）选择器D（可模糊搜索/多选）选择器E（可模糊搜索/单选) 选择器F（图标组合样式）
      // isCascader:true,//是否为级联 //!级联选择器A（四级联选择） 级联选择器B（含模糊搜索选择）
      customSelectType: 'SC',//!S开头代表下拉选择，C开头代表级联：例如SA->选择器A（基础用法/单选）
      allowSearch: true,//可模糊
      multiple: true// 多选 multiple
    },
    ajaxOption: {
      url: `${scmBase}/relation/list-of-currency`,//请求地址
      method: 'get',//请求方式
      staticParams: { //固定参数
        type: ''
      }
    },
    listOption: {//返回数据的路径（dataPath）、下拉列表取值（value）和展示（label）的字段、
      dataPath: 'data',
      label: 'currencyCode',
      value: 'currencyCode'
    }
  },
  '换单供应商': {//选择器C（可模糊搜索/多选）
    selectOption: { //select 额外配置
      isSelect: true,//是否选择框 //!选择器A（基础用法/单选）选择器B（星标/单选）选择器C（多选）选择器D（可模糊搜索/多选）选择器E（可模糊搜索/单选) 选择器F（图标组合样式）
      // isCascader:true,//是否为级联 //!级联选择器A（四级联选择） 级联选择器B（含模糊搜索选择）
      customSelectType: 'SC',//!S开头代表下拉选择，C开头代表级联：例如SA->选择器A（基础用法/单选）
      allowSearch: true,//可模糊
      multiple: true// 多选 multiple
    },
    ajaxOption: {
      url: `${scmBase}/relation-view/queryRelationByDropDown`,//请求地址
      method: 'get',//请求方式
      staticParams: { //固定参数
        relationTypes: '9',
        labelCodes: 'ChangeOrder'
      }
    },
    listOption: {//返回数据的路径（dataPath）、下拉列表取值（value）和展示（label）的字段、
      dataPath: 'data',
      label: 'name',
      value: 'enterpriseId'
    }
  },
  '换单员': {//选择器C（可模糊搜索/多选）
    selectOption: { //select 额外配置
      isSelect: true,//是否选择框 //!选择器A（基础用法/单选）选择器B（星标/单选）选择器C（多选）选择器D（可模糊搜索/多选）选择器E（可模糊搜索/单选) 选择器F（图标组合样式）
      // isCascader:true,//是否为级联 //!级联选择器A（四级联选择） 级联选择器B（含模糊搜索选择）
      customSelectType: 'SC',//!S开头代表下拉选择，C开头代表级联：例如SA->选择器A（基础用法/单选）
      allowSearch: true,//可模糊
      multiple: true// 多选 multiple
    },
    ajaxOption: {
      url: `${memberBase}/business-role-config-view/select-by-role`,//请求地址
      method: 'post',//请求方式
      staticParams: { //固定参数
        roleType: '7' // 7-换单员
      }
    },
    listOption: {//返回数据的路径（dataPath）、下拉列表取值（value）和展示（label）的字段、
      dataPath: 'data',
      label: 'userName',
      value: 'userId'
    }
  },
  '报关供应商': {//选择器C（可模糊搜索/多选）
    selectOption: { //select 额外配置
      isSelect: true,//是否选择框 //!选择器A（基础用法/单选）选择器B（星标/单选）选择器C（多选）选择器D（可模糊搜索/多选）选择器E（可模糊搜索/单选) 选择器F（图标组合样式）
      // isCascader:true,//是否为级联 //!级联选择器A（四级联选择） 级联选择器B（含模糊搜索选择）
      customSelectType: 'SC',//!S开头代表下拉选择，C开头代表级联：例如SA->选择器A（基础用法/单选）
      allowSearch: true,//可模糊
      multiple: true// 多选 multiple
    },
    ajaxOption: {
      url: `${scmBase}/relation-view/queryRelationByDropDown`,//请求地址
      method: 'get',//请求方式
      staticParams: { //固定参数
        relationTypes: '9',
        labelCodes: 'CustomsDeclaration'
      }
    },
    listOption: {//返回数据的路径（dataPath）、下拉列表取值（value）和展示（label）的字段、
      dataPath: 'data',
      label: 'name',
      value: 'enterpriseId'
    }
  },
  '制单员': {//选择器C（可模糊搜索/多选）
    selectOption: { //select 额外配置
      isSelect: true,//是否选择框 //!选择器A（基础用法/单选）选择器B（星标/单选）选择器C（多选）选择器D（可模糊搜索/多选）选择器E（可模糊搜索/单选) 选择器F（图标组合样式）
      // isCascader:true,//是否为级联 //!级联选择器A（四级联选择） 级联选择器B（含模糊搜索选择）
      customSelectType: 'SC',//!S开头代表下拉选择，C开头代表级联：例如SA->选择器A（基础用法/单选）
      allowSearch: true,//可模糊
      multiple: true// 多选 multiple
    },
    ajaxOption: {
      url: `${memberBase}/business-role-config-view/select-by-role`,//请求地址
      method: 'post',//请求方式
      staticParams: { //固定参数
        roleType: '8' // 8-制单员 
      }
    },
    listOption: {//返回数据的路径（dataPath）、下拉列表取值（value）和展示（label）的字段、
      dataPath: 'data',
      label: 'userName',
      value: 'userId'
    }
  },
  '运输供应商': {//选择器C（可模糊搜索/多选）
    selectOption: { //select 额外配置
      isSelect: true,//是否选择框 //!选择器A（基础用法/单选）选择器B（星标/单选）选择器C（多选）选择器D（可模糊搜索/多选）选择器E（可模糊搜索/单选) 选择器F（图标组合样式）
      // isCascader:true,//是否为级联 //!级联选择器A（四级联选择） 级联选择器B（含模糊搜索选择）
      customSelectType: 'SC',//!S开头代表下拉选择，C开头代表级联：例如SA->选择器A（基础用法/单选）
      allowSearch: true,//可模糊
      multiple: true// 多选 multiple
    },
    ajaxOption: {
      url: `${scmBase}/relation-view/queryRelationByDropDown`,//请求地址
      method: 'get',//请求方式
      staticParams: { //固定参数
        relationTypes: '9',
        labelCodes: 'RoadTransport'
      }
    },
    listOption: {//返回数据的路径（dataPath）、下拉列表取值（value）和展示（label）的字段、
      dataPath: 'data',
      label: 'name',
      value: 'enterpriseId'
    }
  },
  '仓储供应商': {//选择器C（可模糊搜索/多选）
    selectOption: { //select 额外配置
      isSelect: true,//是否选择框 //!选择器A（基础用法/单选）选择器B（星标/单选）选择器C（多选）选择器D（可模糊搜索/多选）选择器E（可模糊搜索/单选) 选择器F（图标组合样式）
      // isCascader:true,//是否为级联 //!级联选择器A（四级联选择） 级联选择器B（含模糊搜索选择）
      customSelectType: 'SC',//!S开头代表下拉选择，C开头代表级联：例如SA->选择器A（基础用法/单选）
      allowSearch: true,//可模糊
      multiple: true// 多选 multiple
    },
    ajaxOption: {
      url: `${scmBase}/relation-view/queryRelationByDropDown`,//请求地址
      method: 'get',//请求方式
      staticParams: { //固定参数
        relationTypes: '9',
        labelCodes: 'Storage'
      }
    },
    listOption: {//返回数据的路径（dataPath）、下拉列表取值（value）和展示（label）的字段、
      dataPath: 'data',
      label: 'name',
      value: 'enterpriseId'
    }
  },
  '本方公司': {//选择器C（可模糊搜索/多选）
    selectOption: { //select 额外配置
      isSelect: true,//是否选择框 //!选择器A（基础用法/单选）选择器B（星标/单选）选择器C（多选）选择器D（可模糊搜索/多选）选择器E（可模糊搜索/单选) 选择器F（图标组合样式）
      // isCascader:true,//是否为级联 //!级联选择器A（四级联选择） 级联选择器B（含模糊搜索选择）
      customSelectType: 'SC',//!S开头代表下拉选择，C开头代表级联：例如SA->选择器A（基础用法/单选）
      allowSearch: true,//可模糊
      multiple: true// 多选 multiple
    },
    ajaxOption: {
      url: `${scmBase}/relation-view/queryRelationByDropDown`,//请求地址
      method: 'get',//请求方式
      staticParams: { //固定参数
        relationTypes: '4'
      }
    },
    listOption: {//返回数据的路径（dataPath）、下拉列表取值（value）和展示（label）的字段、
      dataPath: 'data',
      label: 'name',
      value: 'enterpriseId'
    }
  },
  '客服部门': {//选择器C（可模糊搜索/多选）
    selectOption: { //select 额外配置
      isSelect: true,//是否选择框 //!选择器A（基础用法/单选）选择器B（星标/单选）选择器C（多选）选择器D（可模糊搜索/多选）选择器E（可模糊搜索/单选) 选择器F（图标组合样式）
      // isCascader:true,//是否为级联 //!级联选择器A（四级联选择） 级联选择器B（含模糊搜索选择）
      customSelectType: 'SC',//!S开头代表下拉选择，C开头代表级联：例如SA->选择器A（基础用法/单选）
      allowSearch: true,//可模糊
      multiple: true// 多选 multiple
    },
    ajaxOption: {
      url: `${memberBase}/business-department-config-view/select`,//请求地址
      method: 'get',//请求方式
      staticParams: { //固定参数
        departmentType: '2'
      }
    },
    listOption: {//返回数据的路径（dataPath）、下拉列表取值（value）和展示（label）的字段、
      dataPath: 'data',
      label: 'departmentName',
      value: 'departmentId'
    }
  },
  '营销部门': {//选择器C（可模糊搜索/多选）
    selectOption: { //select 额外配置
      isSelect: true,//是否选择框 //!选择器A（基础用法/单选）选择器B（星标/单选）选择器C（多选）选择器D（可模糊搜索/多选）选择器E（可模糊搜索/单选) 选择器F（图标组合样式）
      // isCascader:true,//是否为级联 //!级联选择器A（四级联选择） 级联选择器B（含模糊搜索选择）
      customSelectType: 'SC',//!S开头代表下拉选择，C开头代表级联：例如SA->选择器A（基础用法/单选）
      allowSearch: true,//可模糊
      multiple: true// 多选 multiple
    },
    // /basicdata/basicdata-code/list
    ajaxOption: {
      url: `${memberBase}/business-department-config-view/select`,//请求地址
      method: 'get',//请求方式
      staticParams: { //固定参数
        departmentType: '1'
      }
    },
    listOption: {//返回数据的路径（dataPath）、下拉列表取值（value）和展示（label）的字段、
      dataPath: 'data',
      label: 'departmentName',
      value: 'departmentId'
    }
  },
  '销售员': {//选择器C（可模糊搜索/多选）
    selectOption: { //select 额外配置
      isSelect: true,//是否选择框 //!选择器A（基础用法/单选）选择器B（星标/单选）选择器C（多选）选择器D（可模糊搜索/多选）选择器E（可模糊搜索/单选) 选择器F（图标组合样式）
      isCascader:true,//是否为级联 //!级联选择器A（四级联选择） 级联选择器B（含模糊搜索选择）
      customSelectType: 'SC',//!S开头代表下拉选择，C开头代表级联：例如SA->选择器A（基础用法/单选）
      allowSearch: true,//可模糊
      multiple: true, // 多选 multiple
    },
    ajaxOption: {
      url: `${memberBase}/business-role-config-view/select-by-role`,//请求地址
      method: 'post',//请求方式
      staticParams: { //固定参数
        roleType: '1'
      }
    },
    listOption: {//返回数据的路径（dataPath）、下拉列表取值（value）和展示（label）的字段、
      dataPath: 'data',
      label: 'userName',
      value: 'userId'
    }
  },
  '接单员': {//选择器C（可模糊搜索/多选）
    selectOption: { //select 额外配置
      isSelect: true,//是否选择框 //!选择器A（基础用法/单选）选择器B（星标/单选）选择器C（多选）选择器D（可模糊搜索/多选）选择器E（可模糊搜索/单选) 选择器F（图标组合样式）
      // isCascader:true,//是否为级联 //!级联选择器A（四级联选择） 级联选择器B（含模糊搜索选择）
      customSelectType: 'SC',//!S开头代表下拉选择，C开头代表级联：例如SA->选择器A（基础用法/单选）
      allowSearch: true,//可模糊
      multiple: true// 多选 multiple
    },
    ajaxOption: {
      url: `${memberBase}/business-role-config-view/select-by-role`,//请求地址
      method: 'post',//请求方式
      staticParams: { //固定参数
        roleType: '2'
      }
    },
    listOption: {//返回数据的路径（dataPath）、下拉列表取值（value）和展示（label）的字段、
      dataPath: 'data',
      label: 'userName',
      value: 'userId'
    }
  },
  '审单员': {//选择器C（可模糊搜索/多选）
    selectOption: { //select 额外配置
      isSelect: true,//是否选择框 //!选择器A（基础用法/单选）选择器B（星标/单选）选择器C（多选）选择器D（可模糊搜索/多选）选择器E（可模糊搜索/单选) 选择器F（图标组合样式）
      // isCascader:true,//是否为级联 //!级联选择器A（四级联选择） 级联选择器B（含模糊搜索选择）
      customSelectType: 'SC',//!S开头代表下拉选择，C开头代表级联：例如SA->选择器A（基础用法/单选）
      allowSearch: true,//可模糊
      multiple: true// 多选 multiple
    },
    ajaxOption: {
      url: `${memberBase}/business-role-config-view/select-by-role`,//请求地址
      method: 'post',//请求方式
      staticParams: { //固定参数
        roleType: '3' // 3-审单员
      }
    },
    listOption: {//返回数据的路径（dataPath）、下拉列表取值（value）和展示（label）的字段、
      dataPath: 'data',
      label: 'userName',
      value: 'userId'
    }
  },
  '客服经理': {//选择器C（可模糊搜索/多选）
    selectOption: { //select 额外配置
      isSelect: true,//是否选择框 //!选择器A（基础用法/单选）选择器B（星标/单选）选择器C（多选）选择器D（可模糊搜索/多选）选择器E（可模糊搜索/单选) 选择器F（图标组合样式）
      // isCascader:true,//是否为级联 //!级联选择器A（四级联选择） 级联选择器B（含模糊搜索选择）
      customSelectType: 'SC',//!S开头代表下拉选择，C开头代表级联：例如SA->选择器A（基础用法/单选）
      allowSearch: true,//可模糊
      multiple: true// 多选 multiple
    },
    ajaxOption: {
      url: `${memberBase}/business-role-config-view/select-by-role`,//请求地址
      method: 'post',//请求方式
      staticParams: { //固定参数
        roleType: '5' // 5-客服经理
      }
    },
    listOption: {//返回数据的路径（dataPath）、下拉列表取值（value）和展示（label）的字段、
      dataPath: 'data',
      label: 'userName',
      value: 'userId'
    }
  },
  '客服专员': {//选择器C（可模糊搜索/多选）
    selectOption: { //select 额外配置
      isSelect: true,//是否选择框 //!选择器A（基础用法/单选）选择器B（星标/单选）选择器C（多选）选择器D（可模糊搜索/多选）选择器E（可模糊搜索/单选) 选择器F（图标组合样式）
      // isCascader:true,//是否为级联 //!级联选择器A（四级联选择） 级联选择器B（含模糊搜索选择）
      customSelectType: 'SC',//!S开头代表下拉选择，C开头代表级联：例如SA->选择器A（基础用法/单选）
      allowSearch: true,//可模糊
      multiple: true// 多选 multiple
    },
    ajaxOption: {
      url: `${memberBase}/business-role-config-view/select-by-role`,//请求地址
      method: 'post',//请求方式
      staticParams: { //固定参数
        roleType: '4' // 1-销售员 2-接单员 3-审单员 4-客服专员 5-客服经理 6-结算员 7-换单员 8-制单员 9-质量专员
      }
    },
    listOption: {//返回数据的路径（dataPath）、下拉列表取值（value）和展示（label）的字段、
      dataPath: 'data',
      label: 'userName',
      value: 'userId'
    }
  },
  '结算员': {//选择器C（可模糊搜索/多选）
    selectOption: { //select 额外配置
      isSelect: true,//是否选择框 //!选择器A（基础用法/单选）选择器B（星标/单选）选择器C（多选）选择器D（可模糊搜索/多选）选择器E（可模糊搜索/单选) 选择器F（图标组合样式）
      // isCascader:true,//是否为级联 //!级联选择器A（四级联选择） 级联选择器B（含模糊搜索选择）
      customSelectType: 'SC',//!S开头代表下拉选择，C开头代表级联：例如SA->选择器A（基础用法/单选）
      allowSearch: true,//可模糊
      multiple: true// 多选 multiple
    },
    ajaxOption: {
      url: `${memberBase}/business-role-config-view/select-by-role`,//请求地址
      method: 'post',//请求方式
      staticParams: { //固定参数
        roleType: '6'
      }
    },
    listOption: {//返回数据的路径（dataPath）、下拉列表取值（value）和展示（label）的字段、
      dataPath: 'data',
      label: 'userName',
      value: 'userId'
    }
  },
  '船代': {//选择器C（可模糊搜索/多选）
    selectOption: { //select 额外配置
      isSelect: true,//是否选择框 //!选择器A（基础用法/单选）选择器B（星标/单选）选择器C（多选）选择器D（可模糊搜索/多选）选择器E（可模糊搜索/单选) 选择器F（图标组合样式）
      // isCascader:true,//是否为级联 //!级联选择器A（四级联选择） 级联选择器B（含模糊搜索选择）
      customSelectType: 'SC',//!S开头代表下拉选择，C开头代表级联：例如SA->选择器A（基础用法/单选）
      allowSearch: true,//可模糊
      multiple: true// 多选 multiple
    },
    ajaxOption: {
      url: `${scmBase}/relation-view/queryRelationByDropDown`,//请求地址
      method: 'get',//请求方式
      staticParams: { //固定参数
        relationTypes: '9',
        labelCodes: 'ShippingAgency'
      }
    },
    listOption: {//返回数据的路径（dataPath）、下拉列表取值（value）和展示（label）的字段、
      dataPath: 'data',
      label: 'name',
      value: 'enterpriseId'
    }
  },
  '海运费支付对象': {//选择器C（可模糊搜索/多选）
    selectOption: { //select 额外配置
      isSelect: true,//是否选择框 //!选择器A（基础用法/单选）选择器B（星标/单选）选择器C（多选）选择器D（可模糊搜索/多选）选择器E（可模糊搜索/单选) 选择器F（图标组合样式）
      // isCascader:true,//是否为级联 //!级联选择器A（四级联选择） 级联选择器B（含模糊搜索选择）
      customSelectType: 'SC',//!S开头代表下拉选择，C开头代表级联：例如SA->选择器A（基础用法/单选）
      allowSearch: true,//可模糊
      multiple: true// 多选 multiple
    },
    ajaxOption: {
      url: `${scmBase}/relation-view/queryRelationByDropDown`,//请求地址
      method: 'get',//请求方式
      staticParams: { //固定参数
        relationTypes: '9',
        labelCodes: 'ShippingAgency,FreightForwarder,InterWaterwayTransportation'
      }
    },
    listOption: {//返回数据的路径（dataPath）、下拉列表取值（value）和展示（label）的字段、
      dataPath: 'data',
      label: 'name',
      value: 'enterpriseId'
    }
  },
  '成员': {//选择器C（可模糊搜索/多选）
    selectOption: { //select 额外配置
      isSelect: true,//是否选择框 //!选择器A（基础用法/单选）选择器B（星标/单选）选择器C（多选）选择器D（可模糊搜索/多选）选择器E（可模糊搜索/单选) 选择器F（图标组合样式）
      // isCascader:true,//是否为级联 //!级联选择器A（四级联选择） 级联选择器B（含模糊搜索选择）
      customSelectType: 'SC',//!S开头代表下拉选择，C开头代表级联：例如SA->选择器A（基础用法/单选）
      allowSearch: true,//可模糊
      filterOption: false, // 远程搜索时设置
      multiple: true// 多选 multiple
    },
    ajaxOption: {
      url: `${memberBase}/enterprise-user/get-users-by-enterpriseid`,//请求地址
      method: 'get',//请求方式
      staticParams: { //固定参数
      }
    },
    listOption: {//返回数据的路径（dataPath）、下拉列表取值（value）和展示（label）的字段、
      dataPath: 'data',
      label: 'userName',
      value: 'userId'
    }
  },
  '客户': {//选择器C（可模糊搜索/多选）
    selectOption: { //select 额外配置
      isSelect: true,//是否选择框 //!选择器A（基础用法/单选）选择器B（星标/单选）选择器C（多选）选择器D（可模糊搜索/多选）选择器E（可模糊搜索/单选) 选择器F（图标组合样式）
      // isCascader:true,//是否为级联 //!级联选择器A（四级联选择） 级联选择器B（含模糊搜索选择）
      customSelectType: 'SC',//!S开头代表下拉选择，C开头代表级联：例如SA->选择器A（基础用法/单选）
      allowSearch: true,//可模糊
      multiple: true// 多选 multiple
    },
    ajaxOption: {
      url: `${scmBase}`,//请求地址
      method: 'get',//请求方式
      staticParams: { //固定参数
        type: ''
      }
    },
    listOption: {//返回数据的路径（dataPath）、下拉列表取值（value）和展示（label）的字段、
      dataPath: 'data',
      label: '',
      value: ''
    }
  },
  '供应商': {//选择器C（可模糊搜索/多选）
    selectOption: { //select 额外配置
      isSelect: true,//是否选择框 //!选择器A（基础用法/单选）选择器B（星标/单选）选择器C（多选）选择器D（可模糊搜索/多选）选择器E（可模糊搜索/单选) 选择器F（图标组合样式）
      // isCascader:true,//是否为级联 //!级联选择器A（四级联选择） 级联选择器B（含模糊搜索选择）
      customSelectType: 'SC',//!S开头代表下拉选择，C开头代表级联：例如SA->选择器A（基础用法/单选）
      allowSearch: true,//可模糊
      multiple: true// 多选 multiple
    },
    ajaxOption: {
      url: `${scmBase}`,//请求地址
      method: 'get',//请求方式
      staticParams: { //固定参数
        type: ''
      }
    },
    listOption: {//返回数据的路径（dataPath）、下拉列表取值（value）和展示（label）的字段、
      dataPath: 'data',
      label: '',
      value: ''
    }
  },
  '费用项目': {//选择器C（可模糊搜索/多选）
    selectOption: { //select 额外配置
      isSelect: true,//是否选择框 //!选择器A（基础用法/单选）选择器B（星标/单选）选择器C（多选）选择器D（可模糊搜索/多选）选择器E（可模糊搜索/单选) 选择器F（图标组合样式）
      // isCascader:true,//是否为级联 //!级联选择器A（四级联选择） 级联选择器B（含模糊搜索选择）
      customSelectType: 'SC',//!S开头代表下拉选择，C开头代表级联：例如SA->选择器A（基础用法/单选）
      allowSearch: true,//可模糊
      multiple: true// 多选 multiple
    },
    ajaxOption: {
      url: `${scmBase}`,//请求地址
      method: 'get',//请求方式
      staticParams: { //固定参数
        type: ''
      }
    },
    listOption: {//返回数据的路径（dataPath）、下拉列表取值（value）和展示（label）的字段、
      dataPath: 'data',
      label: '',
      value: ''
    }
  },
  //#endregion

  //#region //! 级联选择器A（门点选择）
  '门点': {
    selectOption: { //select 额外配置
      // isSelect:true,//是否选择框 //!选择器A（基础用法/单选）选择器B（星标/单选）选择器C（多选）选择器D（可模糊搜索/多选）选择器E（可模糊搜索/单选) 选择器F（图标组合样式）
      isCascader: true,//是否为级联 //!级联选择器A（四级联选择） 级联选择器B（含模糊搜索选择）
      customSelectType: 'CA',//!S开头代表下拉选择，C开头代表级联：例如SA->选择器A（基础用法/单选）
      // multiple: true,// 多选 multiple, 
      // allowSearch: true,//模糊 allowSearch
    },
    ajaxOption: {
      url: `${basicdataBase}/basicdata-wharf-port-area/list`,//请求地址
      method: 'get',//请求方式
      staticParams: { //固定参数 
        // countryId:104
      }
    },
    listOption: {//返回数据的路径（dataPath）、下拉列表取值（value）和展示（label）的字段、
      dataPath: 'data',
      label: 'mosWharf',
      value: 'mosWharf'
    },
  },
  //#endregion

  //#region //! 级联选择器B（多选） 港区-靠泊码头
  '港区': {
    selectOption: { //select 额外配置
      // isSelect:true,//是否选择框 //!选择器A（基础用法/单选）选择器B（星标/单选）选择器C（多选）选择器D（可模糊搜索/多选）选择器E（可模糊搜索/单选) 选择器F（图标组合样式）
      isTreeSelect: true,//是否为级联 //!级联选择器A（四级联选择） 级联选择器B（含模糊搜索选择）
      customSelectType: '',//!S开头代表下拉选择，C开头代表级联：例如SA->选择器A（基础用法/单选）
      multiple: true,// 多选 multiple, 
      allowSearch: true,//模糊 allowSearch
      subDataPath: 'basicdataWharfPortArea',
      subLabel: 'mosPortArea',
      subValue: 'mosPortArea'
    },
    ajaxOption: {
      url: `${basicdataBase}/basicdata-seaport/list-port`,//请求地址
      method: 'get',//请求方式
      staticParams: { //固定参数 
        countryId:104
      }
    },
    listOption: {//返回数据的路径（dataPath）、下拉列表取值（value）和展示（label）的字段、
      dataPath: 'data',
      subDataPath: 'basicdataWharfPortArea',
      label: 'seaportName',
      value: 'seaportId'
    },
  },
  '靠泊码头': {
    selectOption: { //select 额外配置
      isSelect: true,//是否选择框 //!选择器A（基础用法/单选）选择器B（星标/单选）选择器C（多选）选择器D（可模糊搜索/多选）选择器E（可模糊搜索/单选) 选择器F（图标组合样式）
      // isCascader:true,//是否为级联 //!级联选择器A（四级联选择） 级联选择器B（含模糊搜索选择）
      customSelectType: 'SC',//!S开头代表下拉选择，C开头代表级联：例如SA->选择器A（基础用法/单选）
      multiple: true,// 多选 multiple, 
      allowSearch: true,//模糊 allowSearch
    },
    ajaxOption: {
      url: `${basicdataBase}/basicdata-wharf-port-area/list`,//请求地址
      method: 'get',//请求方式
      staticParams: { //固定参数
        type: ''
      }
    },
    listOption: {//返回数据的路径（dataPath）、下拉列表取值（value）和展示（label）的字段、
      dataPath: 'data',
      label: 'mosWharf',
      value: 'mosWharf'
    },
  },
  //#endregion

  // 供应商合同列表筛选选择器
  '供应商合同审批状态': {
    selectOption: { //select 额外配置
      isSelect: true,//是否选择框 //!选择器A（基础用法/单选）选择器B（星标/单选）选择器C（多选）选择器D（可模糊搜索/多选）选择器E（可模糊搜索/单选) 选择器F（图标组合样式）
      // isCascader:true,//是否为级联 //!级联选择器A（四级联选择） 级联选择器B（含模糊搜索选择）
      customSelectType: 'SC',//!S开头代表下拉选择，C开头代表级联：例如SA->选择器A（基础用法/单选）
      multiple: true,// 多选 multiple, 
      allowSearch: true,//模糊 allowSearch
    },
    ajaxOption: {
      url: `${crmBase}/common/selectListEnum`,//请求地址
      method: 'get',//请求方式
      staticParams: { //固定参数
        enumName: "CustomerContractApproverStatusEnum"
      }
    },
    listOption: {//返回数据的路径（dataPath）、下拉列表取值（value）和展示（label）的字段、
      dataPath: 'data',
      label: 'desc',
      value: 'value'
    },
  },
  '供应商合同类型': {
    selectOption: { //select 额外配置
      isSelect: true,//是否选择框 //!选择器A（基础用法/单选）选择器B（星标/单选）选择器C（多选）选择器D（可模糊搜索/多选）选择器E（可模糊搜索/单选) 选择器F（图标组合样式）
      // isCascader:true,//是否为级联 //!级联选择器A（四级联选择） 级联选择器B（含模糊搜索选择）
      customSelectType: 'SC',//!S开头代表下拉选择，C开头代表级联：例如SA->选择器A（基础用法/单选）
      multiple: true,// 多选 multiple, 
      allowSearch: true,//模糊 allowSearch
    },
    ajaxOption: {
      url: `${memberBase}/basicdata-code/list`,//请求地址
      method: 'get',//请求方式
      staticParams: { //固定参数
        codeTypeCode: 'suppliercontracttype'
      }
    },
    listOption: {//返回数据的路径（dataPath）、下拉列表取值（value）和展示（label）的字段、
      dataPath: 'data',
      label: 'codeName',
      value: 'codesValues'
    },
  },
  '签约人': {
    selectOption: { //select 额外配置
      isSelect: true,//是否选择框 //!选择器A（基础用法/单选）选择器B（星标/单选）选择器C（多选）选择器D（可模糊搜索/多选）选择器E（可模糊搜索/单选) 选择器F（图标组合样式）
      // isCascader:true,//是否为级联 //!级联选择器A（四级联选择） 级联选择器B（含模糊搜索选择）
      customSelectType: 'SD',//!S开头代表下拉选择，C开头代表级联：例如SA->选择器A（基础用法/单选）
      multiple: true,// 多选 multiple, 
      allowSearch: true,//模糊 allowSearch
    },
    ajaxOption: {
      url: `${memberBase}/enterprise-user/get-users-by-enterpriseid`,//请求地址
      method: 'get',//请求方式
      staticParams: { //固定参数
      }
    },
    listOption: {//返回数据的路径（dataPath）、下拉列表取值（value）和展示（label）的字段、
      dataPath: 'data',
      label: 'userName',
      value: 'userId'
    },
  },
  '所属部门': {
    selectOption: { //select 额外配置
      isSelect: true,//是否选择框 //!选择器A（基础用法/单选）选择器B（星标/单选）选择器C（多选）选择器D（可模糊搜索/多选）选择器E（可模糊搜索/单选) 选择器F（图标组合样式）
      // isCascader:true,//是否为级联 //!级联选择器A（四级联选择） 级联选择器B（含模糊搜索选择）
      customSelectType: 'TA',//!S开头代表下拉选择，C开头代表级联：例如SA->选择器A（基础用法/单选）
      multiple: true,// 多选 multiple, 
      allowSearch: true,//模糊 allowSearch
    },
    ajaxOption: {
      url: `${memberBase}/organization-management/enterprise-or-organization-list`,//请求地址
      method: 'get',//请求方式
      staticParams: { //固定参数
      }
    },
    listOption: {//返回数据的路径（dataPath）、下拉列表取值（value）和展示（label）的字段、
      dataPath: 'data',
      label: 'organizationAbbreviation',
      value: 'organizationId'
    },
  },
  
  '业务类型': {  // 标准产品
    selectOption: { //select 额外配置
      isSelect: true,//是否选择框 //!选择器A（基础用法/单选）选择器B（星标/单选）选择器C（多选）选择器D（可模糊搜索/多选）选择器E（可模糊搜索/单选) 选择器F（图标组合样式）
      // isCascader:true,//是否为级联 //!级联选择器A（四级联选择） 级联选择器B（含模糊搜索选择）
      customSelectType: 'SB',//!S开头代表下拉选择，C开头代表级联：例如SA->选择器A（基础用法/单选）
      multiple: true// 多选 multiple
    },
    ajaxOption: {
      url: `${basicdataBase}/basicdata-code/list`,//请求地址
      method: 'get',//请求方式
      staticParams: { //固定参数
        codeTypeCode: 'customerbusinesstype'
      }
    },
    listOption: {//返回数据的路径（dataPath）、下拉列表取值（value）和展示（label）的字段、
      dataPath: 'data',
      label: 'codeName',
      value: 'codesValues'
    }
  },
  '创建人': {
    selectOption: { //select 额外配置
      isSelect: true,//是否选择框 //!选择器A（基础用法/单选）选择器B（星标/单选）选择器C（多选）选择器D（可模糊搜索/多选）选择器E（可模糊搜索/单选) 选择器F（图标组合样式）
      // isCascader:true,//是否为级联 //!级联选择器A（四级联选择） 级联选择器B（含模糊搜索选择）
      customSelectType: 'SD',//!S开头代表下拉选择，C开头代表级联：例如SA->选择器A（基础用法/单选）
      multiple: true,// 多选 multiple, 
      allowSearch: true,//模糊 allowSearch
    },
    ajaxOption: {
      url: `${memberBase}/enterprise-user/get-users-by-enterpriseid`,//请求地址
      method: 'get',//请求方式
      staticParams: { //固定参数
      }
    },
    listOption: {//返回数据的路径（dataPath）、下拉列表取值（value）和展示（label）的字段、
      dataPath: 'data',
      label: 'userName',
      value: 'userId'
    },
  },
  '单证类型': {
    selectOption: { //select 额外配置
      isSelect: true,//是否选择框 //!选择器A（基础用法/单选）选择器B（星标/单选）选择器C（多选）选择器D（可模糊搜索/多选）选择器E（可模糊搜索/单选) 选择器F（图标组合样式）
      // isCascader:true,//是否为级联 //!级联选择器A（四级联选择） 级联选择器B（含模糊搜索选择）
      customSelectType: 'SC',//!S开头代表下拉选择，C开头代表级联：例如SA->选择器A（基础用法/单选）
      multiple: true,// 多选 multiple, 
      allowSearch: true,//模糊 allowSearch
    },
    ajaxOption: {
      url: `${memberBase}/basicdata-code/list`,//请求地址
      method: 'get',//请求方式
      staticParams: { //固定参数
        codeTypeCode: 'businessdocuments'
      }
    },
    listOption: {//返回数据的路径（dataPath）、下拉列表取值（value）和展示（label）的字段、
      dataPath: 'data',
      label: 'codeName',
      value: 'codesValues'
    },
  },
  '单证签收状态': {
    selectOption: { //select 额外配置
      isSelect: true,//是否选择框 //!选择器A（基础用法/单选）选择器B（星标/单选）选择器C（多选）选择器D（可模糊搜索/多选）选择器E（可模糊搜索/单选) 选择器F（图标组合样式）
      // isCascader:true,//是否为级联 //!级联选择器A（四级联选择） 级联选择器B（含模糊搜索选择）
      customSelectType: 'SC',//!S开头代表下拉选择，C开头代表级联：例如SA->选择器A（基础用法/单选）
      multiple: true,// 多选 multiple, 
      allowSearch: true,//模糊 allowSearch
    },
    ajaxOption: {
      url: `${scmBase}/logistics-mos-view/selector`,//请求地址
      method: 'get',//请求方式
      staticParams: { //固定参数
        type: 'document_sign_status'
      }
    },
    listOption: {//返回数据的路径（dataPath）、下拉列表取值（value）和展示（label）的字段、
      dataPath: 'data.selectorInfoList',
      label: 'name',
      value: 'code'
    },
  },
  '单证名称': {
    selectOption: { //select 额外配置
      isSelect: true,//是否选择框 //!选择器A（基础用法/单选）选择器B（星标/单选）选择器C（多选）选择器D（可模糊搜索/多选）选择器E（可模糊搜索/单选) 选择器F（图标组合样式）
      // isCascader:true,//是否为级联 //!级联选择器A（四级联选择） 级联选择器B（含模糊搜索选择）
      customSelectType: 'SC',//!S开头代表下拉选择，C开头代表级联：例如SA->选择器A（基础用法/单选）
      multiple: true,// 多选 multiple, 
      allowSearch: true,//模糊 allowSearch
    },
    ajaxOption: {
      url: `${memberBase}/basicdata-code/list`,//请求地址
      method: 'get',//请求方式
      staticParams: { //固定参数
        codeTypeCode: 'businessdocuments'
      }
    },
    listOption: {//返回数据的路径（dataPath）、下拉列表取值（value）和展示（label）的字段、
      dataPath: 'data',
      label: 'codeName',
      value: 'codesValues'
    },
  },
}