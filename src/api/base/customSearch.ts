import axios from '@/api/interceptor';
import {  businessBase ,scmBase} from '@/api/api.config';
// 筛选器字段列表（列表\编辑\复制使用）
export function allFilter(params: { viewSystemId: string }) {
  return axios.get(`${businessBase}/view/all-filter`,{ params });
}

export function selectorType(params: { viewSystemId: string }) {
  return axios.get(`${scmBase}/logistics-mos-view/selector`,{ params });
}