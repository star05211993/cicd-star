import axios from '@/api/interceptor';
import {
    memberBase,
    scmBase,
    basicdataBase,
    settleBase,
} from '@/api/api.config';
export const settleTrackApi = {
    //结算单列表
    getSettleTrackPage: (data?: any) => {
        return axios({
            url: `${settleBase}/api/statement/page`,
            method: 'POST',
            data,
        })
    },
    // 导出
    SettleTrackExcelExport: (data?: any) => {
        return axios({
            url: `${settleBase}/api/statement/export`,
            method: 'POST',
            data,
        })
    },
    //统计列表数据
    getStatisticsData: (data?: any) => {
        return axios({
            url: `${settleBase}/api/statement/currency-statistics`,
            method: 'POST',
            data,
        })
    },
    // 添加星标
    settleTrackSubscribe: (data?: any) => {
        return axios({
            url: `${settleBase}/api/statement/subscription`,
            method: 'POST',
            data,
        })
    },
    // 取消星标
    cancelSubscribe: (params?: any) => {
        return axios({
            url: `${settleBase}/api/statement/unsubscribe`,
            method: 'DELETE',
            params,
        })
    },
    //箱量统计
    getBoxVolumnStatistics: (data?: any) => {
        return axios({
            url: `${settleBase}/api/statement/box-volume-statistics`,
            method: 'POST',
            data,
        })
    },
    // 批量成本关闭
    costBatchClose: (data?: any) => {
        return axios({
            url: `${settleBase}/api/statement/batch-close/cost`,
            method: 'POST',
            data,
        })
    },
    // 批量收入关闭
    incomeBatchClose: (data?: any) => {
        return axios({
            url: `${settleBase}/api/statement/batch-close/income`,
            method: 'POST',
            data,
        })
    },

    /**详情页面接口 */
    // 结算单详情
    settleTrackDetail: (params?: any) => {
        return axios({
            url: `${settleBase}/api/statement/detail`,
            method: 'GET',
            params,
        });
    },
    // 结算单详情下载
    settleTrackDetailDownload: (params?: any) => {
        return axios({
            url: `${settleBase}/api/statement/cost/download`,
            method: 'GET',
            params,
        });
    },
    // 运单业务跟踪
    logisticsTrack: (params?: any) => {
        return axios({
            url: `${scmBase}/logistics-mos/detail/get-track`,
            method: 'GET',
            params,
        });
    },
    // 结算送货-详情
    deliveryDetail: (params?: any) => {
        return axios({
            url: `${scmBase}/logistics-mos/delivery/detail-settle`,
            method: 'GET',
            params,
        });
    },
    // 业务结算详情-删除成本票
    settleTrackCostFile: (data?: any) => {
        return axios({
            url: `${settleBase}/api/statement/cost/file-remove`,
            method: 'POST',
            data,
        })
    },
    // 业务结算详情-取消结算复核
    settleTrackUnreviewed: (data?: any) => {
        return axios({
            url: `${settleBase}/api/statement/unreviewed`,
            method: 'POST',
            data,
        })
    },
    // 业务结算详情-结算复核
    settleTrackReviewed: (data?: any) => {
        return axios({
            url: `${settleBase}/api/statement/reviewed`,
            method: 'POST',
            data,
        })
    },
    // 业务结算详情-收入关闭
    settleTrackCloseIncome: (data?: any) => {
        return axios({
            url: `${settleBase}/api/statement/close/income`,
            method: 'POST',
            data,
        })
    },
    // 业务结算详情-成本关闭
    settleTrackCloseCost: (data?: any) => {
        return axios({
            url: `${settleBase}/api/statement/close/cost`,
            method: 'POST',
            data,
        })
    },
    // 业务结算详情-关账
    settleTrackClosed: (data?: any) => {
        return axios({
            url: `${settleBase}/api/statement/closed`,
            method: 'POST',
            data,
        })
    },
    // 关账撤销接口
    rollbackAdjustment: (data?: any) => {
        return axios({
            url: `${settleBase}/api/statement/rollback/adjustment`,
            method: 'POST',
            data,
        })
    },
    //账单详情页-可加入账单
    receivableCanJoin: (params?: any) => {
        return axios({
            url: `${settleBase}/api/receivable/can-join`,
            method: 'GET',
            params,
        });
    },
    // 加入账单
    settleTrackJoinReceivable: (data?: any) => {
        return axios({
            url: `${settleBase}/api/statement/income/join-receivable`,
            method: 'POST',
            data,
        })
    },
    // 根据角色名称获取所属成员 - 结算员
    settleTrackClerkNames: (params?: any) => {
        return axios({
            url: `${memberBase}/enterprise-role/getEnterpriseUsersByRoleName`,
            method: 'GET',
            params,
        });
    },
    // 业务结算详情-授权结算
    settleTrackAuthorized: (data?: any) => {
        return axios({
            url: `${settleBase}/api/statement/authorized`,
            method: 'POST',
            data,
        })
    },
    // 业务结算详情-更换结算
    settleTrackChange: (data?: any) => {
        return axios({
            url: `${settleBase}/api/statement/change`,
            method: 'POST',
            data,
        })
    },
    // 业务结算详情-复核
    settleTrackReview: (data?: any) => {
        return axios({
            url: `${settleBase}/api/statement/review`,
            method: 'POST',
            data,
        })
    },
    // 补录关闭开启按钮
    closeMakeup: (data?: any) => {
        return axios({
            url: `${settleBase}/api/statement/close/make/up`,
            method: 'POST',
            data,
        })
    },

    // 查询企业费用项-收入
    settleBasicEnterpriseIncomeFee: (data?: any) => {
        return axios({
            url: `${settleBase}/api/basicdata/enterprise-income-fee`,
            method: 'POST',
            data,
        })
    },
    // 查询企业币种汇率
    settleBasicCurrencyRate: (data?: any) => {
        return axios({
            url: `${settleBase}/api/basicdata/enterprise-currency-rate`,
            method: 'POST',
            data,
        })
    },
    // 获取费用默认发票
    getLogisticsFeeDefaultInvoice: (params?: any) => {
        return axios({
            url: `${settleBase}/api/logistics/fee/default-invoice`,
            method: 'GET',
            params,
        });
    },
    // 业务结算详情-添加收入
    settleTrackIncome: (data?: any) => {
        return axios({
            url: `${settleBase}/api/statement/income`,
            method: 'POST',
            data,
        })
    },
    //码表
    basicdataCodeSelect: (data?: any) => {
        return axios({
            url: `${basicdataBase}/basicdata-code/select`,
            method: 'POST',
            data,
        });
    },
    // 业务结算详情-添加成本
    settleTrackCost: (data?: any) => {
        return axios({
            url: `${settleBase}/api/statement/cost`,
            method: 'POST',
            data,
        });
    },
    //上传文件
    fileUpload: (data?: any) => {
        return axios({
            url: `${scmBase}/file/upload`,
            method: 'POST',
            data,
            paramsType: true,
            headerType: 'form',
        });
    },
    // 业务结算详情-结算单收入详情
    settleTrackIncomeDetail: (params?: any) => {
        return axios({
            url: `${settleBase}/api/statement/income/details`,
            method: 'GET',
            params,
        });
    },
    // 业务结算详情-结算单成本详情
    settleTrackCostDetail: (params?: any) => {
        return axios({
            url: `${settleBase}/api/statement/cost/details`,
            method: 'GET',
            params,
        });
    },
    // 业务结算详情-批量删除预估收入
    settleTrackBatchEstimateincome: (data?: any) => {
        return axios({
            url: `${settleBase}/api/statement/estimateincome/batch-remove`,
            method: 'POST',
            data,
        });
    },
    // 业务结算详情-批量删除收入
    settleTrackBatchIncome: (data?: any) => {
        return axios({
            url: `${settleBase}/api/statement/income/batch-remove`,
            method: 'POST',
            data,
        });
    },
    // 业务结算详情-批量删除成本
    settleTrackBatchCost: (data?: any) => {
        return axios({
            url: `${settleBase}/api/statement/cost/batch-remove`,
            method: 'POST',
            data,
        });
    },
}