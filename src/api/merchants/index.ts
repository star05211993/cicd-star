import axios from '@/api/interceptor';
import { scmBase,crmBase} from '@/api/api.config';
export const merchants ={
  //客商列表 显示视图
  mosRelationViewPage: (data?: any) => {
    return axios({
      url: `${scmBase}/relation-view/basic/page`,
      method: 'POST',
      data,
    });
  },
  //客商列表导出
  mosRelationViewExcel: (data?: any) => {
    return axios({
      url: `${scmBase}/relation-view/list/excel`,
      method: 'POST',
      data,
      responseType: "blob",
    });
  },
  //检查企业名称或者助记码
  mosBasicUniqueBusiness: (data?: any) => {
    return axios({
      url: `${scmBase}/relation/basic/check-unique-business`,
      method: 'POST',
      data,
    });
  },
  //调用外部接口获取企业的工商信息
  mosQueryRelationLienceByOut: (params?: any) => {
    return axios({
      url: `${scmBase}/relation/queryRelationLicenseByOut`,
      method: 'GET',
      params,
    });
  },
  //查询所有的客户与供应商
  mosQueryRelationByAll: (params?: any) => {
    return axios({
      url: `${scmBase}/relation/queryRelationByAll`,
      method: 'GET',
      params,
    });
  },
  //查询所有的客商关系的标签
  mosQueryAelationAabelAll: (params?: any) => {
    return axios({
      url: `${scmBase}/relation/query-relation-label-all`,
      method: 'GET',
      params,
    });
  },
  //保存客商关系-适用客商管理
  mosSaveRelation: (data?: any) => {
    return axios({
      url: `${scmBase}/relation-view/insert-or-update-by-business`,
      method: 'POST',
      data,
    });
  },

  

  //基本信息-查询详情(适用客商)
  mosBasicDetailKS: (params?: any) => {
    return axios({
      url: `${scmBase}/relation/basic/detail-ks`,
      method: 'GET',
      params,
    });
  },
  //保存客商关系-适用客商管理
  mosBasicSaveOrUpdate: (data?: any) => {
    return axios({
      url: `${scmBase}/relation/insert-or-update-by-business`,
      method: 'POST',
      data,
    });
  },
  //查询企业的工商信息
  mosRelationLience: (params?: any) => {
    return axios({
      url: `${scmBase}/relation/query-relation-lience`,
      method: 'GET',
      params,
    });
  },
  // 保存企业的工商信息
  mosSaveLience: (data?: any) => {
    return axios({
      url: `${scmBase}/relation/save-relation-lience`,
      method: 'POST',
      data,
    });
  },
  // 保存企业的申报信息
  mosSaveCustomsRecord: (data?: any) => {
    return axios({
      url: `${scmBase}/relation/save-customs-record`,
      method: 'POST',
      data,
    });
  },
  //联系人-列表
  mosContactsList: (params?: any) => {
    return axios({
      url: `${scmBase}/relation/contact/list`,
      method: 'GET',
      params,
    });
  },
  //批量联系人-新增或修改
  mosContactsUp: (data?: any) => {
    return axios({
      url: `${scmBase}/relation/contact/all-save-or-update`,
      method: 'POST',
      data,
    });
  },
  //联系人-删除联系人
  mosContactsDelete: (params?: any) => {
    return axios({
      url: `${scmBase}/relation/contact/delete-contact`,
      method: 'GET',
      params,
    });
  },
  //根据客户id查询供应商合同和客户合同
  mosContractInfo: (params?: any) => {
    return axios({
      url: `${crmBase}/contract-info/getApiContractInfo`,
      method: 'GET',
      params,
    });
  },
  //开票信息-详情,返回多个
  mosInvoiceTitleDetailList: (params?: any) => {
    return axios({
      url: `${scmBase}/relation/invoice-title/detail-list`,
      method: 'GET',
      params,
    });
  },
  //开票信息-删除
  mosInvoiceDel: (params?: any) => {
    return axios({
      url: `${scmBase}/relation/invoice-title/delete`,
      method: 'GET',
      params,
    });
  },
  // 开票信息-新增或修改
  mosInvoiceUp: (data?: any) => {
    return axios({
      url: `${scmBase}/relation/invoice-title/save-or-update-business`,
      method: 'POST',
      data,
    });
  },


  //查询开票范围信息
  mosInvoiceRangeQuery: (params?: any) => {
    return axios({
      url: `${scmBase}/relation/invoice-range/query`,
      method: 'GET',
      params,
    });
  },
  //删除开票范围信息
  mosInvoiceRangeDelete: (params?: any) => {
    return axios({
      url: `${scmBase}/relation/invoice-range/delete`,
      method: 'GET',
      params,
    });
  },
  //批量保存修改开票范围信息
  mosInvoiceRangeUp: (data?: any) => {
    return axios({
      url: `${scmBase}/relation/invoice-range/all-save`,
      method: 'POST',
      data,
    });
  },

  //银行信息-列表
  mosBankList: (params?: any) => {
    return axios({
      url: `${scmBase}/relation/bank/list`,
      method: 'GET',
      params,
    });
  },
  //银行信息-删除联系人
  mosBankDel: (params?: any) => {
    return axios({
      url: `${scmBase}/relation/bank/delete-bank`,
      method: 'GET',
      params,
    });
  },
  //银行信息-批量新增修改
  mosBankUp: (data?: any) => {
    return axios({
      url: `${scmBase}/relation/bank/all-save-or-updateByKS`,
      method: 'POST',
      data,
    });
  },

  //门点地址列表
  mosDoorList: (params?: any) => {
    return axios({
      url: `${scmBase}/relation-dock/select-door-list-by-relation-id`,
      method: 'GET',
      params,
    });
  },
  //保存委托人门点数据
  mosSaveRelationDoorInfo: (data?: any) => {
    return axios({
      url: `${scmBase}/relation-dock/save-relation-door-info`,
      method: 'POST',
      data,
    });
  },
  //删除委托人门点数据
  mosDeleteRelationDoor: (params?: any) => {
    return axios({
      url: `${scmBase}/relation-dock/delete-relation-door`,
      method: 'GET',
      params,
    });
  },
  // 门点地址列表-切换默认联系人
  mosRelationDoorDefaultBank: (params?: any) => {
    return axios({
      url: `${scmBase}/relation-dock/change-default-bank`,
      method: 'GET',
      params,
    });
  },
  // /scm/relation-dock/change-default-bank
  //提单列表
  mosRiseList: (params?: any) => {
    return axios({
      url: `${scmBase}/relation-dock/select-loading-display-by-relationid`,
      method: 'GET',
      params,
    });
  },
  //删除提单方式
  mosRiseDel: (params?: any) => {
    return axios({
      url: `${scmBase}/relation-dock/delete-by-id`,
      method: 'GET',
      params,
    });
  },
}