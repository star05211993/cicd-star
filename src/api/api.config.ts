export const env = import.meta.env.MODE;
export const envMap = {
  // localhost: 'https://openapi-dev.antsexpress.net/gateway', // 本地代理调试 NODE_ENV = "development"
  test: 'http://openapi.antsexpress.net/gateway', // 本地代理调试 NODE_ENV = "development"
  dev: 'http://openapi-dev.antsexpress.net/gateway',
  // dev: 'http://localhost:5000',
  qc: 'https://openapi.antsexpress.net/gateway',
  pre: 'https://openapi-pre.antsexpress.net/gateway',
  // prod: 'https://openapi.mayibida.com/gateway',
  // prod: 'http://localhost:5000',
  prods: 'https://openapi.mayizhilian.net/gateway',
};
const key = envMap[env as keyof typeof envMap]; // 环境ip地址
// const key = 'http://openapi.antsexpress.net/gateway'
// console.error(key)
const memberBase = `${key}/member`; // 会员中心
const scmBase = `${key}/scm`; // scm
const wmsBase = `${key}/wms`; // 仓库
const productBase = `${key}/product`;
const buriedBase = `${key}/point`; // 埋点
const notificationBase = `${key}/message`; // 通知中心
const businessBase = `${key}/business`; // 冻品
const processBase = `${key}/process`; // 流程配置
const basicdataBase = `${key}/basicdata`;
const settleBase = `${key}/settle`;
const crmBase = `${key}/crm`;
const process = `${key}/process`;
const customsBase = `${key}/customs`;
export {
  memberBase,
  scmBase,
  productBase,
  buriedBase,
  notificationBase,
  wmsBase,
  businessBase,
  processBase,
  basicdataBase,
  settleBase,
  crmBase,
  process,
  customsBase,
};

/**
 * msgObj： 返回的错误消息
 */
export const msgObj = {
  '200': '正常',
  '401': '登录过期或未登录，请重新登录', // 登录过期或未登录，请重新登录
  '90000': '网络请求繁忙，请稍后再试',
};
