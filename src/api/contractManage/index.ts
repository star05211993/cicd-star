import axios from '@/api/interceptor';
import { crmBase, memberBase, basicdataBase, scmBase, productBase } from '@/api/api.config';
export const contractManage = {
  contractList: (params: {//客户合同列表
    viewUserId: string, // 用户视图ID
    keyword: string, // 搜索关键字
    pageNum: number, // 当前页-默认为1	
    pageSize: number, // 每页大小-默认为10	
    viewFilterRule: '', //筛选规则: 整体为字符串数组JSON格式数组，单条规则之间分割符为|||
    orderByViewSystemFieldId: '', // 排序字段的系统视图字段ID	
    orderByType: '',//排序类型: 降序-desc 升序-asc
  }) => {
    return axios.post(`${crmBase}/contract-info/pageViewList`, { ...params });
  },

  //#region 基本信息相关
  //! 查询用户拥有的角色列表
  userRoleList: (params?: any) => {
    return axios({
      url: `${memberBase}/role/userRoleList`,
      method: 'GET',
      params,
    });
  },

  // !客户合同撤回
  contractRecall: (params?: any) => {
    return axios({
      url: `${crmBase}/contract-info/recall`,
      method: 'GET',
      params,
    });
  },
  // !业务类型
  getBusinessType: (params?: any) => {
    return axios({
      // url: `${crmBase}/contract-business-types/getBusinessType`,
      url: `${basicdataBase}/basicdata-code/list`,
      method: 'GET',
      params,
    });
  },
  // !适用品类
  category: (params?: any) => {
    return axios({
      // url: `${crmBase}/contract-business-types/getBusinessType`,
      url: `${productBase}/saleProduct/select-all-category-name`,
      method: 'GET',
      params,
    });
  },
  // !服务项目   
  serveList: (params?: any) => {
    return axios({
      url: `${basicdataBase}/basicdata-code/relation-list`,
      method: 'GET',
      params,
    });
  },
  // !客户名称   
  customName: (params?: any) => {
    return axios({
      url: `${scmBase}/relation/queryRelationByLabelCode`,
      method: 'GET',
      params,
    });
  },
  // !供应商名称   
  supplierName: (params?: any) => {
    return axios({
      url: `${scmBase}/relation/queryRelationByTypesAndLabelCodes`,
      method: 'GET',
      params,
    });
  },
  // !签约抬头   
  relationList: (params?: any) => {
    return axios({
      url: `${scmBase}/relation-dock/relation-list`,
      method: 'GET',
      params,
    });
  },
    // !查询用户拥有的角色列表   
    getUserRoleList: (params?: any) => {
      return axios({
        url: `${memberBase}/role/userRoleList`,
        method: 'GET',
        params,
      });
    },
    // 
  // !客户合同复制
  contractCopy: (data?: any) => {
    return axios({
      url: `${crmBase}/contract-info/copy`,
      method: 'POST',
      data,
    });
  },
  // !签约销售   
  salesRole: (data?: any) => {
    return axios({
      url: `${memberBase}/business-role-config/select-by-role`,
      method: 'POST',
      data,
    });
  },
  // !营销部门   
  userOrganizationList: (params?: any) => {
    return axios({
      url: `${memberBase}/organization-management/userOrganizationList`,
      method: 'GET',
      params,
    });
  },
  // !控货方名称   
  constrcName: (params?: any) => {
    return axios({
      url: `${scmBase}/relation/queryRelationByLabelCode`,
      method: 'GET',
      params,
    });
  },
  // !新增 双方协议基本信息保存   
  addDoubleAgreement: (data?: any) => {
    return axios({
      url: `${crmBase}/contract-info/addDoubleAgreement`,
      method: 'POST',
      data,
    });
  },
  // !编辑 双方协议基本信息保存   
  updateDoubleAgreement: (data?: any) => {
    return axios({
      url: `${crmBase}/contract-info/updateDoubleAgreement`,
      method: 'POST',
      data,
    });
  },
  // !编辑回显
  contractInfo: (params?: any) => {
    return axios({
      url: `${crmBase}/contract-info/detail`,
      method: 'GET',
      params,
    });
  },
  // !合同详情
  getDetails: (params?: any) => {
    return axios({
      url: `${crmBase}/contract-info/info`,
      method: 'GET',
      params,
    });
  },
  // !新增 三方协议   
  addThirdAgreement: (data?: any) => {
    return axios({
      url: `${crmBase}/contract-info/addThirdAgreement`,
      method: 'POST',
      data,
    });
  },
  // !编辑 三方协议编辑   
  updateThirdAgreement: (data?: any) => {
    return axios({
      url: `${crmBase}/contract-info/updateThirdAgreement`,
      method: 'POST',
      data,
    });
  },
  // !新增 添加控货双方协议   
  addControlDoubleAgreement: (data?: any) => {
    return axios({
      url: `${crmBase}/contract-info/addControlDoubleAgreement`,
      method: 'POST',
      data,
    });
  },
  // !编辑 控货双方协议编辑   
  updateControlDoubleAgreement: (data?: any) => {
    return axios({
      url: `${crmBase}/contract-info/updateControlDoubleAgreement`,
      method: 'POST',
      data,
    });
  },
  // !新增 形式协议   
  addContractFormalAgreement: (data?: any) => {
    return axios({
      url: `${crmBase}/contract-info/addContractFormalAgreement`,
      method: 'POST',
      data,
    });
  },
  // !编辑 形式协议   
  updateContractFormalAgreement: (data?: any) => {
    return axios({
      url: `${crmBase}/contract-info/updateContractFormalAgreement`,
      method: 'POST',
      data,
    });
  },

  // !新增 补充协议   
  addSupplementaryAgreement: (data?: any) => {
    return axios({
      url: `${crmBase}/contract-info/addSupplementaryAgreement`,
      method: 'POST',
      data,
    });
  },
  // !编辑 补充协议   
  updateSupplementaryAgreement: (data?: any) => {
    return axios({
      url: `${crmBase}/contract-info/updateSupplementaryAgreement`,
      method: 'POST',
      data,
    });
  },
  // 导出客户合同列表
  exportContractDownload :(data?: any) => {
    return axios({
      url: `${crmBase}/contract-info/exportViewList`,
      method: 'POST',
      data,
      responseType:'blob',
    });
  },







  //#endregion

  //#region 报价书相关
  quoteBookInfoList: (params?: any) => {//查询合同报价书列表
    return axios({
      url: `${crmBase}/contract-offer-confirmation/list`,
      method: 'GET',
      params,
    });
  },
  containerSizeList: (params?: any) => {//附件类型 //!箱型 箱量
    return axios({
      url: `${basicdataBase}/basicdata-code/list`,
      method: 'GET',
      params,
    });
  },
  // uploadBatch: (data?: any) => { ////批量上传
  //   return axios({
  //     url: `${crmBase}/file/upload_batch`,
  //     method: 'POST',
  //     data,
  //     paramsType: true,
  //     headerType: 'form',
  //   });
  // },
  //上传文件
  fileUpload: (data?: any) => {
    return axios({
      url: `${scmBase}/file/upload`,
      method: 'POST',
      data,
      paramsType: true,
      headerType: 'form',
    });
  },
  // 添加附件
  addAttachment: (data?: any) => {
    return axios({
      url: `${crmBase}/contract-info/addAttachment`,
      method: 'POST',
      data,
    });
  },
  crmContractInfoUpdataAttachment: (data?: any) => { // 修改更新合同附件
    return axios({
      url: `${crmBase}/contract-info/updataAttachment`,
      method: 'POST',
      data,
    });
  },
  // 上传附件供应商合同
  addSupplierAttachment: (data?: any) => {
    return axios({
      url: `${crmBase}/supplier-contracts-info/addAttachment`,
      method: 'POST',
      data,
    });
  },

  // 修改供应商合同附件
  updateAttachment: (data: any) => {
    return axios({
      url: `${crmBase}/supplier-contracts-info/updateAttachment`,
      method: 'POST',
      data,
    });
  },

  // 供应商合同批量下载
  supplierBatchDownload: (params?: any) => {
    return axios({
      url: `${crmBase}/supplier-contracts-info/file/batch-download`,
      method: 'GET',
      params,
      responseType:'blob',
    });
  },

  // 供应商合同批量下载
  contractBatchDownload: (params?: any) => {
    return axios({
      url: `${crmBase}/contract-info/file/batch-download`,
      method: 'GET',
      params,
      responseType:"blob"
    });
  },
  // 删除合同，报价列表某条数据
  offerDelete: (params: any) => {
    return axios({
      url: `${crmBase}/contract-offer-confirmation/delete`,
      method: 'GET',
      params,
    });
  },



  //#endregion

  //#region 关于添加报价书

  // !港口   
  port: (params?: any) => {
    return axios({
      url: `${basicdataBase}/basicdata-seaport/list`,
      method: 'GET',
      params,
    });
  },
  //!港区
  portArea: (params?: any) => {
    return axios({
      url: `${basicdataBase}/basicdata-wharf-port-area/get-by-port-code-distinct`,
      method: 'GET',
      params,
    });
  },
  // !清关运输-报价测算-查询标准产品
  quoteCalcList: (data?: any) => {
    return axios({
      url: `${crmBase}/standard-products/quote-calc-list`,
      method: 'POST',
      data,
    });
  },
  // !选择合同
  selectContract: (params?: any) => {
    return axios({
      url: `${crmBase}/contract-info/customerContractList`,
      method: 'GET',
      params,
    });
  },
  // !获取详情
  quoteDetailInfo: (params?: any) => {
    return axios({
      url: `${crmBase}/contract-offer-confirmation/quote-detail-info`,
      method: 'GET',
      params,
    });
  },

  // !清关运输-报价测算-完成
  contractOfferComplete: (data?: any) => {
    return axios({
      url: `${crmBase}/contract-offer-confirmation/complete`,
      method: 'POST',
      data,
    });
  },
  //#endregion

  //#region 
  // !清关运输-税点及结算方式-保存
  saveOtherInfo: (data?: any) => {
    return axios({
      url: `${crmBase}/contract-offer-confirmation/save-other-info`,
      method: 'POST',
      data,
    });
  },
  //#endregion

  //#region 
  // !检查企业名称或者助记码
  mosNetworkBasicUniqueBusiness: (data?: any) => {
    return axios({
      url: `${scmBase}/relation/basic/check-unique-business`,
      method: 'POST',
      data,
    });
  },
  // !查询企业
  queryRelationLicenseByOut: (params?: any) => {
    return axios({
      url: `${scmBase}/relation/queryRelationLicenseByOut`,
      method: 'GET',
      params,
    });
  },

  // !查询所有的客商关系的标签
  mosNetworkQueryAelationAabelAll: (params?: any) => {
    return axios({
      url: `${scmBase}/relation/query-relation-label-all`,
      method: 'GET',
      params,
    });
  },
  // !保存客商关系-适用客商管理
  mosNetworkInsertOrUpdateByBusiness: (data?: any) => {
    return axios({
      url: `${scmBase}/relation/insert-or-update-by-business`,
      method: 'POST',
      data,
    });
  },
  //#endregion 

  // ! 散货-清关运输-确认费用范围
  BulkCargoSaveQuote: (data?: any) => {
    return axios({
      url: `${crmBase}/contract-offer-confirmation/bulk-cargo/save-quote`,
      method: 'POST',
      data,
    });
  },
}