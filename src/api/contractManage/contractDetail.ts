import axios from '@/api/interceptor';
import { crmBase, memberBase, basicdataBase, scmBase, productBase } from '@/api/api.config';

export const contractDetailApi = {
    //合同基本信息
    contractInfo: (params?: any) => {
        return axios({
            url: `${crmBase}/contract-info/detail`,
            method: 'GET',
            params,
        });
    },
    // 供应商合同详情（包含补充协议）
    supplierDetail: (params?: any) => {
        return axios({
            url: `${crmBase}/supplier-contracts-info/detail`,
            method: 'GET',
            params,
        });
    },
    // 查询合同报价确认书表
    quoteBookInfo: (params?: any) => {
        return axios({
            url: `${crmBase}/contract-offer-confirmation/info`,
            method: 'GET',
            params,
        });
    },
    //查询报价模板中使用的标准产品 
    standardProduct: (data?: any) => {
        return axios({
            url: `${crmBase}/quotation-template/standardProduct`,
            method: 'POST',
            data,
        });
    },
}