import axios from '@/api/interceptor';
import {
  scmBase,
  memberBase,
  settleBase,
  basicdataBase,
  crmBase,
  businessBase,
  process,
  productBase
} from '@/api/api.config';
export const runOrderApi = {
  //正本签收查询
  signQuery: (data?: any) => {
    return axios({
      url: `${scmBase}/logistics-mos/original-signature-query`,
      method: 'POST',
      data,
    });
  },
  // 正本签收提交
  signSubmit: (data?: any) => {
    return axios({
      url: `${scmBase}/logistics-mos/original-signature-submit`,
      method: 'POST',
      data,
    });
  },
  // 国内买家委托人收货单位
  getBusinessUnitList: (params?: any) => {
    return axios({
      url: `${scmBase}/relation/queryRelationByLabelCode`,
      method: 'GET',
      params,
    });
  },
  checkBusiness: (data?: any) => {
    //校验添加国内买家,经营单位,收货单位
    return axios({
      url: `${scmBase}/relation/basic/check-unique-business`,
      method: 'POST',
      data,
    });
  },
  addBusinessUnits: (data?: any) => {
    //添加国内买家,经营单位,收货单位
    return axios({
      url: `${scmBase}/relation/insert-or-update-by-business`,
      method: 'POST',
      data,
    });
  },
  orderGetBusinessData: (params?: any) => {
    //新建运单，获取工商企业信息
    return axios({
      url: `${scmBase}/relation/queryRelationLicenseByOut`,
      method: 'GET',
      params,
    });
  },
  //换单列表
  changeOrderList: (data?: any) => {
    return axios({
      url: `${scmBase}/logistics-mos-view/exchangedo-tracking-list`,
      method: 'POST',
      data,
    });
  },
  orderFileUp: (data?: any) => {
    // 上传货物清单
    return axios({
      url: `${scmBase}/cargo-list/upload`,
      method: 'POST',
      data,
    });
  },
  cargoListFileList: (params?: any) => {
    // 上传货物清单查询
    return axios({
      url: `${scmBase}/logistics-mos-view/document`,
      method: 'GET',
      params,
    });
  },
  // 删除
  deleteRunOrder: (params?: any) => {
    // 上传货物清单查询
    return axios({
      url: `${scmBase}/logistics-mos/list/delete`,
      method: 'GET',
      params,
    });
  },
  cargoListFileDelete: (params?: any) => {
    // 运单文档上传
    return axios({
      url: `${scmBase}/logistics-mos/detail/document/delete`,
      method: 'GET',
      params,
    });
  },
  cargoListFilDownload: (params?: any) => {
    // 运单文档上传
    return axios({
      url: `${scmBase}/logistics-mos/detail/document/batch-upload`,
      method: 'GET',
      params,
      responseType: 'blob',
    });
  },
  cargoListFileMark: (params?: any) => {
    // 运单文档上传
    return axios({
      url: `${scmBase}/logistics-mos/detail/document/update`,
      method: 'GET',
      params,
    });
  },
  costFee: (data?: any) => {
    // 费用项目
    return axios({
      url: `${settleBase}/api/basicdata/enterprise-cost-fee`,
      method: 'POST',
      data,
    });
  },
  currencyList: (params?: any) => {
    // 币别
    return axios({
      url: `${scmBase}/relation/list-of-currency`,
      method: 'GET',
      params,
    });
  },
  costSubmit: (data?: any) => {
    // 额外费用提交
    return axios({
      url: `${scmBase}/logistics-mos/detail/cost-submit`,
      method: 'POST',
      data,
    });
  },
  businessCompleteSubmit: (data?: any) => {
    // 费用登记
    return axios({
      url: `${scmBase}/logistics-mos-view/update-logistics-business`,
      method: 'POST',
      data,
    });
  },
  documentPreparationSubmit: (data?: any) => {
    // 费用登记
    return axios({
      url: `${scmBase}/logistics-mos-view/order-preparation`,
      method: 'POST',
      data,
    });
  },
  deliveryDetail: (params?: any) => {
    // 查看运输计划
    return axios({
      url: `${scmBase}/logistics-mos/delivery/detail`,
      method: 'GET',
      params,
    });
  },
  updateDelivery: (data?: any) => {
    // 运输计划修改
    return axios({
      url: `${scmBase}/logistics-mos/delivery/update-container-info`,
      method: 'POST',
      data,
    });

  },
  // 入监管仓时间设置
  supervisedWarehouseSetTime: (data?: any) => {
    return axios({
      url: `${scmBase}/logistics-mos/delivery/supervised-warehouse/set-time`,
      method: 'POST',
      data,
    });

  },
  deletedelivery: (params?: any) => {
    // 删除运输计划
    return axios({
      url: `${scmBase}/logistics-mos/delivery/delete`,
      method: 'GET',
      params,
    });
  },
  subdelivery: (params?: any) => {
    // 提交运输计划
    return axios({
      url: `${scmBase}/logistics-mos/delivery/submit`,
      method: 'GET',
      params,
    });
  },
  withdrawNo: (data?: any) => {
    // 撤回运输计划详情单箱
    return axios({
      url: `${scmBase}/logistics-mos/delivery/cancel`,
      method: 'POST',
      data,
    });
  },
  getSupperlier: (params?: any) => {
    // 供应商
    return axios({
      url: `${scmBase}/relation-view/queryRelationByDropDown`,
      method: 'GET',
      params,
    });
  },
  getUser: (data?: any) => {
    // 换单员
    return axios({
      url: `${memberBase}/business-role-config-view/select-by-role`,
      method: 'POST',
      data,
    });
  },
  updateService: (data?: any) => {
    // 服务项目提交
    return axios({
      url: `${scmBase}/logistics-mos-view/save-update-service-item`,
      method: 'POST',
      data,
    });
  },
  remarkList: (params?: any) => {
    // 查询留言备注信息
    return axios({
      url: `${scmBase}/logistics-remarks/list`,
      method: 'GET',
      params,
    });
  },
  addRemarkList: (data?: any) => {
    // 新增留言备注信息
    return axios({
      url: `${scmBase}/logistics-remarks/insert`,
      method: 'POST',
      data,
    });
  },
  editRemarkList: (data?: any) => {
    // 编辑留言备注信息
    return axios({
      url: `${scmBase}/logistics-remarks/update`,
      method: 'POST',
      data,
    });
  },
  deleteRemarkList: (params?: any) => {
    // 删除留言备注信息
    return axios({
      url: `${scmBase}/logistics-remarks/delete/${params}`,
      method: 'GET',
    });
  },
  changeOrder: (data?: any) => {
    // 换单派单
    return axios({
      url: `${scmBase}/logistics-mos-view/change-order`,
      method: 'POST',
      data,
    });
  },
  changeProcess: (params?: any) => {
    // 制单正本流转
    return axios({
      url: `${scmBase}/logistics-mos-view/original-circulation/change-process`,
      method: 'GET',
      params,
    });
  },
  makeCargoList: (params?: any) => {
    // 制作货物清单
    return axios({
      url: `${scmBase}/logistics-mos-view/make-cargo-list`,
      method: 'GET',
      params,
    });
  },
  updateOrderTxt: (data?: any) => {
    // 视图节点文本编辑
    return axios({
      url: `${scmBase}/logistics-mos-view/update-logistics-name`,
      method: 'POST',
      data,
    });
  },
  wharfPort: (params?: any) => {
    // 靠泊码头
    return axios({
      url: `${basicdataBase}/basicdata-wharf-port-area/get-by-port-code`,
      method: 'GET',
      params,
    });
  },
  updateArivalTime: (data?: any) => {
    // 视图节点到港时间编辑
    return axios({
      url: `${scmBase}/logistics-mos-view/update-arrival-time`,
      method: 'POST',
      data,
    });
  },
  boxType: (data?: any) => {
    // 箱型箱类下拉
    return axios({
      url: `${basicdataBase}/basicdata-code/commonly-used-box`,
      method: 'POST',
      data,
    });
  },
  updateContainerQuantity: (data?: any) => {
    // 箱型箱量修改
    return axios({
      url: `${scmBase}/logistics-mos-view/update-container-quantity`,
      method: 'POST',
      data,
    });
  },
  updateContainerNo: (data?: any) => {
    // 箱号列表修改
    return axios({
      url: `${scmBase}/logistics-mos-view/update-container-no`,
      method: 'POST',
      data,
    });
  },
  packages: (params?: any) => {
    // 包装单位
    return axios({
      url: `${memberBase}/basicdata-code/list`,
      method: 'GET',
      params,
    });
  },
  getbillList: (params?: any) => {
    // 提单方式
    return axios({
      url: `${scmBase}/logistics-mos-view/selector`,
      method: 'GET',
      params,
    });
  },
  getfreightList: (params?: any) => {
    // 海运费
    return axios({
      url: `${scmBase}/logistics-mos-view/selector`,
      method: 'GET',
      params,
    });
  },
  getContract: (params?: any) => {
    // 查合同报价
    return axios({
      url: `${crmBase}/contract-info/contractOfferInfo`,
      method: 'GET',
      params,
    });
  },

  saveRelationDoorInfo: (data?: any) => {
    // 海运费
    return axios({
      url: `${scmBase}/relation-dock/save-relation-door-info`,
      method: 'POST',
      data,
    });
  },
  //wms仓库地址
  transferWarehouseAddressWms: (params?: any) => {
    // 海运费
    return axios({
      url: `${scmBase}/logistics-mos/delivery/transfer-warehouse-address/wms`,
      method: 'GET',
      params,
    });
  },
  //计划供应商
  queryRelationByLabelCodeAndValid: (params?: any) => {
    return axios({
      url: `${scmBase}/relation/queryRelationByLabelCodeAndValid`,
      method: 'GET',
      params,
    });
  },
  //根据入库单明细ID查询运输签收单
  mosDeliveryTransportSign: (params?: any) => {
    return axios({
      url: `${scmBase}/order-logistics-delivery/queryDeliveryTransportSign`,
      method: 'GET',
      params,
    });
  },
  //保存运输签收单
  mosDeliverySaveTransportSign: (data?: any) => {
    return axios({
      url: `${scmBase}/order-logistics-delivery/saveDeliveryTransportSign`,
      method: 'POST',
      data,
    });
  },

  standingTemplateList: (params?: any) => {
    //查询运单台账模版表
    return axios({
      url: `${scmBase}/standing-book-template/get-list`,
      method: 'GET',
      params,
    });
  },

  getStandingList: (data?: any) => {
    return axios({
      url: `${scmBase}/standing-book-template/list/page`,
      method: 'POST',
      data,
    });
  },
  deleteStanding: (params?: any) => {
    return axios({
      url: `${scmBase}/standing-book-template/delete-id`,
      method: 'GET',
      params,
    });
  },

  exportStanding: (data?: any) => {
    return axios({
      url: `${scmBase}/standing-book-template/list/excel`,
      method: 'POST',
      responseType:'blob',
      data,
    });
  },

  saveStanding: (data?: any) => {
    return axios({
      url: `${scmBase}/standing-book-template/save`,
      method: 'POST',
      data,
    });
  },
  standingBusiness: (data?: any) => {
    return axios({
      url: `${scmBase}/relation/queryRelationByLikeName`,
      method: 'POST',
      data,
    });
  },
  businessViewTabList: (params?: any) => {
    return axios({
      url: `${businessBase}/view/tab/list`,
      method: 'GET',
      params,
    });
  },
  businessViewTabDetail: (params?: any) => {
    return axios({
      url: `${businessBase}/view/tab/detail`,
      method: 'GET',
      params,
    });
  },
  taskPage: (data?: any) => {
    return axios({
      url: `${process}/ult-task/page`,
      method: 'POST',
      data,
    });
  },
  riskPage: (data?: any) => {
    return axios({
      url: `${process}/ult-risk/page`,
      method: 'POST',
      data,
    });
  },

  //   批量单证交接 提交
  handoverBusiness: (data?: any) => {
    return axios({
      url: `${scmBase}/logistics-exchangedo/document-handover-list`,
      method: 'POST',
      data,
    });
  },
  //运单详情
  orderBase: (params?: any) => {
    return axios({
      url: `${scmBase}/logistics-mos/detail/get`,
      method: 'GET',
      params,
    });
  },
  //查询新增运货计划时的待运输集装箱
  deliveryContainerChooseFlag: (params?: any) => {
    return axios({
      url: `${scmBase}/logistics-mos/delivery/add/container-choose-flag`,
      method: 'GET',
      params,
    });
  },
  //入客户门点 -保存
  mosDeliveryAddUpdate: (data?: any) => {
    return axios({
      url: `${scmBase}/logistics-mos/delivery/add-update`,
      method: 'POST',
      data,
    });
  },
  //入客户门点-提交
  mosDeliverySendDelivery: (data?: any) => {
    return axios({
      url: `${scmBase}/logistics-mos/delivery/send-delivery`,
      method: 'POST',
      data,
    });
  },
  // 查询批量单证交接 /scm/logistics-exchangedo/exchangedo-handover-list
  exchangedoHandoverBusiness: (data?: any) => {
    return axios({
      url: `${scmBase}/logistics-exchangedo/exchangedo-handover-list`,
      method: 'POST',
      data,
    });
  },
  //查询监管仓门点地址列表
  supervisedWarehouseDoorList: (params?: any) => {
    return axios({
      url: `${scmBase}/relation-dock/supervised-warehouse-door/list`,
      method: 'GET',
      params,
    });
  },
  //保存监管仓门点数据
  supervisedWarehouseDoorSave: (data?: any) => {
    return axios({
      url: `${scmBase}/relation-dock/supervised-warehouse-door/save`,
      method: 'POST',
      data,
    });
  },
  //删除监管仓门点数据
  supervisedWarehouseDoorDelete: (params?: any) => {
    return axios({
      url: `${scmBase}/relation-dock/supervised-warehouse-door/delete`,
      method: 'GET',
      params,
    });
  },
  //查询更新运货计划时的已勾选和未勾选的箱子
  deliveryUpdateContainerChooseFlag: (params?: any) => {
    return axios({
      url: `${scmBase}/logistics-mos/delivery/update/container-choose-flag`,
      method: 'GET',
      params,
    });
  },
  getDisplayCode: (params?: any) => {
    return axios({
      url: `${scmBase}/relation-dock/select-loading-display-list`,
      method: 'GET',
      params,
    });
  },
  getPostList: (params?: any) => {
    // 港口
    return axios({
      url: `${memberBase}/basicdata-seaport/list`,
      method: 'GET',
      params,
    });
  },
  baseCompany: (params?: any) => {
    // 船公司
        return axios({
            url: `${memberBase}/basic-data-shipping-company/list`,
            method: 'GET',
            params
        });
    },
    getProductName:(params?: any)=>{
        // 根据销售品名销查询商品品名
        return axios({
            url: `${productBase}/saleProduct/selectProductBySaleName`,
            method: 'GET',
            params
        });
    },
    getBusinessName:(params?: any)=>{
        // 根据销售品名销查询商品品名
        return axios({
            url: `${productBase}/saleProduct/selectCategoryBySaleAndProduct`,
            method: 'GET',
            params
        });
    },
    getSupplier:(params?: any)=>{
        // 服务供应商
        return axios({
            url: `${scmBase}/relation/queryRelationByLabelCodeAndValid`,
            method: 'GET',
            params
        });
    },
    getRole:(data?: any)=>{
        // 换单员
        return axios({
            url: `${memberBase}/business-role-config/select-by-role`,
            method: 'POST',
            data
        });
    },
    getService:(params?: any)=>{
      // 客服部门
      return axios({
          url: `${memberBase}/business-department-config/select-by-customer-service`,
          method: 'GET',
          params
      });
    },
    getFileList:(params?: any)=>{
      // 上传文件查询
      return axios({
          url: `${scmBase}/logistics-mos/detail/document`,
          method: 'GET',
          params
      });
    },
    uploadFileList:(data?: any)=>{
      // 上传文件
      return axios({
          url: `${scmBase}/logistics-document/multiple-upload`,
          method: 'POST',
          data
      });
    },
    updateFileList:(data?: any)=>{
      // 上传文件更新单证类型
      return axios({
          url: `${scmBase}/logistics-document/update`,
          method: 'POST',
          data
      });
    },
    entryBillList:(params?: any)=>{
      // 登记报关单查询
      return axios({
          url: `${scmBase}/api/customsclearance/details`,
          method: 'GET',
          params
      });
    },
    entryBillSub:(data?: any)=>{
      // 登记报关单
      return axios({
          url: `${scmBase}/api/customsclearance/logistics/batchsave`,
          method: 'POST',
          data
      });
    },
    deleteBill:(data?: any)=>{
      // 登记报关单删除
      return axios({
          url: `${scmBase}/api/customsclearance/delete`,
          method: 'POST',
          data
      });
    },
    saveOrder:(data?: any)=>{
      // 保存
      return axios({
          url: `${scmBase}/logistics-mos/save-logistics`,
          method: 'POST',
          data
      });
    },
    submitOrder:(data?: any)=>{
      // 提交
      return axios({
          url: `${scmBase}/logistics-mos/submit-logistics`,
          method: 'POST',
          data
      });
    },
    starOrder:(params?: any)=>{
      // 星标订单
      return axios({
          url: `${scmBase}/list-star/star`,
          method: 'GET',
          params
      });
    },
    exportOrder:(data?: any)=>{
      // 导出
      return axios({
          url: `${scmBase}/logistics-mos-view/list/excel`,
          method: 'POST',
          data,
          responseType: "blob",
      });
    },
}
    
