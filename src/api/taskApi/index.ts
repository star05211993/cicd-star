import axios from '@/api/interceptor';
import { scmBase, processBase } from '@/api/api.config';
export const taskApi = {
  //分类获取待处理的任务数量
  todoClassifyCount: (params?: any) => {
    return axios({
      url: `${processBase}/ult-task/todo-classify-count`,
      method: 'GET',
      params,
    });
  },
  //海运费确认-任务列表
  getlistSeaFreight: (data?: any) => {
    return axios({
      url: `${scmBase}/ult-task/list-sea-freight`,
      method: 'POST',
      data,
    });
  },
  //任务信息修改
  editTask: (data?: any) => {
    return axios({
      url: `${scmBase}/ult-task/edit-task`,
      method: 'POST',
      data,
    });
  },
  //海运费确认-确认支付方式
  confrimPaymentTypeSeaFreight: (data?: any) => {
    return axios({
      url: `${scmBase}/ult-task/confirm-payment-type-sea-freight`,
      method: 'POST',
      data,
    });
  },
  //海运费确认-确认支付
  confrimPaymentSeaFreight: (data?: any) => {
    return axios({
      url: `${scmBase}/ult-task/confirm-payment-sea-freight`,
      method: 'POST',
      data,
    });
  },
  //海运费确认-信息修改
  editSeaFreight: (data?: any) => {
    return axios({
      url: `${scmBase}/ult-task/edit-sea-freight`,
      method: 'POST',
      data,
    });
  },
  //海运费确认-筛选数量
  getCountSeaFreight: (params?: any) => {
    return axios({
      url: `${scmBase}/ult-task/count-sea-freight`,
      method: 'GET',
      params,
    });
  },
  //EHC 查询-任务列表
  listEhcQuery: (data?: any) => {
    return axios({
      url: `${scmBase}/ult-task/list-ehc-query`,
      method: 'POST',
      data,
    });
  },
  //EHC 查询-信息修改
  editEhcQuery: (data?: any) => {
    return axios({
      url: `${scmBase}/ult-task/edit-ehc-query`,
      method: 'POST',
      data,
    });
  },
  //EHC 查询-主功能按钮
  functionEehcQuery: (data?: any) => {
    return axios({
      url: `${scmBase}/ult-task/function-ehc-query`,
      method: 'POST',
      data,
    });
  },
  //电放信息确认-任务列表
  listDischarge: (data?: any) => {
    return axios({
      url: `${scmBase}/ult-task/list-discharge`,
      method: 'POST',
      data,
    });
  },
  //电放信息确认-信息修改
  editDischarge: (data?: any) => {
    return axios({
      url: `${scmBase}/ult-task/edit-discharge`,
      method: 'POST',
      data,
    });
  },
  //电放信息确认-主功能按钮
  functionDischarge: (data?: any) => {
    return axios({
      url: `${scmBase}/ult-task/function-discharge`,
      method: 'POST',
      data,
    });
  },
  //货物清单审核-任务列表
  listCargolist: (data?: any) => {
    return axios({
      url: `${scmBase}/ult-task/list-cargolist`,
      method: 'POST',
      data,
    });
  },
  //审核货物清单-货物清单列表
  listCargolistAudit: (params?: any) => {
    return axios({
      url: `${scmBase}/ult-task/list-cargolist-audit`,
      method: 'get',
      params,
    });
  },
  //审核货物清单-商品列表
  listCargolistAuditProduct: (params?: any) => {
    return axios({
      url: `${scmBase}/ult-task/list-cargolist-audit-product`,
      method: 'get',
      params,
    });
  },
  //货物清单审核-主功能
  functionCargolist: (data?: any) => {
    return axios({
      url: `${scmBase}/ult-task/function-cargolist`,
      method: 'POST',
      data,
    });
  },
  //审核货物清单-主功能
  functionCargolistAudit: (data?: any) => {
    return axios({
      url: `${scmBase}/ult-task/function-cargolist-audit`,
      method: 'POST',
      data,
    });
  },
  //申报回执处理-任务列表
  listDeclarationReceipt: (data?: any) => {
    return axios({
      url: `${scmBase}/ult-task/list-declaration-receipt`,
      method: 'POST',
      data,
    });
  },
  //申报回执处理-信息修改
  editDeclarationReceipt: (data?: any) => {
    return axios({
      url: `${scmBase}/ult-task/edit-declaration-receipt`,
      method: 'POST',
      data,
    });
  },
  //申报回执处理-主功能(完成)
  functionDeclarationReceipt: (data?: any) => {
    return axios({
      url: `${scmBase}/ult-task/function-declaration-receipt`,
      method: 'POST',
      data,
    });
  },
  //申报回执处理-异常原因枚举
  selectorDeclarationreceiptAbnormal: (params?: any) => {
    return axios({
      url: `${scmBase}/ult-task/selector-declaration-receipt-abnormal`,
      method: 'get',
      params,
    });
  },
  //异常处理-任务列表
  listException: (data?: any) => {
    return axios({
      url: `${scmBase}/ult-task/list-exception`,
      method: 'POST',
      data,
    });
  },
  //异常处理-主功能
  functionException: (data?: any) => {
    return axios({
      url: `${scmBase}/ult-task/function-exception`,
      method: 'POST',
      data,
    });
  },
  // 税金确认-筛选数量
  countTaxConfirmation: (params?: any) => {
    return axios({
      url: `${scmBase}/ult-task/count-tax-confirmation`,
      method: 'GET',
      params,
    });
  },

  //税金确认-任务列表
  listTaxconfirmation: (data?: any) => {
    return axios({
      url: `${scmBase}/ult-task/list-tax-confirmation`,
      method: 'POST',
      data,
    });
  },

  // 税金确认-确认支付方式
  confirmPaymentTypeTaxConfirmation: (data?: any) => {
    return axios({
      url: `${scmBase}/ult-task/confirm-payment-type-tax-confirmation`,
      method: 'POST',
      data,
    });
  },
  // 税金确认-确认支付
  confirmPaymentTaxConfirmation: (data?: any) => {
    return axios({
      url: `${scmBase}/ult-task/confirm-payment-tax-confirmation`,
      method: 'POST',
      data,
    });
  },
  // 税金确认-子表信息修改
  editChildTaxConfirmation: (data?: any) => {
    return axios({
      url: `${scmBase}/ult-task/edit-child-tax-confirmation`,
      method: 'POST',
      data,
    });
  },
  // 税金确认-母表信息修改
  editParentTaxConfirmation: (data?: any) => {
    return axios({
      url: `${scmBase}/ult-task/edit-parent-tax-confirmation`,
      method: 'POST',
      data,
    });
  },
  // 历史任务
  processUltTaskPage: (data?: any) => {
    return axios({
      url: `${processBase}/ult-task/page`,
      method: 'POST',
      data,
    });
  },
  // 任务配置分页
  taskConfigPage: (data?: any) => {
    return axios({
      url: `${processBase}/ult-task-config/page`,
      method: 'POST',
      data,
    });
  },
  // 任务配置修改
  taskConfigEditPage: (data?: any) => {
    return axios({
      url: `${processBase}/ult-task-config/edit-config`,
      method: 'POST',
      data,
    });
  },
  // 任务 选择器查询
  commonSelector: (params?: any) => {
    return axios({
      url: `${processBase}/common/selector`,
      method: 'GET',
      params,
    });
  },
  // 异常列表分页查询
  riskPage: (data?: any) => {
    return axios({
      url: `${processBase}/ult-risk/page`,
      method: 'POST',
      data,
    });
  },
  // 异常配置分页
  riskConfigPage: (data?: any) => {
    return axios({
      url: `${processBase}/ult-risk-config/page`,
      method: 'POST',
      data,
    });
  },
  // 异常配置修改
  riskConfigEditPage: (data?: any) => {
    return axios({
      url: `${processBase}/ult-risk-config/edit-config`,
      method: 'POST',
      data,
    });
  },
};
