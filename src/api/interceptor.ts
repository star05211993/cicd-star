import useAxios, { AxiosRequestConfig, AxiosResponse } from 'axios';
import { Message, Modal } from '@arco-design/web-vue';
import _ from 'lodash';
import { useUserStore } from '@/store';
import { AGENT, APPVERSION, APPID, APPNAME, DEFAULT_CODE } from '@/constant';
import { getToken, jsonToForm, sha256Encrypt, localStorage, sessionStorage } from '@/utils';
import jsonBig from 'json-bigint';
import { h } from 'vue'

export interface HttpResponse<T = unknown> {
  status: number;
  message: string;
  code: string;
  data: T;
}
if (import.meta.env.VITE_API_BASE_URL) {
  useAxios.defaults.baseURL = import.meta.env.VITE_API_BASE_URL;
}
const headerConfig: any = {
  json: {
    headers: {
      'Content-Type': 'application/json;charset=UTF-8',
      'X-Requested-With': 'XMLHttpRequest',
    },
  },
  form: {
    headers: {
      'Content-Type': 'multipart/form-data',
    },
  },
  file: {
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8',
    },
  },
  excel: {
    responseType: 'arraybuffer',
  },
  zip: {
    responseType: 'blob',
  },
};

// 雪花算法id超过17位
useAxios.defaults.transformResponse = [(data) => {
  try {
    return jsonBig({ storeAsString: true }).parse(data);
  } catch (e) {
    return data;
  }
},
];
declare module 'axios' {
  export interface AxiosRequestConfig {headerType?: string,paramsType?:Boolean}
}
useAxios.interceptors.request.use((config: any) => {
  // let each request carry token
  // this example using the JWT token
  // Authorization is a custom headers key
  // please modify it according to the actual situation

  const token = getToken();
  const language = localStorage.getStorage('language') === 'en-US' ? 'en' : 'zh';
  const requestId = sha256Encrypt(String(+new Date()));
  const requestAgent = AGENT;
  const appVersion = APPVERSION;
  const appId = APPID;
  const timestamp = +new Date();
  const appName = APPNAME;

  if (!requestId) { // 加密算法出问题了
    localStorage.clearStorage();
    sessionStorage.clearStorage();
    window.location.reload();
    return;
  }
  const { headerType = 'json', paramsType } = (config as any)
  if (paramsType) {
    config.data = jsonToForm(config.data)
  }
//  console.log(config,'======')
  const _headerConfig: any = _.assignIn(
    { ...headerConfig[headerType].headers },
    {
      'Accept': 'application/json, text/plain',
      'access-token': token,
      language,
      'request-id': requestId,
      'request-agent': requestAgent,
      'app-version': appVersion,
      'app-id': appId,
      timestamp,
      'app-name': appName,
      'sign': sha256Encrypt(appId + timestamp),
    } // 新增headers记得让后端加白名单
  );

  config.headers = _headerConfig;
  return config;
},
  (error) => {
    // do something
    return Promise.reject(error);
  }
);
// add response interceptors
useAxios.interceptors.response.use(
  (response: AxiosResponse<HttpResponse>) => {
    const res = response.data;
    if(res.code){
       if (res.code !== '200') {
          Message.error({
            content: () => {
              const { message } = res
              if (!message.includes('<br/>')) return message || 'Error'

              return h(
                'div',
                { style: { lineHeight: 1.5 } },
                [
                  message.split('<br/>').map((line: string) => {
                    return h('div', line)
                  })
                ]
              )
            },
            duration: 5 * 1000,
          });
      if (DEFAULT_CODE.includes(res.code)) {
        Modal.error({
          title: '登出确认',
          content:
            '登录过期，请重新登录',
          okText: '确认',
          async onOk() {
            const userStore = useUserStore();
            await userStore.logout();
            window.location.reload();
          },
        });
      }
      // return Promise.reject(new Error(res.message || 'Error'));
    }
    }
   
    return res;
  },
  (error) => {
    const { response } = error
    // 根据返回的code值来做不同的处理(和后端约定)
    switch (response?.code) {
      case '401':
        // token失效
        localStorage.removeStorage('token');
        Message.error({
          content: error.message || '登录过期或未登录，请重新登录"',
          duration: 5 * 1000,
        });
        // 跳转登录页
        // window.location.href = `/login?redirect=${encodeURIComponent(
        //   window.location.href
        // )}`;
        break;
      case '403':
        // 没有权限
        Message.error({
          content: error.message || '暂无权限',
          duration: 5 * 1000,
        });
        break;
      case '404':
        // 接口地址错误
        Message.error({
          content: error.message || '接口地址错误',
          duration: 5 * 1000,
        });
        break;
      case '1099':
        // 服务端错误
        Message.error({
          content: error.message || '服务器异常',
          duration: 5 * 1000,
        });
        break;
      case '503':
        // 服务端错误
        Message.error({
          content: error.message || '服务端错误',
          duration: 5 * 1000,
        });
        break;
      case '308083':
          // 服务端错误
          Message.error({
            content: error.message || '服务端错误',
            duration: 5 * 1000,
          });
          break;
        default:
        break;
    }
    return Promise.reject(error);
  }
);
// function axios (config: any) {
//   const { headerType = 'json', paramsType } = (config as any)
//   const _headerConfig: any = _.assignIn(
//     { ...headerConfig[headerType].headers },
//     {...config?.headers }
//   );
//   if (paramsType) {
//     config.data = jsonToForm(config.data)
//   }

//   config.headers = _headerConfig;
//   return useAxios(config)
// }
export default useAxios;
