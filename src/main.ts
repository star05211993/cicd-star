import { createApp } from 'vue';
import ArcoVue from '@arco-design/web-vue';
import ArcoVueIcon from '@arco-design/web-vue/es/icon';
import globalComponents from '@/components';
import router from './router';
import store from './store';
import i18n from './locale';
import directive from './directive';
// import './mock'; //会导致导出的表格打不开
import App from './App.vue';
import '@arco-design/web-vue/dist/arco.css';
import '@/assets/style/global.less';
import DwButton from './components/base/dwButton/index.vue';
import DwForm from './components/base/dwForm/index.jsx';
import MosUI from '@/components/base';
import '@/assets/iconfont/iconfont.css';
import print from 'vue3-print-nb';
const app = createApp(App);
app.config.warnHandler = () => null;
app.use(ArcoVue, {});
app.use(print);
app.use(ArcoVueIcon);
app.use(router);
app.use(store);
app.use(i18n);
app.use(globalComponents);
app.use(directive);
app.use(MosUI);

// 全局组件
app.component('DwForm', DwForm);
app.component('DwButton', DwButton);

app.mount('#app');
