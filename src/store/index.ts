import { createPinia } from 'pinia';
import piniaPersist from 'pinia-plugin-persist'
import useAppStore from './modules/app';
import {useUserStore, useTentantStore } from './modules/user';
import useTabBarStore from './modules/tab-bar';
import useRouterStore from './modules/routers';

const pinia = createPinia();
pinia.use(piniaPersist)

export { useAppStore, useUserStore, useTabBarStore, useTentantStore, useRouterStore };
export default pinia;
