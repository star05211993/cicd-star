import { defineStore } from 'pinia';
import { memberUser } from '@/api';

const useRouterStore = defineStore('routers', {
  state: ():any => ({
    ROUTER: [],
    editComponent: undefined,
    btnAuthority: undefined,
    pageIdList: undefined,
    menuRouter: undefined,
    menuFunction: undefined,
    dataStatus: undefined,
    isRouter: false,
  }),
  getters: {},
  actions: {
    setInfo (partial: Partial<any>) {
      this.ROUTER = partial.basicdataMenuResultVO.routerList || []; // 菜单路由
      this.editComponent = partial.basicDataComponentList || []; // 组件
      this.btnAuthority = partial.basicDataFunctionList || []; // 功能
      this.pageIdList = partial.basicDataPageVOList || []; // 页面
      console.log(this.pageIdList,'页面权限')
      this.menuRouter = partial.memberMenuList || []; // 会员中心菜单
      this.menuFunction = partial.memberFunctionList || []; // 会员功能列表
      this.dataStatus = partial.dataStatus; // 企业不存在或者状态不是已认证
    },
    // Reset user's information
    resetInfo() {
      this.$reset();
    },
    // Get user's information
    async info(params: any) {
      const res = await memberUser.getUserRouters(params);
      this.isRouter = true
      this.setInfo(res.data);
    },

  }
})

export default useRouterStore