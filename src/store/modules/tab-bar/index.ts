import type { RouteLocationNormalized } from 'vue-router';
import { defineStore } from 'pinia';
import { TabBarState, TagProps } from './types';
import { sessionStrategie,localStrategie } from '@/utils';

const formatTag = (route: RouteLocationNormalized): TagProps => {
  const { name, meta, fullPath, query } = route;
  return {
    title: meta.locale || '',
    name: String(name),
    fullPath,
    query,
  };
};

const useAppStore = defineStore('tabBar', {
  // 开启数据持久化
  persist: {
    enabled: true,
    strategies: [
      {
        // storage: sessionStrategie
        storage: localStorage
      },
    ],
  },
  state: (): TabBarState => ({
     cacheTabList: ['dashboard'],
    // cacheTabList: ['/dashboard'],
    tagList: [
      // Set the first element dynamically as needed
      {
        title: '概览',
        name: 'dashboard',
        fullPath: '/mos/dashboard',
      },
    ],
  }),

  getters: {
    getTabList(state): TagProps[] {
      return state.tagList;    
    },
    getCacheList(): string[] {
      return Array.from(this.cacheTabList);
    },
  },

  actions: {
    updateTabList(route: RouteLocationNormalized) {
      // if (this.tagList.some(item => item.name === route.name)) return;
      // this.tagList.push(formatTag(route));
      // if (!route.meta.ignoreCache) {
      //   this.cacheTabList.push(route.name as string);
      // }
      // 
      
      if (this.tagList.some(item => item.fullPath === route.fullPath)) return;
      this.tagList.push(formatTag(route));
    
      this.tagList.map((item:any,index)=>{
        item.currIndex = index-1
      })
      console.log(this.tagList, 'tag路由=====')
      if (!route.meta.ignoreCache) {
        this.cacheTabList.push(route.name as string);
      }
    },
    deleteTag(idx: number, tag: TagProps) {
      this.tagList.splice(idx, 1);
       console.log(tag,'tag 删除')
      this.cacheTabList = this.cacheTabList.filter(v => v !== tag.name);
      // this.cacheTabList = this.cacheTabList.filter(v => v !== tag.fullPath);
    },
    setFirstTabList() {
      this.tagList = [{
        title: '概览',
        name: 'dashboard',
        fullPath: '/mos/dashboard',
      }];
      
    },

  },
});

export default useAppStore;
