import { defineStore } from 'pinia';
import { memberUser, LoginData } from '@/api';

import { setToken, clearToken } from '@/utils/auth';
import { localStorage, localStrategie,  sessionStrategie } from '@/utils/Storage';
import { removeRouteListener } from '@/utils/route-listener';
import type { UserState, TentantState } from './types';
import { TENANT_DOMAIN } from '@/constant';
import { Message } from '@arco-design/web-vue';

export const useTentantStore = defineStore('tentant', {
  // 开启数据持久化
  persist: {
    enabled: true,
    strategies: [
      {
        storage: localStrategie
      },
    ],
  },
  state: (): TentantState => ({
    address: undefined,
    appletUrl: undefined,
    applicationType: undefined,
    cityId: undefined,
    countryId: undefined,
    countyId: undefined,
    dataStatus: undefined,
    endDate: undefined,
    enterpriseScale: undefined,
    faviconUrl: undefined,
    industry: undefined,
    officialAccountsUrl: undefined,
    parentId: undefined,
    phone: undefined,
    provinceId: undefined,
    startDate: undefined,
    tenantAbbreviation: undefined,
    tenantCode: undefined,
    tenantDomainName: undefined,
    tenantEmail: undefined,
    tenantIco: undefined,
    tenantId: undefined,
    tenantLogo: undefined,
    tenantName: undefined,
    tenantOwnerId: undefined,
    verificationStatus: undefined,
    isTentant: false,
  }),
  getters: {
    tentantInfo(state: any) {
      return { ...state }
    }
  },
  actions: {
    // Set user's information
    setInfo (partial: Partial<TentantState>) {
      this.$patch(partial);
    },
    // Reset user's information
    resetInfo() {
      this.$reset();
    },
    // 租户信息
    async getTenantInfo() {
      try {
        const params = {
          tenantDomainName: TENANT_DOMAIN
        }
        const res = await memberUser.getTenantInfo(params);
        const { code, data, message } = res as any
        if (code !== '200') {
          // 存储租户企业用户信息
          Message.error(message);
          return
        }
        data.isTentant = true
        this.setInfo(data as Partial<TentantState>)
      } catch (err) {
        console.log('err', err);
        // you can report use errorHandler or other
      }
    }
  },
})
export const useUserStore = defineStore('user', {
  // 开启数据持久化
  persist: {
    enabled: true,
    strategies: [
      {
        // storage: sessionStrategie
        storage: localStrategie
      },
    ],
  },
  state: (): UserState => ({
    userId: undefined,
    tenantId: undefined,
    loginName: undefined,
    userName: undefined,
    surName: undefined,
    firstName: undefined,
    mobile: undefined,
    email: undefined,
    password: undefined,
    headPortrait: undefined,
    briefIntroduction: undefined,
    gender: undefined,
    birthday: undefined,
    language: undefined,
    enterpriseId: undefined,
    enterpriseIdentity: undefined,
    firstLoginTime: undefined,
    lastLoginTime: undefined,
    dataStatus: undefined,
    pushProcessFlag: undefined,
    wechatName: undefined,
    userSource: undefined,
    token: undefined,
    updatePwdFlag: undefined,
    tenantEnterpriseVO: undefined,
    enterpriseIdentityList: undefined,
    identityCode: undefined,
    identityName: undefined,
    optEnterpriseId: undefined,
    optEnterpriseName: undefined,
    optEnterpriseNameEn: undefined,
    memberEnterpriseVO: undefined,
    isPlatform: undefined,
    openId: undefined,
    exceptionFlag: undefined,
    enterpriseUserFlag: undefined,
  }),
  getters: {
    userInfo(state: UserState): UserState {
      return { ...state };
    },
  },

  actions: {
    switchRoles() {
      // return new Promise((resolve) => {
      //   this.role = this.role === 'user' ? 'admin' : 'user';
      //   resolve(this.role);
      // });
    },
    // Set user's information
    setInfo(partial: Partial<UserState>) {
      this.$patch(partial);
    },

    // Reset user's information
    resetInfo() {
      this.$reset();
    },

    // Get user's information
    async info() {
      const res = await memberUser.getUserInfo();

      this.setInfo(res.data);
    },

    // Login
    async login(loginForm: LoginData) {
      try {
        const res = await memberUser.login(loginForm);
        const { code, message, data } = res as any
        if (code !== '200') {
          // 存储租户企业用户信息
          // Message.error(message);
          return false
        }
        this.setInfo(data as Partial<UserState>)
        setToken(data.token);
        return true;
      } catch (err) {
        clearToken();
        // throw err;
      }
    },
    //验证码登录
    async loginCode(loginForm: LoginData) {
      try {
        const res = await memberUser.loginCode(loginForm);
        const { code, message, data } = res as any
        if (code !== '200') {
          // 存储租户企业用户信息
          // Message.error(message);
          return false
        }
        this.setInfo(data as Partial<UserState>)
        setToken(data.token);
        return true;
      } catch (err) {
        clearToken();
        // throw err;
      }
    },
   
    // Logout
    async logout() {
      await  memberUser.logout();
      // 清除本地缓存
      this.resetInfo();
      useTentantStore().resetInfo()
      localStorage.clearStorage();
      // sessionStorage.clearStorage();
      sessionStorage.clear()
      removeRouteListener();
    },
  },
});
