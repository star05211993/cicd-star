import { App } from 'vue';
import { use } from 'echarts/core';
import { CanvasRenderer } from 'echarts/renderers';
import { BarChart, LineChart, PieChart, RadarChart } from 'echarts/charts';
import {
  GridComponent,
  TooltipComponent,
  LegendComponent,
  DataZoomComponent,
  GraphicComponent,
} from 'echarts/components';
import Chart from './chart/index.vue';
import Breadcrumb from './breadcrumb/index.vue';

import IconFont from './icon-font/index.vue';
import DoorPoint from './select/doorPoint.vue';
import DwTag from './base/dwTag/index.vue'; // 文字标签

import ButtonArea from './button-area/index.vue'; // 列表页面button区域组件
import SearchBtn from './button-area/search/index.vue'; // button区域搜索按钮
import FilterBtn from './button-area/filter/index.vue'; // button区域筛选按钮
import CustomColumn from './button-area/custom-column/index.vue'; // button区域自定义列按钮
import ExportTable from './button-area/export-table/index.vue'; // button区域导出按钮
import ScreenBtn from './button-area/screen/index.vue'; // button区域全屏按钮
import SyncBtn from './button-area/sync/index.vue'; // button区域刷新按钮
import EditTable from './button-area/edit-table/index.vue'; // button区域编辑表格

import CommonTable from './common-table/index.vue';
import CommonTableSettle from './common-table/indexSettle.vue';

import TabView from './tab-view/index.vue'; // 列表页底部视图组件
import TabViewModal from './tab-view/modalView/index.vue'; // 列表页底部视图弹窗

import FilterContent from './button-area/filter/Content.vue'; // 筛选表单组件
import CustomColumnContent from './button-area/custom-column/Content.vue'; // 自定义列组件

// Manually introduce ECharts modules to reduce packing size

use([
  CanvasRenderer,
  BarChart,
  LineChart,
  PieChart,
  RadarChart,
  GridComponent,
  TooltipComponent,
  LegendComponent,
  DataZoomComponent,
  GraphicComponent,
]);

export default {
  install(Vue: App) {
    // 全局组件注册列表
    Vue.component('Chart', Chart);
    Vue.component('Breadcrumb', Breadcrumb);

    Vue.component('IconFont', IconFont);
    Vue.component('DoorPoint', DoorPoint);
    Vue.component('DwTag', DwTag);

    Vue.component('ButtonArea', ButtonArea);

    Vue.component('SearchBtn', SearchBtn);
    Vue.component('FilterBtn', FilterBtn);
    Vue.component('CustomColumn', CustomColumn);
    Vue.component('ExportTable', ExportTable);
    Vue.component('ScreenBtn', ScreenBtn);
    Vue.component('SyncBtn', SyncBtn);
    Vue.component('EditTable', EditTable);

    Vue.component('CommonTable', CommonTable);
    Vue.component('CommonTableSettle', CommonTableSettle);
    Vue.component('TabView', TabView);
    Vue.component('TabViewModal', TabViewModal);

    Vue.component('FilterContent', FilterContent);
    Vue.component('CustomColumnContent', CustomColumnContent);
  },
};
