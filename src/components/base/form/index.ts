import { TYPES } from './type'
// 设置 input 等 ---> props及event
export const useForm = (v:any) => {
  // 去除多余字段
  const attrsItem = Object.fromEntries(Object.entries(v).filter(n => !/^(props|is|model|form-items)/.test(n[0])))
  return { ...attrsItem }
}

// 设置formitem  ---> props
export const useItem = (item: any) => {
  const { labelWidth } = item

  // 默认设置label 固定宽度
  const colStyle = !!labelWidth ? {
    wrapperColStyle: {
      flex: 1,
    },
    labelColStyle: {
      width: labelWidth,
      flex: 'none'
    }
  } : {}
  // 去除多余字段
  const attrsItem = Object.fromEntries(Object.entries(item).filter(n => !/^(props|is|on|form|model|form-items|labelWidth)/.test(n[0])))

  return { ...colStyle, ...attrsItem }
}

// 设置 input 等 ---> props及event
export const useProps = (v:any) => {
  const placeholder = (/^(select|el-date-picker)/.test(v.is) ? '请选择' : '请输入') + v.label
  const { is = 'a-input', props, on } = v
  return { is, ...props, ...on, placeholder }
}

export const useItems = (v:any, attr:any) => {
  const itemResult = []
  for (const item of v) {
    // 剩余参数语法允许我们将一个不定数量的参数表示为一个数组。theArgs
    let {is, showIf, fieId, props = {}, on, ...theArgs} = item
    if (typeof showIf === 'function' && !showIf(attr.model[attr.field])) {
      continue
    }
    if (typeof props === 'function') {
      props = props(attr.model[attr.field])
    }
    if (is === 'label') {
      props = { ...props, model: attr.model[attr.field], fieId }
    }
    if (TYPES[is]) {
      props = { ...TYPES[is].props, ...props }
      on = { ...TYPES[is].on, ...on }
      is = TYPES[is].is
    }
    itemResult.push({ is, fieId, props, on, ...theArgs })
  }
  return itemResult
}