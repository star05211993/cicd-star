FROM node:14-alpine

# 设置为工作目录 以下 RUN/ADD 都是在这个文件路径下
WORKDIR /app

COPY node/package*.json ./
# 把宿主机的代码添加到镜像中
COPY dist/ ./

# 可以看到镜像里面的内容  查看命令 
RUN ls -lah

# 安装 yarn 依赖
RUN yarn add http-server -g

# 启动端口
EXPOSE 5000

#启动 Node Serve
CMD npm run server