

# vite vue3 temp

## 前端业务规范必读
<!-- ![前端业务规范必读](https://qr.api.cli.im/newqr/create?level=H&transparent=&bgcolor=%23FFFFFF&forecolor=%23000000&blockpixel=12&marginblock=2&logopath=&size=0&text=&data=http%3A%2F%2Fqr61.cn%2FonwF0m%2FqRYcJWW&kid=bizcliim&time=1656036487&key=2246f0ff7713ae4aeb14d0153838a4d2) -->
## 简介
前端线上地址
mos1.0

mos 2.0 
dev 线上地址 ：http://devmos.mayizhilian.net 帐号：密码：
test 线上地址：http://testmos.mayizhilian.net帐号：密码：
pre 线上地址：https://premos.mayibida.com  帐号：密码：
prod 线上地址：https://mos20.mayibida.com 帐号：密码：

mos3.0
dev 线上地址 ：http://devmosnew.mayizhilian.net   帐号：密码：
test 线上地址：http://testmosnew.mayizhilian.net  帐号：密码：
pre 线上地址： http://premosnew.mayizhilian.net 

prod 线上地址：https://mos.mayibida.com
git 地址：



> 使用最新的`vue3`,`vite2`,`typescript`等主流技术搭建的一个供学习参考的模版工程。

参考文档 https://v3.cn.vuejs.org
UI 框架参考文档  https://arco.design/vue/docs/start
设计稿 参考 https://www.figma.com/file/4zImmRTkE2rznMqwAxKrSl/MOS%E4%BC%98%E5%8C%96?node-id=8253%3A44145
 帐号 ：wy.chen@greatmicro.net
 密码： chen1234

## 包含

- **技术栈**：使用 `vue3`,`vite`,`typescript`等前沿技术开发
- **ajax**：二次封装`axios`，统一管理接口
- **工具**：常用的指令，过滤器，`storage`存储，工具函数

## 目录结构

```md
├── README.md
├── package.json
├── index.html
├── src
│   ├── api  # 请求接口
│   ├── assets  # 静态资源
│          └── style 全局样式
│   ├── components  # 通用业务组件
│   ├── config  # 全局配置(包含echarts主题)
│          └── settings.json  # 配置文件
│   ├── directives # 指令集（如需，可自行补充）
│   ├── filters # 过滤器（如需，可自行补充）
│   ├── hooks # 全局hooks
│   ├── layout  # 布局
│   ├── locale  # 国际化语言包
│   ├── mock  # 模拟数据
│   ├── views  # 页面模板
│   ├── router # 路由配置
│   ├── store  # 状态管理中心
│   ├── types  # Typescript 类型
│   └── utils  # 工具库
│   └── App.vue  # 视图入口
│   └── main.ts  # 入口文件
└── tsconfig.json
```

## Project setup

```
yarn
```


### Compiles and hot-reloads for development

```
yarn  server
```

### Compiles and minifies for production

```
yarn dev  #  dev 打包
yarn qc  #  测试 打包
yarn pre  #  预发 打包
yarn build  #  生产 打包
```

### Lints and fixes files

```
yarn lint
```

## 浏览器支持

本地开发推荐使用`Chrome 80+` 浏览器

支持现代浏览器, 不支持 IE

