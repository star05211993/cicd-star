const express = require('express');
const router = express.Router();
const fs = require('fs');
const path = require('path');
const shortid = require('shortid');
const jwt = require('jsonwebtoken');
// const bcrypt = require('bcrypt');
const formidable = require('formidable'); // 文件上传插件
const localDB = require('../public/localDB/uploadData.json');

const MD5 = require('MD5');
//要生成token的字符串
const token_str = 'xzxnb';

//中间件
function verityToken(req, res) {
  // console.log('verityToken', req.body.accessToken);
  const token = req.body.accessToken;
  jwt.verify(token, token_str, (err, data) => {
    if (err) {
      res.redirect('/login');
      return;
    }
    next();
  });
}

//登录接口
router.post('/member/member-user/login', (req, res) => {
  console.log('member-user');
  const { loginName, password } = req.body;
  // 密码加密
  const psd = MD5(password);
  if (loginName === 'admin' && password === 'admin') {
    jwt.sign(
      { username: loginName },
      token_str,
      { expiresIn: 60 },
      (err, token) => {
        if (err) throw err;
        res.send({
          requestId: shortid.generate(),
          timestamp: new Date().getTime(),
          code: '200',
          message: 'success',
          data: {
            basicdataMenuResultVO: {},
            basicDataComponentList: {},
            basicDataFunctionList: {},
            basicDataPageVOList: {},
            memberMenuList: [],
            memberFunctionList: [],
            dataStatus: null,
            userId: shortid.generate(),
            tenantId: '',
            loginName: loginName,
            userName: 'mr谢',
            password: psd,
            wechatName: '',
            userSource: '',
            updatePwdFlag: '',
            tenantEnterpriseVO: [],
            enterpriseIdentityList: [],
            identityCode: '',
            identityName: '',
            optEnterpriseId: '',
            optEnterpriseName: '',
            optEnterpriseNameEn: '',
            memberEnterpriseVO: {},
            isPlatform: '',
            openId: '',
            exceptionFlag: 0,
            enterpriseUserFlag: false,
            token: token,
          },
        });
      }
    );
  }
});

//获取图形验证码
router.get('/gateway/member/member-user/captcha', (req, res) => {
  console.log('login');
  res.send({
    requestId: shortid.generate(),
    timestamp: new Date().getTime(),
    code: '200',
    message: 'success',
    data: {
      captcha:
        'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIIAAAAwCAIAAABSYzXUAAAIWUlEQVR42u2aX0xTVxzHTZZlD3vZwx72sIdl2cuejTpN3NM0Ju7FxCXTGOeDq4BDFEHEGUSZBv8yI9NW/qjlj/JPDFVQipiK/FEopcqftVApIBSwQEshQKE9+17PzaUDenvvbbeZ5fzye/HSnp7z+/z+nusqwuQ9kFXMBAwDE4aBYWDCMDAMTN7J3PQcwyBJ9J3u7zO7P41vW6Vq+eSgactla1W7OyIrD1oG85Pym+81MwxiMu/z/3K7/4vklxk1I71Ozm0dbu81w+hXx1/t1do9sz7FK/t8Plg/KypLo9JUXq7EPxmGoAIG32VYJ6YXljzHE8QHSChb1j3iLk8vBwCo+ZE5kAHDsEIuQhwsZyCQQEwoyE42oy03NhcACpMLHd0OVqJDCPwduUjkA7dfjKFOyKrGdfl1NAiQiGY8M6xTCi2oybQeBBPUCVRsiauNDY6VppVSBsYHxiWJiGEI3r+rWiLyGUjPix4hEaE7YnPDvx0N3lmvQWsQOqJgiYhh+Adrg7PfWZJaQhMR2lORRCQPA9Z58YJkZpL4eKJSkdhYcu4cqa0lHs//s1P6PClEp6Qzu4J9vcPQkb0/GwDyEvL6zH0Ru8xwuXx/xK7WqFZRxZPpadLZSW7eJAcPEoMhxOquYRd65Gp1NbaVFZ2VHZNdcLTgSe6TYduwXAONj5OmJu53z58nBw6Q6GgSE0OOHCEaDWluVmLxmb6+Ya3Wlpho3rzZuG6dcf36l1u39qaknLpYrWBuGOj1Zp60Jqlex6imo/b5sbejR0luLrHZwsaAOPg9KVVgQDEsDuWDJDWV1NSIrU5jc0V9XvZcltUQiCJ68iRxOuVhaFm9OpjeiP4N08ONeqcwRSMXiUzRo/ZRkb2VlYWH4elDs2bfB1nRH66IAYK8lJBA+vuDrlB6qrRF19Lf3r/gXaDTfHdTt/awlpKwNFjkYrh8mcuQrndZwe8nFgu5dIn/EyJjakoGho4dO4Y0Gnd9vW+Os7V/YWGssrJt0yZKwqAphu+jGtM7JcRHsKmtq64LiShO5TwdZzY8cnq9vAcjdg8f5vfW0KAUg29+Tn3ga5jeVHkmGAZIfT3JyZHnhq9bX1MMZWll0r+F4jQ0tPKfrl3jT1tSEm5tmHj8mGLo3LlTymimV+uF0WxqfKkXtLbyG0tLU4qhqTQRdi9NW/sutwTFgFKBoi1L/H4/3TqqRUTqqsPBn/bYsbDX8vkoBlQL8Q++6XyDgsedIirLVGVasSNCvNKNoZIpweDoeQajq1UfuYYt4hhoulCGIfdAbqQ6HImnlY7BtHFj8I/4Gksb6ViA3nTENhL8pPzG0FPIxuCd9dw+9iWMnhZ70e0mEY+GgY4BigEzTkQYTE7yp92/P9yl3I2NFIM9SB6ZcEwI9xPYPyY1sfLTwW9Mq5WPwaDdC4uXp39z4QIxm0NgMJnI1asy4sDeZs87wsVy0YmimcmZiGBAtyYxBYvHwYTBYN6yBQzat2+fR4O8LAjQfNP7iVvxt3pNvaInJW1tXNeAXZ04wTmKPAz97VUwd07sx+63NowF6NNFMCAfome1SOh3AlvVO8fvoH0S9yMZ/uvm50rovXtKVghsVV9t24b2yYcYX1YJilKK6P5rsmpE7icCW9Xjx4lOR2ZnZTass9PjeQmfwdzttVdoP4oTYj4IhuHuXa6BkSKBGK5HXddd0qGLJRHI5AQhS88cFxfa6UJiMK5ZY4mKQhe7mPGck7A7rQT5Sfk2o036fBMVxfXT7e0yMeg1P8DWFee/DexHU1JWwIDzg0FyMnHLeQWCpDRkGbqfcZ/yaChuCBNDdvbimZXN0oFIJ41Ga3Q05TFw8SJmneZ7zTQLAQPKMp1+pJ2USxIZGfzeioslY7C1FNN05HH+bV6vrSVLLjNQMJCLEAdupS/JH2Y+pCSsjVbFdisoWGQgPszLku5DhyiJip9+FWaCscExZavBSnSHjY3SMNyK/1RIR8tSCo+BXu2hJnd2hnXUt/a3/AR3ukzZCvAvgcH9+xFj4B5xN2UWUQxPN24pTy8PM3na7fwmT5+WhiHw7iikhnnaMCc4TMuybmxCX/N5Zrrquugr++s/q/lSsXatxGtq8ewkZab5jzHk7M+R+93y8kUGIXOuiCDRj9pH0YOiVtHbaVoDaq5VUwytGzaED1jAID7TLDXooGXQ+MBoqbdArU1WNGp4Ilh/anwqfAeBoFDTY8MBZX0RLanAoLBQbg32YfhCq4PCW5VZRWuvYH3AQEAgLFCoKYauPXvCPykKNd1terocDC0VLSvdSwtxwD+B+xQmF0IxUuou6KA42JObT6DPbj/DIlA4GsWJSQcsoZj70f9xg+gpfhA1683Sj1RRscggN8g9CBwFP4EfQk6HG2EP2I9erS9JLRFcXtC8hDxsGGCEIcY3M9O1ezfFMJyfHyaDuTly5gy/Yb1eZjTAWahBa3Nq4SMwsYABjTPtoCOrAtRAFQBTvZT8p8DgVAI3TAV+eLmJV1RMvzgRwJi2/ziQc8PT1ub38gC8Y2NOnQ7zs3DD6l+Q2p6ePcu1aj09ZH5+cahsaODmZ2G8F08ikrL88pKAxAqngzr7ndTT+8x91Pc7DB00GsQtkhUjD6f4ax9Bbx25A0LABh96fve5qcqEVIO9YauB6VTknQ+0Oy5u3uWSe7EYTK9cCf0iRCEGSXf3jgnknGp1dcHRAnREGJ5RkNGhGrQGh9WxJGtTqIGKVp0CpioRg5SbA6416u1FzrElJr7cupV7A7pmDQpy565d9rQ0T2urgpt25By1mnvxiY4IwzMKMjpUrZZYpc1F7H9mvBfCMDAMTBgGhoEJw8AwMGEYGAYmDAPDwESq/AXqa19IAmCaGgAAAABJRU5ErkJggg==',
    },
  });
});

//获取dominName
router.post(
  '/member/tenant-enterprise/selectTenantByDomainName',
  (req, res) => {
    res.send({
      code: '200',
      data: null,
      message: 'success',
      requestId: shortid.generate(),
      timestamp: new Date().getTime(),
    });
  }
);

// 文件上传简易版本
// router.post('/member/upload', (req, res) => {
//   const form = new formidable.IncomingForm();
//   form.parse(req, (err, fileds, files) => {
//     console.log('上传', err);
//     // 向前端发送文件
//     if (err) {
//       res.send({
//         code: '200',
//         data: null,
//         message: '上传失败',
//         success: true,
//       });
//       return;
//     }
//     res.send({
//       code: '200',
//       data: fileds,
//       message: '上传成功',
//       success: true,
//     });
//   });
// });

// 文件上传完整版
router.post('/member/upload', (req, res) => {
  const form = new formidable.IncomingForm(); // 创建上传表单
  form.encoding = 'utf-8'; // 设置编辑
  console.log('public', path.resolve(__dirname, '../public/upload/'));
  form.uploadDir = path.resolve(__dirname, '../public/upload/'); // 设置上传目录
  form.keepExtensions = true; // 保留后缀
  form.maxFieldsSize = 2 * 1024 * 1024; // 文件大小（默认20M）

  form.parse(req, (err, fileds, files) => {
    console.log('fields', fileds);
    console.log('files', files);
    if (err) {
      res.send({
        status: '201',
        message: err,
      });
      return;
    }
    try {
      // 若文件流的键名为uplaodFile，则filename = files.uplaodFile.name
      console.log('files.file.path', files.file.filepath);
      let filename = files.file.originalFilename;
      // // 对文件名进行处理，以应对上传同名文件的情况
      let nameArray = filename.split('.');
      let type = nameArray[nameArray.length - 1];
      let name = '';
      for (var i = 0; i < nameArray.length - 1; i++) {
        name = name + nameArray[i];
      }
      var rand = Math.random() * 100 + 900;
      var num = parseInt(rand, 10);

      var avatarName = name + num + '.' + type;

      var newPath = form.uploadDir + '\\' + avatarName;
      console.log('newPath', newPath);
      // // 若文件流的键名为uplaodFile，则fs.renameSync(files.uplaodFile.path, newPath)
      fs.renameSync(files.file.filepath, newPath); //重命名
      res.send({ code: '200', message: '文件上传成功', success: true });
    } catch (err) {
      res.send({
        code: '201',
        message: err,
        success: false,
      });
      return;
    }
  });
});

// 文件下载小文件和大文件
router.get('/member/dowload', async (req, res) => {
  try {
    // 模拟数据不然应该从mongoDB中获取数据
    const name = localDB.url + '.' + localDB.type;
    console.log('name', name);
    // 获取文件路径
    // let filePath = path.join(__dirname, '../public/upload/' + req.query.name);
    let filePath = path.join(__dirname, '../public/upload/' + name);
    // 获取文件大小
    const size = fs.statSync(filePath).size;
    res.writeHead(200, {
      // 告诉浏览器这是一个需要以附件形式下载的文件（浏览器下载的默认行为，前端可以从这个响应头中获取文件名：前端使用ajax请求下载的时候，后端若返回文件流，此时前端必须要设置文件名-主要是为了获取文件后缀，否则前端会默认为txt文件）
      'Content-Disposition': 'attachment; filename=' + encodeURIComponent(name),
      // 告诉浏览器是二进制文件，不要直接显示内容
      'Content-Type': 'application/octet-stream',
      // 下载文件大小
      'Content-Length': size,
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Headers': 'X-Requested-With',
      'Access-Control-Allow-Methods': 'PUT,POST,GET,DELETE,OPTIONS',
      //如果不暴露header，那就Refused to get unsafe header "Content-Disposition"
      'Access-Control-Expose-Headers': 'Content-Disposition',
    });
    const readStream = fs.createReadStream(filePath);
    readStream.on('data', function (chunk) {
      if (!res.write(chunk)) {
        readStream.pause();
      }
    });
    readStream.on('end', function () {
      res.send();
    });

    res.on('drain', function () {
      readStream.resume();
    });
  } catch (err) {
    res.send({
      status: 201,
      message: err,
    });
    return;
  }
});
module.exports = router;
