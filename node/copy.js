const fs = require('fs');
const path = require('path');
// console.log(path.resolve(__dirname, '../dist'));
// console.log(path.resolve(__dirname, '../node/dist'));
fs.cp(
  path.resolve(__dirname, '../dist'),
  path.resolve(__dirname, '../node/dist'),
  { recursive: true },
  (err) => {
    if (err) {
      console.log(err);
    }
  }
);
