const http = require('node:http');
console.log('http', http);
const fs = require('node:fs');
// const html = `<!DOCTYPE html>
// <html lang="en">
//   <head>
//     <meta charset="UTF-8" />
//     <link rel="shortcut icon" type="image/x-icon" href="https://testmos.mayizhilian.net/favicon.ico">
//     <meta name="viewport" content="width=device-width, initial-scale=1.0" />
//     <title>蚂蚁必达</title>
//   </head>
//   <body>
//    <div>hello node</div>
//   </body>
// </html>`;
const html = fs.readFileSync('../index.html');
const server = http.createServer((req, res) => {
  //   res.end(html);
  // 以流的形式读取静态资源
  fs.createReadStream('../index.html').pipe(res);
});
server.listen(5000, () => console.log('listening 5000'));
