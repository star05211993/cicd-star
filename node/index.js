const express = require('express');
const app = express();
const path = require('path');
// const api = require('./api/index.js');
const history = require('connect-history-api-fallback');

//设置跨域访问
app.all('*', function (req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', '*');
  res.header('Access-Control-Allow-Methods', 'PUT, POST, GET, DELETE, OPTIONS');
  if (req.method === 'OPTIONS') {
    res.sendStatus(200);
  } else {
    next();
  }
});

app.use(history());
//加载静态资源
app.use(express.static(path.resolve(__dirname, './dist')));
// 路由
const route = require('./route/index.js');
//为了获取请求头里面的json
app.use(express.json());

// 为了获取请求头里面的body
app.use(express.urlencoded({ extended: false }));
// app.use(bodyParser.urlencoded({ extended: true }));
// app.use(bodyParser.json());
//使用路由
app.use(route);

// 使用接口
// app.use(api);

app.listen(5000, () => console.log('listening 5000'));
