FROM nginx:alpine

# 设置为工作目录 以下 RUN/ADD 都是在这个文件路径下
# WORKDIR /cicd-star

# 把宿主机的代码添加到镜像中
# ADD . /cicd-star

# 复制dist 文件到 nginx的index.html文件夹下
COPY dist/ /usr/share/nginx/html/

# 复制本地nginx配置覆盖原来配置 
COPY nginx/default.conf /etc/nginx/conf.d/default.conf