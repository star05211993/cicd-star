// eslint-disable-next-line @typescript-eslint/no-var-requires
const path = require('path');

module.exports = {
  root: true,
  parser: 'vue-eslint-parser',
  parserOptions: {
    // Parser that checks the content of the <script> tag
    parser: '@typescript-eslint/parser',
    sourceType: 'module',
    ecmaVersion: 2020,
    ecmaFeatures: {
      jsx: true,
    },
  },
  env: {
    'browser': true,
    'node': true,
    'vue/setup-compiler-macros': true,
  },
  extends: ["plugin:vue/vue3-essential", "plugin:vue/essential", "eslint:recommended"],
  plugins: ["vue", "@typescript-eslint"],
  settings: {
    'import/resolver': {
      typescript: {
        project: path.resolve(__dirname, './tsconfig.json'),
      },
    },
  },
  rules: {
    'no-debugger': import.meta.env.MODE === 'prod' ? 'warn' : 'off',
    "vue/multi-word-component-names":"off",
    'vue/no-multiple-template-root': 'off'
  },
};
